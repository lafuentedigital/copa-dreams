<!DOCTYPE html>
<html lang="es">
  <head>
    <!-- META -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Librería jQuery requerida por los plugins de JavaScript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-migrate.min.js"></script>

    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.file-input.js"></script>
    <script src="js/docs.min.js"></script>
    <script src="js/form.min.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/locales/bootstrap-datepicker.es.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/additional-methods.min.js"></script>
    <script src="js/highcharts.js"></script>
    <script src="js/custom.js"></script>

    <title>Copa Sueños</title>

    <link rel="shortcut icon" type="image/x-icon" href="favicon.jpg">
    <link href="favicon_apple.jpg" rel="apple-touch-icon">

    <!-- CSS de Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="css/bootstrap-theme.css" rel="stylesheet" media="screen">

    <!-- CSS personalizado para Copa Dreams -->
    <link href="css/custom.css" rel="stylesheet">

    <!-- CSS Login -->
    <link href="css/login.css" rel="stylesheet">

    <!-- CSS Dashboard -->
    <link href="css/dashboard.css" rel="stylesheet">

    <!-- CSS Bootstrap Picker -->
    <link href="css/datepicker.css" rel="stylesheet">

    <!-- librerías opcionales que activan el soporte de HTML5 para IE8 -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>