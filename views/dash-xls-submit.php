  <?php
    if(isset($_SESSION['load-data']))
    {
      $dataPage=$_SESSION['load-data'];
    }

    $dataInfo=array('coms-load'=> array('title'=>'Cargar Reporte de Comunicaciones'),'agent-load'=> array('title'=>'Cargar Reporte de Agente'),'agency-load'=> array('title'=>'Cargar Reporte de Agencia'),'agent-commail-b-load'=> array('title'=>'Cargar Comunicaciones Básicas Agente (Mail)'),'agency-commail-b-load'=> array('title'=>'Cargar Comunicaciones Básicas Agencia (Mail)'),'agent-commail-t-load'=> array('title'=>'Cargar Comunicaciones Tácticas Agente (Mail)'),'agency-commail-t-load'=> array('title'=>'Cargar Comunicaciones Tácticas Agencia (Mail)'),'agent-comsms-t-load'=> array('title'=>'Cargar Comunicaciones Tácticas Agente (SMS)'),'agency-comsms-t-load'=> array('title'=>'Cargar Comunicaciones Tácticas Agencia (SMS)'))
  ?>

  <div class="row">
    <?php
        include("sidebar.php");
    ?>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      <?php
      
      if(in_array('write', $_SESSION['capabilities']))
      {

      ?>
      <h1 class="page-header"><?php echo $dataInfo[$dataPage]['title']; ?></h1>

        <div id="load-bar" class="progress progress-striped active">
          <div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
            <span>Esto puede tardar algo.</span>
          </div>
        </div>

        <form id="data-xls-submit" name="data-xls-submit" action="inc/xls-upload.php" method="POST" enctype="multipart/form-data">
           <!--  <input type="hidden" name="MAX_FILE_SIZE" value="33554432"> -->
            <input type="file" name="file" title="Seleccionar Archivo" class="btn"> <br>
            <?php 
              if($dataPage=='coms-load')
              {
            ?>
                <select id="condition" name="condition">
                  <option value="agent">Agente</option>
                  <option value="agency">Agencia</option>
                </select>
                <select id="type" name="type">
                  <option value="commail-b-load">Básicas Email</option>
                  <option value="commail-t-load">Tácticas Email</option>
                  <option value="comsms-t-load">Tácticas SMS</option>
                </select>
                <br><br>
            <?php
              }
            ?>
            <input type="submit" value="Cargar" name="uploadSubmitter" id="submitbtn" class="btn">
        </form>
        <div id="responsesubmit" style="display:none">
        </div>
      <?php

      }
      else
      {

      ?>
      <h1 class="page-header">No tienes permiso para ver esta pagina.</h1>
      <?php

      }

      ?>
    </div>
  </div>
  