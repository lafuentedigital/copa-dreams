<?php
 include('vfunctions.php');
?>
                <thead>
                  <tr>
                    <th>Agente/Agencia</th>
                    <th>% Abiertos</th>
                    <th>Vecindario</th>
                    <th>Pais</th>
                    <th>Ciudad</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  ini_set('memory_limit', '-1');
                  if(!isset($_SESSION['query_coms_0']))
                  {
                   
                    $query="
                      SELECT
                        code,
                        opening,
                        loyalty,
                        country,
                        city
                      FROM
                            (
                              SELECT
                                      basic_email_agent_agent as code,
                                      basic_email_agent_campaign as campaign,
                                      ((basic_email_agent_opened*100)/basic_email_agent_recived) as opening,
                                      dn_neighborhood as loyalty,
                                      country_name as country,
                                      city_name as city,
                                      basic_email_agent_city
                              FROM
                                      basic_communications_email_agent
                              JOIN
                                      dashboard_neighborhood_agent
                              ON
                                      dn_agent=basic_email_agent_agent
                              JOIN
                                      country
                              ON
                                      country_id=basic_email_agent_country
                              JOIN
                                      city
                              ON
                                      city_id=basic_email_agent_city
                              GROUP BY
                                code, campaign
                            ) as drv2
                    ";

                    $_SESSION['query_coms_0']=$query;
                    //echo $query;
                  }
                  else
                  {
                    $query=$_SESSION['query_coms_0'];
                  }

                  if($sta=$db->prepare($query))
                  {
                    $sta->execute();

                    $sta->bind_result($code,$opening,$loyalty,$country,$city);

                    while ($sta->fetch()) 
                    {
                ?>
                  <tr>
                    <td><?php echo $code; ?></td>
                    <td class="aright"><?php if($opening==NULL){$opening='0'; echo $opening; }else{ echo number_format((float)round($opening, 2, PHP_ROUND_HALF_ODD), 0, '.', ','); } ?></td> 
                    <td><?php if($loyalty==NULL){$loyalty='No Aplica'; echo $loyalty; }else{ echo neighborhoodsName($loyalty); } ?></td> 
                    <td><?php echo $country; ?></td>
                    <td><?php echo $city; ?></td>
                  </tr>
                <?php
                    }
                    $sta->close();
                  }
                  
                ?>
                <!-- <tr><td colspan="3"><?php //echo $_SESSION['query_coms_0']; ?></td></tr> -->
                </tbody>
              </table>