      <?php
        include('../inc/db.php');
      ?>
      <div class="row">
        
        <?php
          include("sidebar.php");
        ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          
          <h1 class="page-header">Definir Vecindarios</h1>

          <div class="row">
            <?php
              $sql="SELECT main.report_agent_date_flown FROM report_agent as main GROUP BY main.report_agent_date_flown ORDER BY main.report_agent_date_flown DESC";

              $theDateList=array();
              $cnt=1;

              if($stmt=$db->prepare($sql))
              {
                  $stmt->execute();
                  $stmt->bind_result($getDate);

                  while($stmt->fetch())
                  {
                    $d=explode("-", $getDate);
                    $theDate=$d[1]."-".$d[0];

                    if(empty($theDateList[$theDate]))
                    {
                      $sqli="SELECT dn_id FROM dashboard_neighborhood_agent WHERE dn_month=? AND dn_year=? LIMIT 0,1";
                      $id=0;
                      if($stm=$dbi->prepare($sqli))
                      {
                        $stm->bind_param('ii',$d[1],$d[0]);
                        $stm->execute();
                        $stm->bind_result($id);
                        $stm->fetch();
                        if($id==0)
                        {
                          $theDateList[$theDate]['opt']=$cnt;
                          $theDateList[$theDate]['date']=$theDate;
                          $cnt++;

                        }
                         //echo "fecha:".$theDate." id:".$id."\n";
                        $stm->close();
                      }
                    }
                  }
                  
                  $stmt->close();
              }

              //var_dump($theDateList);
            ?>
            <?php
              if(sizeof($theDateList)>0)
              {
            ?>
            <div class="col-sm-3">
              <select id="date-neighborhood" class="form-control input-sm">
                <option value="0">Seleccione una fecha.</option>
              <?php
                foreach ($theDateList as $row) {
                  //var_dump($row);
              ?>
                <option value="<?php echo $row['opt']; ?>"><?php echo $row['date']; ?></option>
              <?php
                }
              ?>
              </select>
              </div>
               <a id="define-neighborhoods" href="javascript:void(0)" class="btn btn-primary btn-sm">Definir Vecindarios</a>

            <?php
              }
              else
              {
            ?>
            <div class="col-sm-12">
              <h2>Todos los vecindarios estan definidos.</h2>
            </div>
            <?php
              }
            ?>
            
          </div>

        </div>
      </div>