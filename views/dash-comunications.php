<?php
 include('vfunctions.php');
?>
 <div class="row">
        
        <?php
          include("sidebar.php");
        ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          
          <h1 class="page-header">Comunicaciones Generales</h1>
          <h4>Filtros:</h4>
            <div class="text-left col-md-12">
                <div class="col-sm-2">
                  <select id="filter-neighborhood" class="form-control input-sm loyalty-control">
                    <option value="0">Vecindario</option>
                    <?php
                      if($sta=$db->prepare('SELECT neighborhood_id,neighborhood_name FROM  neighborhood'))
                      {
                        $sta->execute();
                        $sta->bind_result($id,$name);
         
                        while ($sta->fetch()) 
                        {
                    ?>
                    <option value="<?php echo $id; ?>"><?php echo neighborhoodsName($id); ?></option>
                    <?php
                        }
                        $sta->close();
                      }
                    ?>
                  </select>
                </div>
                <div class="col-sm-1">
                  <h5>Período:</h5>
                </div>
                <div class="col-sm-2">
                  <div class="input-group date p">
                    <input id="filter-period" class="form-control input-sm loyalty-control" type="text" 
                    readonly>
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                  </div>
                </div>

                <div class="col-sm-2">
                  <select id="filter-location-country" class="form-control input-sm loyalty-control">
                    <option value="0">País</option>
                    <?php
                      if($sta=$db->prepare('SELECT country_id,country_name FROM  country'))
                      {
                        $sta->execute();
                        $sta->bind_result($id,$name);
         
                        while ($sta->fetch()) 
                        {
                    ?>
                    <option value="<?php echo $id; ?>"><?php echo $name; ?></option>
                    <?php
                        }
                        $sta->close();
                      }
                    ?>
                  </select>
                </div>

                <div class="col-sm-2">
                  <select id="filter-location-city" class="form-control input-sm loyalty-control">
                    <option value="0">Ciudad</option>
                  </select>
                </div>

                
                            </div>

            <div class="text-right col-md-12 filter-btn">
                <div class="col-sm-9">
                  <a id="filter-coms-tab1" href="javascript:void(0)"  class="btn btn-primary btn-sm">Filtrar</a>
                </div>
                <div class="col-sm-1">
                  <a id="clean-coms-tab1" href="javascript:void(0)" class="btn btn-primary btn-sm">Reset</a>
                </div>
                <div class="col-sm-2">
                  <a href="inc/excelComGen.php" class="btn btn-primary btn-sm">Descargar Excel</a>
                </div>
            </div>
            <div class="row">
              <table id="top-table1" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>----</th>
                    <th>Recibidos</th>
                    <th>% Abiertos</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  ini_set('memory_limit', '-1');
                  if(!isset($_SESSION['query_coms']))
                  {
                   
                    $query="
                      SELECT
                              campaign, SUM(recieved) as total_recieved, ((SUM(opened)*100)/SUM(recieved)) as percent_opened
                      FROM
                              (
                                      (SELECT 
                                              agent_id,agent_agency,basic_email_agent_campaign as campaign, basic_email_agent_recived as recieved, basic_email_agent_opened as opened
                                      FROM
                                              agent
                                      INNER JOIN
                                              basic_communications_email_agent
                                      ON
                                              agent_id=basic_email_agent_agent
                                      )
                                      UNION
                                      (SELECT 
                                              agent_id,agent_agency,basic_email_agency_campaign as campaign, basic_email_agency_recived as recieved, basic_email_agency_opened as opened
                                      FROM
                                              agent
                                      INNER JOIN
                                              basic_communications_email_agency
                                      ON
                                              agent_agency=basic_email_agency_agency
                                      GROUP BY
                                              agent_agency
                                      )
                              ) as drv1
                      GROUP BY
                              campaign
                    ";

                    $_SESSION['query_coms']=$query;
                    //echo $query;
                  }
                  else
                  {
                    $query=$_SESSION['query_coms'];
                  }

                  if($sta=$db->prepare($query))
                  {
                    $sta->execute();

                    $sta->bind_result($campaign,$recieved,$opened);

                    while ($sta->fetch()) 
                    {
                ?>
                  <tr>
                    <td><?php echo $campaign; ?></td>
                    <td class="aright"><?php if($recieved==NULL){$recieved='0'; echo $recieved; }else{ echo number_format((float)round($recieved, 2, PHP_ROUND_HALF_ODD), 0, '.', ','); } ?></td> 
                    <td class="aright"><?php if($opened==NULL){$opened='0'; echo $opened; }else{ echo number_format((float)round($opened, 2, PHP_ROUND_HALF_ODD), 0, '.', ','); } ?></td> 
                  </tr>
                <?php
                    }
                    $sta->close();
                  }
                  
                ?>
                <!-- <tr><td colspan="3"><?php //echo $_SESSION['query_coms']; ?></td></tr> -->
                </tbody>
              </table>
            </div>
            

            <!-- ############################################################################################################## -->

            <h4>Filtros:</h4>
            <div class="text-left col-md-12">

                <div class="col-sm-2">
                  <select id="filter-condition" class="form-control input-sm loyalty-control">
                    <option value="1">Agente</option>
                    <option value="2">Agencia</option>
                  </select>
                </div>

                <div class="col-sm-2">
                  <select id="filter-open-percent" class="form-control input-sm loyalty-control">
                    <option value="0">Porcentaje</option>
                    <option value="1">0% - 60%</option>
                    <option value="2">61% - 70%</option>
                    <option value="3">71% - 80%</option>
                    <option value="4">81% - 90%</option>
                    <option value="5">91% - 100%</option>
                  </select>
                </div>

                <div class="col-sm-2">
                  <select id="filter-location-country-2" class="form-control input-sm loyalty-control">
                    <option value="0">Pais</option>
                    <?php
                      if($sta=$db->prepare('SELECT country_id,country_name FROM  country'))
                      {
                        $sta->execute();
                        $sta->bind_result($id,$name);
         
                        while ($sta->fetch()) 
                        {
                    ?>
                    <option value="<?php echo $id; ?>"><?php echo $name; ?></option>
                    <?php
                        }
                        $sta->close();
                      }
                    ?>
                  </select>
                </div>

                <div class="col-sm-2">
                  <select id="filter-location-city-2" class="form-control input-sm loyalty-control">
                    <option value="0">Ciudad</option>
                  </select>
                </div>

                <div class="col-sm-2">
                  <select id="filter-neighborhood-2" class="form-control input-sm loyalty-control">
                    <option value="0">Vecindario</option>
                    <?php
                      if($sta=$db->prepare('SELECT neighborhood_id,neighborhood_name FROM  neighborhood'))
                      {
                        $sta->execute();
                        $sta->bind_result($id,$name);
         
                        while ($sta->fetch()) 
                        {
                    ?>
                    <option value="<?php echo $id; ?>"><?php echo neighborhoodsName($id); ?></option>
                    <?php
                        }
                        $sta->close();
                      }
                    ?>
                  </select>
                </div>
                          </div>

            <div class="text-right col-md-12 filter-btn">
                <div class="col-sm-11">
                  <a id="filter-coms-tab2" href="javascript:void(0)"  class="btn btn-primary btn-sm">Filtrar</a>
                </div>
                <div class="col-sm-1">
                  <a id="clean-coms-tab2" href="javascript:void(0)" class="btn btn-primary btn-sm">Reset</a>
                </div>
            </div>
            <div class="row">
              <table id="top-table2" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Agente/Agencia</th>
                    <th>% Abiertos</th>
                    <th>Vecindario</th>
                    <th>Pais</th>
                    <th>Ciudad</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  ini_set('memory_limit', '-1');
                  if(!isset($_SESSION['query_coms_0']))
                  {
                   
                    $query="
                      SELECT
                        code,
                        opening,
                        loyalty,
                        country,
                        city
                      FROM
                            (
                              SELECT
                                      basic_email_agent_agent as code,
                                      basic_email_agent_campaign as campaign,
                                      ((basic_email_agent_opened*100)/basic_email_agent_recived) as opening,
                                      dn_neighborhood as loyalty,
                                      country_name as country,
                                      city_name as city,
                                      basic_email_agent_city
                              FROM
                                      basic_communications_email_agent
                              JOIN
                                      dashboard_neighborhood_agent
                              ON
                                      dn_agent=basic_email_agent_agent
                              JOIN
                                      country
                              ON
                                      country_id=basic_email_agent_country
                              JOIN
                                      city
                              ON
                                      city_id=basic_email_agent_city
                              GROUP BY
                                code, campaign
                            ) as drv2
                    ";

                    $_SESSION['query_coms_0']=$query;
                    //echo $query;
                  }
                  else
                  {
                    $query=$_SESSION['query_coms_0'];
                  }

                  if($sta=$db->prepare($query))
                  {
                    $sta->execute();

                    $sta->bind_result($code,$opening,$loyalty,$country,$city);

                    while ($sta->fetch()) 
                    {
                ?>
                  <tr>
                    <td><?php echo $code; ?></td>
                    <td class="aright"><?php if($opening==NULL){$opening='0'; echo $opening; }else{ echo number_format((float)round($opening, 2, PHP_ROUND_HALF_ODD), 0, '.', ','); } ?></td> 
                    <td><?php if($loyalty==NULL){$loyalty='No Aplica'; echo $loyalty; }else{ echo neighborhoodsName($loyalty); } ?></td> 
                    <td><?php echo $country; ?></td>
                    <td><?php echo $city; ?></td>
                  </tr>
                <?php
                    }
                    $sta->close();
                  }
                  
                ?>
                <!-- <tr><td colspan="3"><?php //echo $_SESSION['query_coms_0']; ?></td></tr> -->
                </tbody>
              </table>
            </div>

          </div>
        </div>