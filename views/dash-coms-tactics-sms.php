<?php
  include('vfunctions.php');
  $routesTab1="
    (
    SELECT
      report_agent_flight_origin, report_agent_flight_destiny
    FROM
      report_agent
    INNER JOIN
      agent
    ON
      report_agent_agent=agent_id
    INNER JOIN
      tactic_communications_sms_agent
    ON
      report_agent_agent =  tactic_sms_agent_agent
    GROUP BY
      report_agent_flight_origin, report_agent_flight_destiny

    )UNION(

    SELECT
      report_agent_flight_origin, report_agent_flight_destiny
    FROM
      report_agent
    INNER JOIN
      agent
    ON
      report_agent_agent=agent_id
    INNER JOIN
      tactic_communications_sms_agency
    ON
      tactic_sms_agency_agency=agent_agency
    GROUP BY
      report_agent_flight_origin, report_agent_flight_destiny
    )";

    $sqlAgents="
    SELECT
        report_agent_flight_origin, report_agent_flight_destiny
      FROM
        report_agent
      INNER JOIN
        agent
      ON
        report_agent_agent=agent_id
      INNER JOIN
        tactic_communications_sms_agent
      ON
        report_agent_agent =  tactic_sms_agent_agent
      GROUP BY
        report_agent_flight_origin, report_agent_flight_destiny
    ";

?>

 <div class="row">
        
        <?php
          include("sidebar.php");
        ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          
          <h1 class="page-header">Comunicaciones Tacticas SMS</h1>
          <h4>Filtros:</h4>
            <div class="text-left col-md-12">
                <div class="col-sm-2">
                  <select id="filter-neighborhood" class="form-control input-sm loyalty-control">
                    <option value="0">Vecindario</option>
                    <?php
                      if($sta=$db->prepare('SELECT neighborhood_id,neighborhood_name FROM  neighborhood'))
                      {
                        $sta->execute();
                        $sta->bind_result($id,$name);
         
                        while ($sta->fetch()) 
                        {
                    ?>
                    <option value="<?php echo $id; ?>"><?php echo neighborhoodsName($id); ?></option>
                    <?php
                        }
                        $sta->close();
                      }
                    ?>
                  </select>
                </div>
                <div class="col-sm-1">
                  <h5>Período:</h5>
                </div>
                <div class="col-sm-2">
                  <div class="input-group date p">
                    <input id="filter-period" class="form-control input-sm loyalty-control" type="text" 
                    readonly>
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                  </div>
                </div>

                <div class="col-sm-2">
                  <select id="filter-location-country" class="form-control input-sm loyalty-control">
                    <option value="0">País</option>
                    <?php
                      if($sta=$db->prepare('SELECT country_id,country_name FROM  country'))
                      {
                        $sta->execute();
                        $sta->bind_result($id,$name);
         
                        while ($sta->fetch()) 
                        {
                    ?>
                    <option value="<?php echo $id; ?>"><?php echo $name; ?></option>
                    <?php
                        }
                        $sta->close();
                      }
                    ?>
                  </select>
                </div>

                <div class="col-sm-2">
                  <select id="filter-location-city" class="form-control input-sm loyalty-control">
                    <option value="0">Ciudad</option>
                  </select>
                </div>

                <div class="col-sm-2">
                  <select id="filter-route-1" class="form-control input-sm loyalty-control">
                    <option value="0">Ruta</option>
                    <?php
                      if($sta=$db->prepare($routesTab1))
                      {
                        $sta->execute();
                        $sta->bind_result($origin,$destiny);
         
                        while ($sta->fetch()) 
                        {
                    ?>
                    <option value="<?php echo $origin.'-'.$destiny; ?>"><?php echo $origin.'-'.$destiny; ?></option>
                    <?php
                        }
                        $sta->close();
                      }
                    ?>
                  </select>
                </div>
            </div>

            <div class="text-right col-md-12 filter-btn">
                <div class="col-sm-9">
                  <a id="filter-coms-tacs-tab1" href="javascript:void(0)"  class="btn btn-primary btn-sm">Filtrar</a>
                </div>
                <div class="col-sm-1">
                  <a id="clean-coms-tacs-tab1" href="javascript:void(0)" class="btn btn-primary btn-sm">Reset</a>
                </div>
                <div class="col-sm-2">
                  <a href="inc/excelComTactsSMS.php" class="btn btn-primary btn-sm">Descargar Excel</a>
                </div>
            </div>
            <div class="row">
              <table id="top-table1" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>----</th>
                    <th>Recibidos</th>
                    <th>% CLICS</th>
                    <th>Tickets</th>
                    <th>Ingreso</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  ini_set('memory_limit', '-1');
                  if(!isset($_SESSION['query_coms_tacs_0']))
                  {
                   
                    $query="
                      (
                         SELECT
                            tactic as type,
                            SUM(recieved) as total_recieved,
                            ((SUM(opened)*100)/SUM(recieved)) as percent_opened,
                            SUM(tickets) as total_tickets,
                            SUM(revenue) as total_revenue

                          FROM
                          (
                            (SELECT
                                tactic_sms_agent_tactic as tactic,
                                tactic_sms_agent_basic as basic,
                                tactic_sms_agent_recived as recieved, 
                                tactic_sms_agent_clic as opened, 
                                (
                                  SELECT COUNT( DISTINCT report_agent_flight_origin, report_agent_flight_destiny ) AS cont
                                  FROM report_agent
                                  WHERE report_agent_agent = tactic_sms_agent_agent
                                ) as tickets,
                                (
                                  SELECT SUM(report_agent_copa_revenue) AS sm
                                  FROM report_agent
                                  WHERE report_agent_agent =  tactic_sms_agent_agent
                                ) as revenue
                              FROM
                                tactic_communications_sms_agent
                              )UNION(
                              SELECT
                            tactic_sms_agency_tactic as tactic,
                            tactic_sms_agency_basic as basic,
                            tactic_sms_agency_recived as recieved, 
                            tactic_sms_agency_clic as opened, 
                            (
                              SELECT COUNT( DISTINCT report_agent_flight_origin, report_agent_flight_destiny ) AS cont
                              FROM report_agent
                              INNER JOIN agent
                              ON report_agent_agent=agent_id
                              WHERE agent_agency = tactic_sms_agency_agency
                            ) as tickets,
                            (
                              SELECT SUM(report_agent_copa_revenue) AS sm
                              FROM report_agent
                              INNER JOIN agent
                              ON report_agent_agent=agent_id
                              WHERE agent_agency = tactic_sms_agency_agency
                            ) as revenue
                          FROM
                            tactic_communications_sms_agency
                              )
                          ) drv
                          WHERE
                            tactic='1'
                        )UNION(
                         SELECT
                            tactic as type,
                            SUM(recieved) as total_recieved,
                            ((SUM(opened)*100)/SUM(recieved)) as percent_opened,
                            SUM(tickets) as total_tickets,
                            SUM(revenue) as total_revenue

                          FROM
                          (
                            (SELECT
                                tactic_sms_agent_tactic as tactic,
                                tactic_sms_agent_basic as basic,
                                tactic_sms_agent_recived as recieved, 
                                tactic_sms_agent_clic as opened, 
                                (
                                  SELECT COUNT( DISTINCT report_agent_flight_origin, report_agent_flight_destiny ) AS cont
                                  FROM report_agent
                                  WHERE report_agent_agent = tactic_sms_agent_agent
                                ) as tickets,
                                (
                                  SELECT SUM(report_agent_copa_revenue) AS sm
                                  FROM report_agent
                                  WHERE report_agent_agent =  tactic_sms_agent_agent
                                ) as revenue
                              FROM
                                tactic_communications_sms_agent
                              )UNION(
                              SELECT
                            tactic_sms_agency_tactic as tactic,
                            tactic_sms_agency_basic as basic,
                            tactic_sms_agency_recived as recieved, 
                            tactic_sms_agency_clic as opened, 
                            (
                              SELECT COUNT( DISTINCT report_agent_flight_origin, report_agent_flight_destiny ) AS cont
                              FROM report_agent
                              INNER JOIN agent
                              ON report_agent_agent=agent_id
                              WHERE agent_agency = tactic_sms_agency_agency
                            ) as tickets,
                            (
                              SELECT SUM(report_agent_copa_revenue) AS sm
                              FROM report_agent
                              INNER JOIN agent
                              ON report_agent_agent=agent_id
                              WHERE agent_agency = tactic_sms_agency_agency
                            ) as revenue
                          FROM
                            tactic_communications_sms_agency
                              )
                          ) drv

                          WHERE
                           tactic='0'
                        )

                    ";

                    $_SESSION['query_coms_tacs_0']=$query;
                    //echo $query;
                  }
                  else
                  {
                    $query=$_SESSION['query_coms_tacs_0'];
                  }

                  if($sta=$db->prepare($query))
                  {
                    $sta->execute();

                    $sta->bind_result($type,$recieved,$opened,$tickets,$revenue);

                    while ($sta->fetch()) 
                    {
                ?>
                  <tr>
                    <td><?php if($type==1){echo "Tactica"; }else{ echo "Basica"; } ?></td> 
                    <td class="aright"><?php if($recieved==NULL){$recieved='0'; echo $recieved; }else{ echo number_format((float)round($recieved, 2, PHP_ROUND_HALF_ODD), 0, '.', ','); } ?></td> 
                    <td class="aright"><?php if($opened==NULL){$opened='0'; echo $opened; }else{ echo number_format((float)round($opened, 2, PHP_ROUND_HALF_ODD), 0, '.', ','); } ?></td> 
                    <td class="aright"><?php if($tickets==NULL){$tickets='0'; echo $tickets; }else{ echo number_format((float)round($tickets, 2, PHP_ROUND_HALF_ODD), 0, '.', ','); } ?></td> 
                    <td class="aright"><?php if($revenue==NULL){$revenue='0.00'; echo "$ ".$revenue; }else{ echo "$ ".number_format((float)round($revenue, 2, PHP_ROUND_HALF_ODD), 2, '.', ','); } ?></td> 
                  </tr>
                <?php
                    }
                    $sta->close();
                  }
                  
                ?>
                <!-- <tr><td colspan="3"><?php// echo $_SESSION['query_coms_tacs_0']; ?></td></tr> -->
                </tbody>
              </table>
            </div>
            

            <!-- ############################################################################################################## -->

            <h4>Filtros:</h4>
            <div class="text-left col-md-12">

                <div class="col-sm-2">
                  <select id="filter-condition" class="form-control input-sm loyalty-control">
                    <option value="1">Agente</option>
                    <option value="2">Agencia</option>
                  </select>
                </div>

                <div class="col-sm-2">
                  <select id="filter-open-percent" class="form-control input-sm loyalty-control">
                    <option value="0">Porcentaje</option>
                    <option value="1">0% - 60%</option>
                    <option value="2">61% - 70%</option>
                    <option value="3">71% - 80%</option>
                    <option value="4">81% - 90%</option>
                    <option value="5">91% - 100%</option>
                  </select>
                </div>

                <div class="col-sm-2">
                  <select id="filter-location-country-2" class="form-control input-sm loyalty-control">
                    <option value="0">Pais</option>
                    <?php
                      if($sta=$db->prepare('SELECT country_id,country_name FROM  country'))
                      {
                        $sta->execute();
                        $sta->bind_result($id,$name);
         
                        while ($sta->fetch()) 
                        {
                    ?>
                    <option value="<?php echo $id; ?>"><?php echo $name; ?></option>
                    <?php
                        }
                        $sta->close();
                      }
                    ?>
                  </select>
                </div>

                <div class="col-sm-2">
                  <select id="filter-location-city-2" class="form-control input-sm loyalty-control">
                    <option value="0">Ciudad</option>
                  </select>
                </div>

                <div class="col-sm-2">
                  <select id="filter-neighborhood-2" class="form-control input-sm loyalty-control">
                    <option value="0">Vecindario</option>
                    <?php
                      if($sta=$db->prepare('SELECT neighborhood_id,neighborhood_name FROM  neighborhood'))
                      {
                        $sta->execute();
                        $sta->bind_result($id,$name);
         
                        while ($sta->fetch()) 
                        {
                    ?>
                    <option value="<?php echo $id; ?>"><?php echo neighborhoodsName($id); ?></option>
                    <?php
                        }
                        $sta->close();
                      }
                    ?>
                  </select>
                </div>

                <div class="col-sm-2">
                  <select id="filter-route-2" class="form-control input-sm loyalty-control">
                    <option value="0">Ruta</option>
                    <?php
                      if($sta=$db->prepare($sqlAgents))
                      {
                        $sta->execute();
                        $sta->bind_result($origin,$destiny);
         
                        while ($sta->fetch()) 
                        {
                    ?>
                    <option value="<?php echo $origin.'-'.$destiny; ?>"><?php echo $origin.'-'.$destiny; ?></option>
                    <?php
                        }
                        $sta->close();
                      }
                    ?>
                  </select>
                </div>
            </div>

            <div class="text-right col-md-12 filter-btn">
                <div class="col-sm-11">
                  <a id="filter-coms-tacs-tab2" href="javascript:void(0)"  class="btn btn-primary btn-sm">Filtrar</a>
                </div>
                <div class="col-sm-1">
                  <a id="clean-coms-tacs-tab2" href="javascript:void(0)" class="btn btn-primary btn-sm">Reset</a>
                </div>
            </div>

            <div class="row">
              <table id="top-table2" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Agente/Agencia</th>
                    <th>% CLICS</th>
                    <th>Vecindario</th>
                    <th>Pais</th>
                    <th>Ciudad</th>
                    <th>Tickets</th>
                    <th>Ingreso</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  ini_set('memory_limit', '-1');
                  if(!isset($_SESSION['query_coms_tacs_1']))
                  {
                   
                    $query="
                    SELECT
                        code,
                        opening,
                        loyalty,
                        country,
                        city,
                        cityCod,
                        tickets,
                        revenue
                      FROM
                            (
                              SELECT
                                tactic_sms_agent_agent as code,
                                tactic_sms_agent_id as id,
                                ((tactic_sms_agent_clic*100)/tactic_sms_agent_recived) as opening,
                                dn_neighborhood as loyalty,
                                country_name as country,
                                city_name as city,
                                tactic_sms_agent_city as cityCod,
                                (
                                  SELECT COUNT( DISTINCT report_agent_flight_origin, report_agent_flight_destiny ) AS cont
                                  FROM report_agent
                                  WHERE report_agent_agent = tactic_sms_agent_agent
                                ) as tickets,
                                (
                                  SELECT SUM(report_agent_copa_revenue) AS sm
                                  FROM report_agent
                                  WHERE report_agent_agent =  tactic_sms_agent_agent
                                ) as revenue
                              FROM
                                tactic_communications_sms_agent
                              JOIN
                                dashboard_neighborhood_agent
                              ON
                                dn_agent=tactic_sms_agent_agent
                              JOIN
                                country
                              ON
                                country_id=tactic_sms_agent_country
                              JOIN
                                city
                              ON
                                city_id=tactic_sms_agent_city
                              GROUP BY
                                code, id
                            ) as drv2
                    ";

                    $_SESSION['query_coms_tacs_1']=$query;
                    //echo $query;
                  }
                  else
                  {
                    $query=$_SESSION['query_coms_tacs_1'];
                  }

                  if($sta=$db->prepare($query))
                  {
                    $sta->execute();

                    $sta->bind_result($code,$opening,$loyalty,$country,$city,$cityCod,$tickets,$revenue);

                    while ($sta->fetch()) 
                    {
                ?>
                  <tr>
                    <td><?php echo $code; ?></td>
                    <td class="aright"><?php if($opening==NULL){$opening='0'; echo $opening; }else{ echo number_format((float)round($opening, 2, PHP_ROUND_HALF_ODD), 0, '.', ','); } ?></td> 
                    <td><?php if($loyalty==NULL){$loyalty='No Aplica'; echo $loyalty; }else{ echo neighborhoodsName($loyalty); } ?></td> 
                    <td><?php echo $country; ?></td>
                    <td><?php echo $city; ?></td>
                    <td class="aright"><?php if($tickets==NULL){$tickets='0'; echo $tickets; }else{ echo number_format((float)round($tickets, 2, PHP_ROUND_HALF_ODD), 0, '.', ','); } ?></td> 
                    <td class="aright"><?php if($revenue==NULL){$revenue='0.00'; echo "$ ".$revenue; }else{ echo "$ ".number_format((float)round($revenue, 2, PHP_ROUND_HALF_ODD), 2, '.', ','); } ?></td> 
                  </tr>
                <?php
                    }
                    $sta->close();
                  }
                  
                ?>
                <!-- <tr><td colspan="3"><?php //echo $_SESSION['query_coms_tacs_1']; ?></td></tr> -->
                </tbody>
              </table>
            </div>

          </div>
        </div>

        <script type="text/javascript">
          $(document).ready(function(){
            $(document).on('change','#filter-condition', function()
            {
              var c = $(this).val();
              $("#load-anim").fadeIn(100);
              $.ajax({
                type: "POST",
                url: "inc/selected-routes-filters.php",
                data: { tacticsSMS: true, condition: c },
                async: true
              }).done(function(data){
                $('#filter-route-2').html(data);
                $("#load-anim").fadeOut(100);
              });
            });
          });
        </script>