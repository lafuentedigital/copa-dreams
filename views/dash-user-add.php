 <?php
  ini_set('memory_limit', '-1');
  if(isset($_POST['edit']) && $_POST['edit']==1)
  {
    include("../inc/db.php");
    require('../inc/classes/session.class.php');
    $session = new session();
    $session->start_session('_s', false, $db);

    
    $du=null;
    $dc=null;
    $dm=null;

    if($stbb = $db->prepare("SELECT du_user,du_mail,du_capabilities FROM dashboard_users WHERE du_id=?;"))
    {
      $stbb->bind_param('i',$_POST['id']);
      $stbb->execute();
      $stbb->bind_result($du,$dm,$dc);
      $stbb->fetch();
      $stbb->close();
      $edit=true;
    }

    if ($dc=='read,write') {
      $write=true;
    }
    else
    {
      $write=false;
    }
  }
 ?>

 <div class="row">
        
        <?php
          include("sidebar.php");
        ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          
          <h1 class="page-header">Agregar Usuario</h1>
          <form id="data-add-user" name="data-add-user">
            <div class="form-group">
                <label for="user">Usuario:</label>
               
                <div class="input-group">
                  <?php 
                    if(isset($edit))
                    {
                  ?>
                  <input type="text" class="form-control" id="user-" name="user-" value="<?php echo $du; ?>" disabled="disabled">
                  <input type="hidden" value="<?php echo $du; ?>" name="user" id="user" />
                  <?php 
                    }
                    else
                    {
                  ?>
                   <input type="text" class="form-control" id="user" name="user">
                   <?php 
                    }
                  ?>
                </div>
            </div>
            <div class="form-group">
                <label for="email">Correo:</label>
                <div class="input-group">
                  <?php 
                    if(isset($edit))
                    {
                  ?>
                  <input type="text" class="form-control" id="email" name="email" value="<?php echo $dm; ?>">
                  <?php 
                    }
                    else
                    {
                  ?>
                   <input type="text" class="form-control" id="email" name="email">
                   <?php 
                    }
                  ?>
                </div>
            </div>
            <div class="form-group">
                <label for="basic_amount">Clave:</label>
                <div class="input-group">
                  <input type="password" class="form-control" id="pass1" name="pass1">
                </div>
            </div>
            <div class="form-group">
                <label for="basic_amount">Repita Clave:</label>
                <div class="input-group">
                  <input type="password" class="form-control" id="pass2" name="pass2">
                </div>
            </div>
            <div class="form-group">
                <label for="basic_amount">Capacidades:</label>
                <div class="input-group">
                  <?php 
                    if(isset($edit))
                    {
                  ?>
                  <input type="checkbox" value="write" class="capabilities" name="capabilities" <?php if ($write) {echo 'checked="checked"'; }?> /> Escritura
                  <?php 
                    }
                    else
                    {
                  ?>
                  <input type="checkbox" value="write" class="capabilities" name="capabilities" /> Escritura
                  <?php 
                    }
                  ?>
                </div>
            </div>
            <div class="text-right">
              <?php 
                if(isset($edit))
                {
              ?>
              <input type="hidden" value="1" name="edit" id="edit" />
              <input type="hidden" value="<?php echo $_POST['id']; ?>" name="id" id="id" />
              <input type="submit" value="Editar" class="btn btn-primary" />
              <?php 
                }
                else
                {
              ?>
              <input type="submit" value="Agregar" class="btn btn-primary" />
              <?php 
                }
              ?>
            </div>
          </form>
        </div>
      </div>
      <?php 
        if(isset($edit))
        {
      ?>
      <script type="text/javascript">
      $(document).ready(function(){
        
        $("#data-add-user").validate({
          rules: {
              user:{
                required: true
              },
              email: {
                required: true,
                email: true
              },
              pass2:{
                equalTo: "#pass1"
              }
          },
          messages: {
              user:{
                required: ""
              },
              email: {
                required: "",
                email: ""
              },
              pass2:{
                equalTo: ""
              }
          },
          submitHandler: function(form) {
            var dataSend = $('#data-add-user').formSerialize(); 
            $.ajax({
              type:"POST",
              url:"../inc/add-user.php",
              async: true,
              data:dataSend
            }).done(function(response){
              console.log(response);
              if(response==1)
              {
                alert("Datos Actualizados Exitosamente");
                loadPage("#usuarios","#page-loader");
                //form.reset();
              }else if (response=='5') {
                alert("Datos & Contraseña Actualizadas Exitosamente");
                loadPage("#usuarios","#page-loader");
              };
            });
          }
        });

      });
      </script>
      <?php 
        }
        else
        {
      ?>
       <script type="text/javascript">
      $(document).ready(function(){
        
        $("#data-add-user").validate({
          rules: {
              user:{
                required: true
              },
              email: {
                required: true,
                email: true
              },
              pass1:{
                required: true
              },
              pass2:{
                equalTo: "#pass1"
              }
          },
          messages: {
              user:{
                required: ""
              },
              email: {
                required: "",
                email: ""
              },
              pass1:{
                required: ""
              },
              pass2:{
                equalTo: ""
              }
          },
          submitHandler: function(form) {
            var dataSend = $('#data-add-user').formSerialize(); 
            $.ajax({
              type:"POST",
              url:"../inc/add-user.php",
              async: true,
              data:dataSend
            }).done(function(response){
              console.log(response);
              if(response==1)
              {
                form.reset();
              }
            });
          }
        });

      });
      </script>
      <?php 
        }
      ?>