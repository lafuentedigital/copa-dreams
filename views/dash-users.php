 <div class="row">
        
        <?php
          include("sidebar.php");
        ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          
          <h1 class="page-header">Usuarios</h1>
          <div class="text-right">
            <a href="#add-user" class="btn btn-primary dash-link">Agregar Usuario</a>
            <br><br>
          </div>
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>#ID</th>
                <th>Usuario</th>
                <th>Correo</th>
                <th>Capacidades</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
            <?php
              ini_set('memory_limit', '-1');
              if($sta=$db->prepare("SELECT du_id,du_user,du_mail,du_capabilities FROM dashboard_users"))
              {
                $sta->execute();
                $sta->bind_result($id,$user,$mail,$capab);
 
                while ($sta->fetch()) 
                {
            ?>
              <tr>
                <td><?php echo $id; ?></td>
                <td><?php echo $user; ?></td>
                <td><?php echo $mail; ?></td>
                <td><?php echo $capab; ?></td>
                <td><button id="<?php echo $id; ?>" type="button" class="btn btn-primary edit">Editar</button></td>
              </tr>
            <?php
                }
              }
              $sta->close();
            ?>
            </tbody>
          </table>

        </div>
      </div>
      <script type="text/javascript">
      $(document).ready(function(){
        $(document).on('click','.edit', function(e){
          e.preventDefault();
          var id = $(this).attr("id");
          loadPage("0","#page-loader","0","views/dash-user-add.php","&edit=1&id="+id);
        });
      });
      </script>