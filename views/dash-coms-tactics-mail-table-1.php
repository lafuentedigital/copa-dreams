<?php
 include('vfunctions.php');
?>
                <thead>
                  <tr>
                    <th>Agente/Agencia</th>
                    <th>% Abiertos</th>
                    <th>Vecindario</th>
                    <th>Pais</th>
                    <th>Ciudad</th>
                    <th>Tickets</th>
                    <th>Ingreso</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  ini_set('memory_limit', '-1');
                  if(!isset($_SESSION['query_coms_tac_1']))
                  {
                   
                    $query="
                       SELECT
                        code,
                        opening,
                        loyalty,
                        country,
                        city,
                        cityCod,
                        tickets,
                        revenue
                      FROM
                            (
                              SELECT
                                tactic_email_agent_agent as code,
                                tactic_email_agent_id as id,
                                ((tactic_email_agent_opened*100)/tactic_email_agent_recived) as opening,
                                dn_neighborhood as loyalty,
                                country_name as country,
                                city_name as city,
                                tactic_email_agent_city as cityCod,
                                (
                                  SELECT COUNT( DISTINCT report_agent_flight_origin, report_agent_flight_destiny ) AS cont
                                  FROM report_agent
                                  WHERE report_agent_agent = tactic_email_agent_agent
                                ) as tickets,
                                (
                                  SELECT SUM(report_agent_copa_revenue) AS sm
                                  FROM report_agent
                                  WHERE report_agent_agent =  tactic_email_agent_agent
                                ) as revenue
                              FROM
                                tactic_communications_email_agent
                              JOIN
                                dashboard_neighborhood_agent
                              ON
                                dn_agent=tactic_email_agent_agent
                              JOIN
                                country
                              ON
                                country_id=tactic_email_agent_country
                              JOIN
                                city
                              ON
                                city_id=tactic_email_agent_city
                              GROUP BY
                                code, id
                            ) as drv2
                    ";

                    $_SESSION['query_coms_tac_1']=$query;
                    //echo $query;
                  }
                  else
                  {
                    $query=$_SESSION['query_coms_tac_1'];
                  }

                  if($sta=$db->prepare($query))
                  {
                    $sta->execute();

                    $sta->bind_result($code,$opening,$loyalty,$country,$city,$cityCod,$tickets,$revenue);

                    while ($sta->fetch()) 
                    {
                ?>
                  <tr>
                    <td><?php echo $code; ?></td>
                    <td class="aright"><?php if($opening==NULL){$opening='0'; echo $opening; }else{ echo number_format((float)round($opening, 2, PHP_ROUND_HALF_ODD), 0, '.', ','); } ?></td> 
                    <td><?php if($loyalty==NULL){$loyalty='No Aplica'; echo $loyalty; }else{ echo neighborhoodsName($loyalty); } ?></td> 
                    <td><?php echo $country; ?></td>
                    <td><?php echo $city; ?></td>
                    <td class="aright"><?php if($tickets==NULL){$tickets='0'; echo $tickets; }else{ echo number_format((float)round($tickets, 2, PHP_ROUND_HALF_ODD), 0, '.', ','); } ?></td> 
                    <td class="aright"><?php if($revenue==NULL){$revenue='0.00'; echo "$ ".$revenue; }else{ echo "$ ".number_format((float)round($revenue, 2, PHP_ROUND_HALF_ODD), 2, '.', ','); } ?></td> 
                  </tr>
                <?php
                    }
                    $sta->close();
                  }
                  
                ?>
                <!-- <tr><td colspan="3"><?php echo $_SESSION['query_coms_tac_1']; ?></td></tr> -->
                </tbody>