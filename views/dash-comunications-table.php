<?php
 include('vfunctions.php');
?>
                <thead>
                  <tr>
                    <th>----</th>
                    <th>Recibidos</th>
                    <th>% Abiertos</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  ini_set('memory_limit', '-1');
                  if(!isset($_SESSION['query_coms']))
                  {
                   
                    $query="
                      SELECT
                              campaign, SUM(recieved) as total_recieved, (SUM(opened)*100/SUM(recieved)) as percent_opened
                      FROM
                              (
                                      (SELECT 
                                              agent_id,agent_agency,basic_email_agent_campaign as campaign, basic_email_agent_recived as recieved, basic_email_agent_opened as opened
                                      FROM
                                              agent
                                      INNER JOIN
                                              basic_communications_email_agent
                                      ON
                                              agent_id=basic_email_agent_agent
                                      )
                                      UNION
                                      (SELECT 
                                              agent_id,agent_agency,basic_email_agency_campaign as campaign, basic_email_agency_recived as recieved, basic_email_agency_opened as opened
                                      FROM
                                              agent
                                      INNER JOIN
                                              basic_communications_email_agency
                                      ON
                                              agent_agency=basic_email_agency_agency
                                      GROUP BY
                                              agent_agency
                                      )
                              ) as drv1
                      GROUP BY
                              campaign
                    ";

                    $_SESSION['query_coms']=$query;
                    //echo $query;
                  }
                  else
                  {
                    $query=$_SESSION['query_coms'];
                  }

                  if($sta=$db->prepare($query))
                  {
                    $sta->execute();

                    $sta->bind_result($campaign,$recieved,$opened);

                    while ($sta->fetch()) 
                    {
                ?>
                  <tr>
                    <td><?php echo $campaign; ?></td>
                    <td class="aright"><?php if($recieved==NULL){$recieved='0'; echo $recieved; }else{ echo number_format((float)round($recieved, 2, PHP_ROUND_HALF_ODD), 0, '.', ','); } ?></td> 
                    <td class="aright"><?php if($opened==NULL){$opened='0'; echo $opened; }else{ echo number_format((float)round($opened, 2, PHP_ROUND_HALF_ODD), 0, '.', ','); } ?></td> 
                  </tr>
                <?php
                    }
                    $sta->close();
                  }
                  
                ?>
                <!-- <tr><td colspan="3"><?php //echo $_SESSION['query_coms']; ?></td></tr> -->
                </tbody>
              </table>