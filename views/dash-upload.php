      <div class="row">
        
        <?php
          include("sidebar.php");
        ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <?php

        if(in_array('write', $_SESSION['capabilities']))
        {

        ?>
          <h1 class="page-header">Cargar Datos</h1>

          <div class="row placeholders">
             
            <a href="#kpi-load" class="dash-link">
              <div class="col-xs-6 col-sm-3 dash-button dash-button-kpi">
                <h3>KPI</h3>
              </div>
            </a>

            <a href="#agent-load" class="dash-link">
              <div class="col-xs-6 col-sm-3 dash-button dash-button-agency">
                <h3>Reporte de Agente</h3>
              </div>
            </a>
            
            <a href="#agency-load" class="dash-link">
              <div class="col-xs-6 col-sm-3 dash-button dash-button-agency">
                <h3>Reporte de Agencias</h3>
              </div>
            </a>

            <a href="#agent-commail-b-load" class="dash-link">
              <div class="col-xs-6 col-sm-3 dash-button dash-button-effective">
                <h3>Basica Agente (Mail)</h3>
              </div>
            </a>

            <a href="#agency-commail-b-load" class="dash-link">
              <div class="col-xs-6 col-sm-3 dash-button dash-button-effective">
                <h3>Basica Agencia (Mail)</h3>
              </div>
            </a>

            <a href="#agent-commail-t-load" class="dash-link">
              <div class="col-xs-6 col-sm-3 dash-button dash-button-effective">
                <h3>Tactica Agente (Mail)</h3>
              </div>
            </a>

            <a href="#agency-commail-t-load" class="dash-link">
              <div class="col-xs-6 col-sm-3 dash-button dash-button-effective">
                <h3>Tactica Agencia (Mail)</h3>
              </div>
            </a>

            <a href="#agent-comsms-t-load" class="dash-link">
              <div class="col-xs-6 col-sm-3 dash-button dash-button-effective">
                <h3>Tactica Agente (SMS)</h3>
              </div>
            </a>

            <a href="#agency-comsms-t-load" class="dash-link">
              <div class="col-xs-6 col-sm-3 dash-button dash-button-effective">
                <h3>Tactica Agencia (SMS)</h3>
              </div>
            </a>

          </div>
        <?php

        }
        else
        {

        ?>
        <h1 class="page-header">No tienes permiso para ver esta pagina.</h1>
        <?php

        }

        ?>
        </div>
      </div>