<?php
 include('vfunctions.php');
?>
          <thead>
            <tr>
              <th>Vecindario</th>
              <th>Total Agentes</th>
              <th>Total Ingresos Copa</th>
              <th>Cantidad de Tickets</th>
            </tr>
          </thead>
          <tbody>
          <?php
            ini_set('memory_limit', '-1');
            if(!isset($_SESSION['query']))
            {
              $sql="
                  SELECT
                    dn_month,dn_year
                  FROM
                    dashboard_neighborhood_agent
                  ORDER BY
                    dn_year DESC,
                    dn_month DESC
                  LIMIT 0,1
              ";
              if($sta=$db->prepare($sql))
              {
                $sta->execute();
                $sta->bind_result($m,$y);
                $sta->fetch();
                $sta->close();
              }

              $query="
                  SELECT
                    (SELECT city_name FROM city INNER JOIN agent ON city_id = agent_city_agency WHERE agent_id=dn_agent) as dn_city,
                     dn_neighborhood,
                     dn_date_record,
                     dn_revenue,
                     dn_routes_sold,
                     dn_month,
                     dn_year,
                     dn_agent
                  FROM
                    dashboard_neighborhood_agent
                  INNER JOIN
                      (
                        SELECT 
                          agent_id, agent_city_agency
                        FROM 
                                    agent
                              INNER JOIN 
                                    report_agent 
                              ON 
                                    agent_id=report_agent_agent
                        ) as sb1 
                  ON 
                        dn_agent=sb1.agent_id
                  WHERE
                    (dn_month = '".$m."') AND (dn_year = '".$y."')
                  GROUP BY
                    dn_agent
                  ORDER BY
                    dn_year DESC,
                    dn_month DESC
              ";

              $_SESSION['query']=$query;
              //echo $query;
            }
            else
            {
              $query=$_SESSION['query'];
            }

            $nt= array();

            if($sta=$db->prepare($query))
            {
              $sta->execute();

              $sta->bind_result($city,$neighborhood,$dateRecord,$revenue,$routes,$month,$year,$code);

              while ($sta->fetch()) 
              {
                $nt[$neighborhood]['title']=$neighborhood;
                $nt[$neighborhood]['code'][$code]=$code;
                $nt[$neighborhood]['revenue']+=$revenue;
                $nt[$neighborhood]['routes']+=$routes;
              } 
              $sta->close();
            }
            //echo $_SESSION['query'];
          ?>
            <tr>
              <td><?php if(empty($nt['V8']['title'])){echo neighborhoodsName('V8'); }else{ echo neighborhoodsName($nt['V8']['title']); } ?></td>
              <td class="aright"><?php echo number_format((float)round(sizeof($nt['V8']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
              <td class="aright"><?php echo "$ ".number_format((float)round($nt['V8']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
              <td class="aright"><?php echo number_format((float)round($nt['V8']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
            </tr>
            <tr>
              <td><?php if(empty($nt['V7']['title'])){echo neighborhoodsName('V7'); }else{ echo neighborhoodsName($nt['V7']['title']); } ?></td>
              <td class="aright"><?php echo number_format((float)round(sizeof($nt['V7']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
              <td class="aright"><?php echo "$ ".number_format((float)round($nt['V7']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
              <td class="aright"><?php echo number_format((float)round($nt['V7']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
            </tr>
            <tr>
              <td><?php if(empty($nt['V6']['title'])){echo neighborhoodsName('V6'); }else{ echo neighborhoodsName($nt['V6']['title']); } ?></td>
              <td class="aright"><?php echo number_format((float)round(sizeof($nt['V6']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
              <td class="aright"><?php echo "$ ".number_format((float)round($nt['V6']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
              <td class="aright"><?php echo number_format((float)round($nt['V6']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
            </tr>
            <tr>
              <td><?php if(empty($nt['V5']['title'])){echo neighborhoodsName('V5'); }else{ echo neighborhoodsName($nt['V5']['title']); } ?></td>
              <td class="aright"><?php echo number_format((float)round(sizeof($nt['V5']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
              <td class="aright"><?php echo "$ ".number_format((float)round($nt['V5']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
              <td class="aright"><?php echo number_format((float)round($nt['V5']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
            </tr>
            <tr>
              <td><?php if(empty($nt['V4']['title'])){echo neighborhoodsName('V4'); }else{ echo neighborhoodsName($nt['V4']['title']); } ?></td>
              <td class="aright"><?php echo number_format((float)round(sizeof($nt['V4']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
              <td class="aright"><?php echo "$ ".number_format((float)round($nt['V4']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
              <td class="aright"><?php echo number_format((float)round($nt['V4']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
            </tr>
            <tr>
              <td><?php if(empty($nt['V3']['title'])){echo neighborhoodsName('V3'); }else{ echo neighborhoodsName($nt['V3']['title']); } ?></td>
              <td class="aright"><?php echo number_format((float)round(sizeof($nt['V3']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
              <td class="aright"><?php echo "$ ".number_format((float)round($nt['V3']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
              <td class="aright"><?php echo number_format((float)round($nt['V3']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
            </tr>
            <tr>
              <td><?php if(empty($nt['V2']['title'])){echo neighborhoodsName('V2'); }else{ echo neighborhoodsName($nt['V2']['title']); } ?></td>
              <td class="aright"><?php echo number_format((float)round(sizeof($nt['V2']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
              <td class="aright"><?php echo "$ ".number_format((float)round($nt['V2']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
              <td class="aright"><?php echo number_format((float)round($nt['V2']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
            </tr>
            <tr>
              <td><?php if(empty($nt['V1']['title'])){echo neighborhoodsName('V1'); }else{ echo neighborhoodsName($nt['V1']['title']); } ?></td>
              <td class="aright"><?php echo number_format((float)round(sizeof($nt['V1']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
              <td class="aright"><?php echo "$ ".number_format((float)round($nt['V1']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
              <td class="aright"><?php echo number_format((float)round($nt['V1']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
            </tr>
           <!-- <tr><td colspan="4"><?php echo $_SESSION['query']; ?></td></tr> -->
          </tbody>