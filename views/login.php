    <style type="text/css">
	    #response {
	    	text-align: left;
			float: right;
			margin-top: 10px;
	    }

	    #login {
	    	float: left;
	    }

	    #load-anim {
	    	color: #005993 !important;
	    }
	    #response-error {
	    	display: none;
	    	color: #FA0000 !important;
	    }
    </style>

    <div class="container">
      <form class="form-signin" role="form">
        <h2 class="form-signin-heading center">Ingresar al Sistema</h2>
        <input id="user" type="text" class="form-control" placeholder="Usuario" required="" autofocus="">
        <input id="pass" type="password" class="form-control" placeholder="Contraseña" required="">
        <button id="login" class="btn btn-primary" type="submit">Entrar</button>
        <div id="response">
        	<div id="response-error"></div>
      		<div id="load-anim" >Accesando...</div>
      	</div>
      </form>
    </div>

    <script type="text/javascript">

		$( document ).ready(function() {

			$('#login').click(function(e){
				e.preventDefault();
				$('#response-error').fadeOut('fast');
				$('#load-anim').fadeIn(800);
				$.ajax({
					type: "POST",
					url: "inc/set-login.php",
					data: { user:$('#user').val(), pass:$('#pass').val()},
					async: true
				}).done(function(data){
					//console.log(data);
					if(data==1){
						location.reload();
					}
					else
					{
						$('#load-anim').fadeOut(100);
						if(data==0)
						{
							$('#response-error').html('Usuario o contraseña incorrecta.');
						}
						else if(data==2)
						{
							$('#response-error').html('Introduzca su nombre de Usuario.');	
						}
						else if(data==3)
						{
							$('#response-error').html('Introduzca su contraseña.');	
						}
						$('#response-error').fadeIn(1000);
					}
				});
			});
		});

    </script>