      <div class="row">
        
        <?php
          include("sidebar.php");
        ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          
          <h1 class="page-header">Dashboard</h1>

          <div class="row placeholders">
            
            <a href="#kpi" class="dash-link">
              <div class="col-xs-6 col-sm-3 dash-button dash-button-kpi">
                <h3>KPI</h3>
              </div>
            </a>

            <a href="#loyalty" class="dash-link">
              <div class="col-xs-6 col-sm-3 dash-button dash-button-kpi">
                <h3>Loyalty Framework</h3>
              </div>
            </a>

            <a href="#top-agencies" class="dash-link">
              <div class="col-xs-6 col-sm-3 dash-button dash-button-agency">
                <h3>Top Agencias</h3>
              </div>
            </a>

            <a href="#top-agent" class="dash-link">
              <div class="col-xs-6 col-sm-3 dash-button dash-button-agency">
                <h3>Top Agentes</h3>
              </div>
            </a>

            <a href="#coms" class="dash-link">
              <div class="col-xs-6 col-sm-3 dash-button dash-button-effective">
                <h3>Basicas</h3>
              </div>
            </a>

            <a href="#coms-tmail" class="dash-link">
              <div class="col-xs-6 col-sm-3 dash-button dash-button-effective">
                <h3>Tacticas Mail</h3>
              </div>
            </a>

            <a href="#coms-tsms" class="dash-link">
              <div class="col-xs-6 col-sm-3 dash-button dash-button-effective">
                <h3>Tacticas SMS</h3>
              </div>
            </a>

            <a href="#routes-growth" class="dash-link">
              <div class="col-xs-6 col-sm-3 dash-button dash-button-graph">
                <h3>Crecimiento de Rutas</h3>
              </div>
            </a>

            <a href="#neighborhood" class="dash-link">
              <div class="col-xs-6 col-sm-3 dash-button dash-button-graph">
                <h3>Vecindarios</h3>
              </div>
            </a>

            <a href="#program" class="dash-link">
              <div class="col-xs-6 col-sm-3 dash-button dash-button-graph">
                <h3>Efectividad del Programa</h3>
              </div>
            </a>
        <?php

          if(in_array('write', $_SESSION['capabilities']))
          {

        ?>
            <a href="#usuarios" class="dash-link">
              <div class="col-xs-6 col-sm-3 dash-button dash-button-load">
                <h3>Usuarios</h3>
              </div>
            </a>

            <a href="#cargar-datos" class="dash-link">
              <div class="col-xs-6 col-sm-3 dash-button dash-button-load">
                <h3>Cargar Datos</h3>
              </div>
            </a>
        <?php

          }

        ?>

          </div>

        </div>
      </div>