      <div class="row">
        
        <?php
          include("sidebar.php");
        ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-9 col-md-offset-2 main">
          
          <h1 class="page-header">Cargar Kpi</h1>

          <form id="data-kpi-submit" name="data-kpi-submit">
            <div class="form-group">
              <label for="kpi_date">Fecha: </label>
              <div class="input-group date p">
                <input id="kpi_date" name="kpi_date" class="form-control input-sm loyalty-control" type="text" readonly>
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
              </div>
            </div>
            <div class="form-group">
              <label for="treatment">Tratamiento: </label>
              <select id="treatment" name="treatment" class="form-control">
                <option value="0">Ambos</option>
                <option value="1">Con Tratamiento</option>
                <option value="2">Sin Tratamiento</option>
              </select>
            </div>
            <div class="form-group">
              <label for="basic_amount">Ruta Basica: </label>
              <div class="input-group">
                <input type="text" class="form-control" id="basic_amount" name="basic_amount">
                <span class="input-group-addon">$</span>
              </div>
            </div>
            <div class="form-group">
              <label for="basic_ticket">Ruta Basica: </label>
              <div class="input-group">
                <input type="text" class="form-control" id="basic_ticket" name="basic_ticket">
                <span class="input-group-addon">Ticket</span>
              </div>
            </div>
            <div class="form-group">
              <label for="tactic_amount">Ruta Tactica: </label>
              <div class="input-group">
                <input type="text" class="form-control" id="tactic_amount" name="tactic_amount">
                <span class="input-group-addon">$</span>
              </div>
            </div>
            <div class="form-group">
              <label for="tactic_ticket">Ruta Tactica: </label>
              <div class="input-group">
                <input type="text" class="form-control" id="tactic_ticket" name="tactic_ticket">
                <span class="input-group-addon">Ticket</span>
              </div>
            </div>
            <div class="form-group">
              <label for="copa_revenue">Ingreso Copa: </label>
              <div class="input-group">
                <input type="text" class="form-control" id="copa_revenue" name="copa_revenue">
                <span class="input-group-addon">$</span>
              </div>
            </div>
            <div class="form-group">
              <label for="roi">ROI: </label>
              <div class="input-group">
                <input type="text" class="form-control" id="roi" name="roi">
                <span class="input-group-addon">$</span>
              </div>
            </div>
            <div class="form-group">
              <label for="cost_amount">Costos: </label>
              <div class="input-group">
                <input type="text" class="form-control" id="cost_amount" name="cost_amount">
                <span class="input-group-addon">$</span>
              </div>
            </div>
            <input type="submit" value="Cargar" name="submitKpi" id="submitKpi" class="btn btn-primary">
          </form>
          <div id="responsesubmit"></div>
          </div>
      </div>
      <script type="text/javascript">
      $(document).ready(function(){
        
        $("#data-kpi-submit").validate({
          rules: {
              kpi_date:{
                required: true
              },
              treatment:{
                required: true
              },
              basic_amount:{
                required: true
              },
              basic_ticket:{
                required: true
              },
              tactic_amount:{
                required: true
              },
              tactic_ticket:{
                required: true
              },
              copa_revenue:{
                required: true
              },
              roi:{
                required: true
              },
              cost_amount:{
                required: true
              }
          },
          messages: {
              kpi_date:{
                required: ""
              },
              treatment:{
                required: ""
              },
              basic_amount:{
                required: ""
              },
              basic_ticket:{
                required: ""
              },
              tactic_amount:{
                required: ""
              },
              tactic_ticket:{
                required: ""
              },
              copa_revenue:{
                required: ""
              },
              roi:{
                required: ""
              },
              cost_amount:{
                required: ""
              }
          },
          submitHandler: function(form) {
            var dataSend = $('#data-kpi-submit').formSerialize(); 
            $.ajax({
              type:"POST",
              url:"inc/kpi-request.php",
              async: false,
              data:dataSend
            }).done(function(response){
              //console.log(response);
              if(response==1)
              {
                alert("KPI's Agregados Correctamente");
                form.reset();
              }
              else
              {
                alert(response);
              }
            });
          }
        });

      });
      </script>