     <?php
        include("../inc/functions.php");
     ?>
     <div class="row">
        
        <?php
          include("sidebar.php");
        ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          
          <h1 class="page-header">KPI</h1>
          <h4>Filtros:</h4>
          
          <div class="text-left col-md-9">

            <div class="row">

              <div class="col-sm-3">
                <select id="filter-q" class="form-control input-sm loyalty-control">
                    <option value="0">Q</option>
                    <option value="Q1">Q1 (ENE, FEB, MAR)</option>
                    <option value="Q2">Q2 (ABR, MAY, JUN)</option>
                    <option value="Q3">Q3 (JUL, AGO, SEP)</option>
                    <option value="Q4">Q4 (OCT, NOV, DIC)</option>
                  </select>
              </div>
              <div class="col-sm-1">
                <h5>Período:</h5>
              </div>
              <div class="col-sm-3">
                <div class="input-group date p">
                  <input id="filter-period-start" class="form-control input-sm loyalty-control" type="text" 
                  readonly>
                  <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
              </div>
            </div>
          </div>

            <div class="text-right col-md-12 filter-btn">
              <div class="col-sm-9">
                  <a id="filter-kpi" href="javascript:void(0)"  class="btn btn-primary btn-sm">Filtrar</a>
                </div>
                <div class="col-sm-1">
                  <a id="clean-kpi" href="javascript:void(0)" class="btn btn-primary btn-sm">Reset</a>
                </div>
                <div class="col-sm-2">
                  <a href="inc/excelKPI.php" class="btn btn-primary btn-sm">Descargar Excel</a>
                </div>
            </div>
           <table id="table-kpi" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>KPI's</th>
                <th>GRUPO (RM)</th>
                <th>GRUPO (CTRL)</th>
                <th>CUMPLIMIENTO KPI's</th>
              </tr>
            </thead>
            <tbody>
            <?php
              ini_set('memory_limit', '-1');
              //unset($_SESSION['kpi_array']);

              if(!isset($_SESSION['kpi_array']))
              {
                $today=date('Y-m-d');
                $todayM=(int)date('m');
                $todayY=(int)date('Y');
                $todayPY=$todayY-1;
                $todayD=(int)date('d');

                $kpi=array();

                //CONSULTAS

                $sqlAgentGRM="
                SELECT 
                  COUNT(DISTINCT report_agent_agent) 
                FROM 
                  report_agent
                WHERE
                  (report_agent_agent IN (SELECT basic_email_agent_agent FROM basic_communications_email_agent) OR
                  report_agent_agent IN (SELECT tactic_email_agent_agent FROM tactic_communications_email_agent) OR
                  report_agent_agent IN (SELECT tactic_sms_agent_agent FROM tactic_communications_sms_agent))
                ";

                $sqlAgentGCTRL="
                SELECT 
                  COUNT(DISTINCT report_agent_agent) 
                FROM 
                  report_agent
                WHERE
                  (report_agent_agent NOT IN (SELECT basic_email_agent_agent FROM basic_communications_email_agent) AND
                  report_agent_agent NOT IN (SELECT tactic_email_agent_agent FROM tactic_communications_email_agent) AND
                  report_agent_agent NOT IN (SELECT tactic_sms_agent_agent FROM tactic_communications_sms_agent))
                ";

                $sqlAgentTotal="
                SELECT 
                  COUNT(DISTINCT report_agent_agent) 
                FROM 
                  report_agent
                ";

                $sqlRevenueGRM="
                SELECT 
                  SUM(report_agent_copa_revenue) 
                FROM 
                  report_agent
                WHERE
                  (report_agent_agent IN (SELECT basic_email_agent_agent FROM basic_communications_email_agent) OR
                  report_agent_agent IN (SELECT tactic_email_agent_agent FROM tactic_communications_email_agent) OR
                  report_agent_agent IN (SELECT tactic_sms_agent_agent FROM tactic_communications_sms_agent))
                ";

                $sqlRevenueGCTRL="
                SELECT 
                  SUM(report_agent_copa_revenue) 
                FROM 
                  report_agent
                WHERE
                  (report_agent_agent NOT IN (SELECT basic_email_agent_agent FROM basic_communications_email_agent) AND
                  report_agent_agent NOT IN (SELECT tactic_email_agent_agent FROM tactic_communications_email_agent) AND
                  report_agent_agent NOT IN (SELECT tactic_sms_agent_agent FROM tactic_communications_sms_agent))
                ";

                $sqlRevenueTotal="
                SELECT 
                  SUM(report_agent_copa_revenue) 
                FROM 
                  report_agent
                ";

                $sqlTicketGRM="
                SELECT 
                  COUNT(DISTINCT report_agent_flight_origin,report_agent_flight_destiny)
                FROM 
                  report_agent
                WHERE
                  (report_agent_agent IN (SELECT basic_email_agent_agent FROM basic_communications_email_agent) OR
                  report_agent_agent IN (SELECT tactic_email_agent_agent FROM tactic_communications_email_agent) OR
                  report_agent_agent IN (SELECT tactic_sms_agent_agent FROM tactic_communications_sms_agent))
                ";

                $sqlTicketGCTRL="
                SELECT 
                  COUNT(DISTINCT report_agent_flight_origin,report_agent_flight_destiny)
                FROM 
                  report_agent
                WHERE
                  (report_agent_agent NOT IN (SELECT basic_email_agent_agent FROM basic_communications_email_agent) AND
                  report_agent_agent NOT IN (SELECT tactic_email_agent_agent FROM tactic_communications_email_agent) AND
                  report_agent_agent NOT IN (SELECT tactic_sms_agent_agent FROM tactic_communications_sms_agent))
                ";

                $sqlTicketTotal="
                SELECT 
                  COUNT(DISTINCT report_agent_flight_origin,report_agent_flight_destiny)
                FROM 
                  report_agent
                ";

                $sqlWings="
                SELECT
                  SUM((report_agent_white_wings+report_agent_white_plus+report_agent_blue_wings)*4000)
                FROM 
                  report_agent
                ";

                if($sta=$db->prepare($sqlAgentTotal))
                {
                  $sta->execute();
                  $sta->bind_result($agentTotal);
                  $sta->fetch();
                  $sta->close();
                }

                if($sta=$db->prepare($sqlAgentGRM))
                {
                  $sta->execute();
                  $sta->bind_result($agentGRM);
                  $sta->fetch();

                  $kpi['compromiso']['grm']=(($agentGRM/$agentTotal)*100);

                  $sta->close();
                }

                if($sta=$db->prepare($sqlAgentGCTRL))
                {
                  $sta->execute();
                  $sta->bind_result($agentGCTRL);
                  $sta->fetch();

                  $kpi['compromiso']['gctrl']=(($agentGCTRL/$agentTotal)*100);
                  
                  $sta->close();
                }

                if($kpi['compromiso']['grm']>$kpi['compromiso']['gctrl']){
                  $kpi['compromiso']['cumplimiento']='#00FF00';
                }
                else if($kpi['compromiso']['grm']<$kpi['compromiso']['gctrl']){
                  $kpi['compromiso']['cumplimiento']='#FF0000';
                }
                else if($kpi['compromiso']['grm']==$kpi['compromiso']['gctrl']){
                  $kpi['compromiso']['cumplimiento']='#FFFF00';
                }

                if($sta=$db->prepare($sqlRevenueTotal))
                {
                  $sta->execute();
                  $sta->bind_result($revenueTotal);
                  $sta->fetch();
                  $sta->close();
                }

                if($sta=$db->prepare($sqlRevenueGRM))
                {
                  $sta->execute();
                  $sta->bind_result($revenueGRM);
                  $sta->fetch();

                  $kpi['conducta']['grm']=(($revenueGRM/$revenueTotal)*100);

                  $sta->close();
                }

                if($sta=$db->prepare($sqlRevenueGCTRL))
                {
                  $sta->execute();
                  $sta->bind_result($revenueGCTRL);
                  $sta->fetch();

                  $kpi['conducta']['gctrl']=(($revenueGCTRL/$revenueTotal)*100);
                  
                  $sta->close();
                }

                if($kpi['conducta']['grm']>$kpi['conducta']['gctrl']){
                  $kpi['conducta']['cumplimiento']='#00FF00';
                }
                else if($kpi['conducta']['grm']<$kpi['conducta']['gctrl']){
                  $kpi['conducta']['cumplimiento']='#FF0000';
                }
                else if($kpi['conducta']['grm']==$kpi['conducta']['gctrl']){
                  $kpi['conducta']['cumplimiento']='#FFFF00';
                }

                if($sta=$db->prepare($sqlTicketTotal))
                {
                  $sta->execute();
                  $sta->bind_result($ticketTotal);
                  $sta->fetch();
                  $sta->close();
                }

                if($sta=$db->prepare($sqlTicketGRM))
                {
                  $sta->execute();
                  $sta->bind_result($ticketGRM);
                  $sta->fetch();

                  $kpi['involucramiento']['grm']=(($ticketGRM/$ticketTotal)*100);

                  $sta->close();
                }

                if($sta=$db->prepare($sqlTicketGCTRL))
                {
                  $sta->execute();
                  $sta->bind_result($ticketGCTRL);
                  $sta->fetch();

                  $kpi['involucramiento']['gctrl']=(($ticketGCTRL/$ticketTotal)*100);
                  
                  $sta->close();
                }

                if($kpi['involucramiento']['grm']>$kpi['involucramiento']['gctrl']){
                  $kpi['involucramiento']['cumplimiento']='#00FF00';
                }
                else if($kpi['involucramiento']['grm']<$kpi['involucramiento']['gctrl']){
                  $kpi['involucramiento']['cumplimiento']='#FF0000';
                }
                else if($kpi['involucramiento']['grm']==$kpi['involucramiento']['gctrl']){
                  $kpi['involucramiento']['cumplimiento']='#FFFF00';
                }

                if($sta=$db->prepare($sqlWings))
                {
                  $sta->execute();
                  $sta->bind_result($wingsTotal);
                  $sta->fetch();

                  $kpi['rate']['real']=(($wingsTotal/$revenueTotal)*100);

                  $kpi['rate']['dif']=$kpi['rate']['real']-6;

                  $sta->close();
                }

                if($kpi['rate']['dif']>0){
                  $kpi['rate']['cumplimiento']='#00FF00';
                }
                else if($kpi['rate']['dif']<0){
                  $kpi['rate']['cumplimiento']='#FF0000';
                }
                else if($kpi['rate']['dif']==0){
                  $kpi['rate']['cumplimiento']='#FFFF00';
                }

                $_SESSION['kpi_array']=$kpi;
              }
              else
              {
                $kpi=$_SESSION['kpi_array'];
              }

            ?>
              <tr>
                <td>COMPROMISO</td>
                <td class="aright"><?php echo number_format((float)round($kpi['compromiso']['grm'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?>%</td>
                <td class="aright"><?php echo number_format((float)round($kpi['compromiso']['gctrl'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?>%</td>
                <td class="aright" style="background-color:<?php echo  $kpi['compromiso']['cumplimiento']; ?> !important"></td>
              </tr>
              <tr>
                <td>CONDUCTA DESEADA</td>
                <td class="aright"><?php echo number_format((float)round($kpi['conducta']['grm'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?>%</td>
                <td class="aright"><?php echo number_format((float)round($kpi['conducta']['gctrl'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?>%</td>
                <td class="aright" style="background-color:<?php echo  $kpi['conducta']['cumplimiento']; ?> !important"></td>
              </tr>
              <tr>
                <td>INVOLUCRAMIENTO</td>
                <td class="aright"><?php echo number_format((float)round($kpi['involucramiento']['grm'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?>%</td>
                <td class="aright"><?php echo number_format((float)round($kpi['involucramiento']['gctrl'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?>%</td>
                <td class="aright" style="background-color:<?php echo  $kpi['involucramiento']['cumplimiento']; ?> !important"></td>
              </tr>
            </tbody>
          </table>
          <table id="table-kpi2" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>KPI's</th>
                <th>META</th>
                <th>REAL</th>
                <th>% DIFERENCIA</th>
                <th>CUMPLIMIENTO KPI's</th>
              </tr>
            </thead>
            <tbody>
            <tr>
                <td>RATE DE PAGO</td>
                <td class="aright">6%</td>
                <td class="aright"><?php echo number_format((float)round($kpi['rate']['real'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?>%</td>
                <td class="aright"><?php echo number_format((float)round($kpi['rate']['dif'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?>%</td>
                <td class="aright" style="background-color:<?php echo  $kpi['rate']['cumplimiento']; ?> !important"></td>
              </tr>
            </tbody>
          </table>
          <?php //var_dump($kpi); ?>
        </div>
      </div>
      <script type="text/javascript">
        $(document).ready(function(){
          $(document).on('change','#filter-q', function(){
            if($(this).val()!=0)
            {
              $('#filter-period-start').val("");
            }
          });
          $(document).on('change','#filter-period-start', function(){
            $("#filter-q").val(0).change();
          });
        });
        </script>