<?php
 include('vfunctions.php');
?>
<div class="row">
        
<?php
  include("sidebar.php");
?>

  <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    
    <h1 class="page-header">Loyalty Framework</h1>
    <h4>Filtros:</h4>
      <div class="text-left col-md-9">

        <div class="row" style="padding-bottom: 8px">

          <div class="col-sm-2">
            <select id="filter-condition" class="form-control input-sm loyalty-control">
              <option value="1">Agente</option>
              <option value="2">Agencia</option>
            </select>
          </div>

          <div class="col-sm-3">
            <select id="filter-treatment" class="form-control input-sm loyalty-control">
              <option value="0">Ambos Grupos</option>
              <option value="1">Grupo RM</option>
              <option value="2">Grupo Ctrl</option>
            </select>
          </div>
          <div class="col-sm-1">
            <h5>Periodo:</h5>
          </div>
          <div class="col-sm-3">
            <div class="input-group date p">
              <input id="filter-period-start" class="form-control input-sm loyalty-control" type="text" 
              readonly>
              <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
          </div>

           <div class="col-sm-2">
            <select id="filter-rute" class="form-control input-sm loyalty-control">
              <option value="0">Ruta</option>
              <?php
                if($sta=$db->prepare('SELECT report_agent_flight_origin,report_agent_flight_destiny FROM report_agent GROUP BY report_agent_flight_origin,report_agent_flight_destiny'))
                {
                  $sta->execute();
                  $sta->bind_result($origin,$destiny);
   
                  while ($sta->fetch()) 
                  {
              ?>
              <option value="<?php echo $origin.'-'.$destiny; ?>"><?php echo $origin.'-'.$destiny; ?></option>
              <?php
                  }
                  $sta->close();
                }
              ?>
            </select>
          </div>
        </div>

        <div class="row" style="padding-bottom: 10px">

          <div class="col-sm-2">
            <select id="filter-location-country" class="form-control input-sm loyalty-control">
              <option value="0">Pais</option>
              <?php
                if($sta=$db->prepare('SELECT country_id,country_name FROM  country'))
                {
                  $sta->execute();
                  $sta->bind_result($id,$name);
   
                  while ($sta->fetch()) 
                  {
              ?>
              <option value="<?php echo $id; ?>"><?php echo $name; ?></option>
              <?php
                  }
                  $sta->close();
                }
              ?>
            </select>
          </div>

          <div class="col-sm-2">
            <select id="filter-location-city" class="form-control input-sm loyalty-control">
              <option value="0">Ciudad</option>
            </select>
          </div>

          <div class="col-sm-3">
            <select id="filter-rute-type" class="form-control input-sm loyalty-control">
              <option value="0">Tipo de Ruta</option>
              <option value="1">Tactica</option>
              <option value="2">Basica</option>
            </select>
          </div>

          <div class="col-sm-2">
            <select id="filter-q" class="form-control input-sm loyalty-control">
              <option value="0">Q</option>
              <option value="Q1">Q1 (ENE, FEB, MAR)</option>
              <option value="Q2">Q2 (ABR, MAY, JUN)</option>
              <option value="Q3">Q3 (JUL, AGO, SEP)</option>
              <option value="Q4">Q4 (OCT, NOV, DIC)</option>
            </select>
          </div>

        </div>
      </div>
      <div class="text-right col-sm-1">
        <div class="row">
          <a id="filter-loyalty" href="javascript:void(0)"  class="btn btn-primary btn-sm">Filtrar</a>
        </div>
        <div class="row">
          <a id="clean-filter" href="javascript:void(0)" style="margin-top: 12px" class="btn btn-primary btn-sm">Reset</a>
        </div>
      </div>
      <div class="text-right col-sm-1">
        <div class="row">
          <a href="inc/excelSummary.php" style="margin-top: 22px; margin-left: 40px;" class="btn btn-primary btn-sm">Descargar Excel</a>
        </div>
      </div>
      
      <div class="row">
        <table id="loyalty-table" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Vecindario</th>
              <th>Total Agentes</th>
              <th>Total Ingresos Copa</th>
              <th>Cantidad de Tickets</th>
            </tr>
          </thead>
          <tbody>
          <?php
            ini_set('memory_limit', '-1');
            if(!isset($_SESSION['query']))
            {
              $sql="
                  SELECT
                    dn_month,dn_year
                  FROM
                    dashboard_neighborhood_agent
                  ORDER BY
                    dn_year DESC,
                    dn_month DESC
                  LIMIT 0,1
              ";
              if($sta=$db->prepare($sql))
              {
                $sta->execute();
                $sta->bind_result($m,$y);
                $sta->fetch();
                $sta->close();
              }

              $query="
                  SELECT
                    (SELECT city_name FROM city INNER JOIN agent ON city_id = agent_city_agency WHERE agent_id=dn_agent) as dn_city,
                     dn_neighborhood,
                     dn_date_record,
                     dn_revenue,
                     dn_routes_sold,
                     dn_month,
                     dn_year,
                     dn_agent
                  FROM
                    dashboard_neighborhood_agent
                  INNER JOIN
                      (
                        SELECT 
                          agent_id, agent_city_agency
                        FROM 
                                    agent
                              INNER JOIN 
                                    report_agent 
                              ON 
                                    agent_id=report_agent_agent
                        ) as sb1 
                  ON 
                        dn_agent=sb1.agent_id
                  WHERE
                    (dn_month = '".$m."') AND (dn_year = '".$y."')
                  GROUP BY
                    dn_agent
                  ORDER BY
                    dn_year DESC,
                    dn_month DESC
              ";

              $_SESSION['query']=$query;
              //echo $query;
            }
            else
            {
              $query=$_SESSION['query'];
            }

            $nt= array();

            if($sta=$db->prepare($query))
            {
              $sta->execute();

              $sta->bind_result($city,$neighborhood,$dateRecord,$revenue,$routes,$month,$year,$code);

              while ($sta->fetch()) 
              {
                $nt[$neighborhood]['title']=$neighborhood;
                $nt[$neighborhood]['code'][$code]=$code;
                $nt[$neighborhood]['revenue']+=$revenue;
                $nt[$neighborhood]['routes']+=$routes;
              } 
              $sta->close();
            }
            //echo $_SESSION['query'];
          ?>
            <tr>
              <td><?php if(empty($nt['V8']['title'])){echo neighborhoodsName('V8'); }else{ echo neighborhoodsName($nt['V8']['title']); } ?></td>
              <td class="aright"><?php echo number_format((float)round(sizeof($nt['V8']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
              <td class="aright"><?php echo "$ ".number_format((float)round($nt['V8']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
              <td class="aright"><?php echo number_format((float)round($nt['V8']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
            </tr>
            <tr>
              <td><?php if(empty($nt['V7']['title'])){echo neighborhoodsName('V7'); }else{ echo neighborhoodsName($nt['V7']['title']); } ?></td>
              <td class="aright"><?php echo number_format((float)round(sizeof($nt['V7']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
              <td class="aright"><?php echo "$ ".number_format((float)round($nt['V7']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
              <td class="aright"><?php echo number_format((float)round($nt['V7']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
            </tr>
            <tr>
              <td><?php if(empty($nt['V6']['title'])){echo neighborhoodsName('V6'); }else{ echo neighborhoodsName($nt['V6']['title']); } ?></td>
              <td class="aright"><?php echo number_format((float)round(sizeof($nt['V6']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
              <td class="aright"><?php echo "$ ".number_format((float)round($nt['V6']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
              <td class="aright"><?php echo number_format((float)round($nt['V6']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
            </tr>
            <tr>
              <td><?php if(empty($nt['V5']['title'])){echo neighborhoodsName('V5'); }else{ echo neighborhoodsName($nt['V5']['title']); } ?></td>
              <td class="aright"><?php echo number_format((float)round(sizeof($nt['V5']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
              <td class="aright"><?php echo "$ ".number_format((float)round($nt['V5']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
              <td class="aright"><?php echo number_format((float)round($nt['V5']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
            </tr>
            <tr>
              <td><?php if(empty($nt['V4']['title'])){echo neighborhoodsName('V4'); }else{ echo neighborhoodsName($nt['V4']['title']); } ?></td>
              <td class="aright"><?php echo number_format((float)round(sizeof($nt['V4']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
              <td class="aright"><?php echo "$ ".number_format((float)round($nt['V4']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
              <td class="aright"><?php echo number_format((float)round($nt['V4']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
            </tr>
            <tr>
              <td><?php if(empty($nt['V3']['title'])){echo neighborhoodsName('V3'); }else{ echo neighborhoodsName($nt['V3']['title']); } ?></td>
              <td class="aright"><?php echo number_format((float)round(sizeof($nt['V3']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
              <td class="aright"><?php echo "$ ".number_format((float)round($nt['V3']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
              <td class="aright"><?php echo number_format((float)round($nt['V3']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
            </tr>
            <tr>
              <td><?php if(empty($nt['V2']['title'])){echo neighborhoodsName('V2'); }else{ echo neighborhoodsName($nt['V2']['title']); } ?></td>
              <td class="aright"><?php echo number_format((float)round(sizeof($nt['V2']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
              <td class="aright"><?php echo "$ ".number_format((float)round($nt['V2']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
              <td class="aright"><?php echo number_format((float)round($nt['V2']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
            </tr>
            <tr>
              <td><?php if(empty($nt['V1']['title'])){echo neighborhoodsName('V1'); }else{ echo neighborhoodsName($nt['V1']['title']); } ?></td>
              <td class="aright"><?php echo number_format((float)round(sizeof($nt['V1']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
              <td class="aright"><?php echo "$ ".number_format((float)round($nt['V1']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
              <td class="aright"><?php echo number_format((float)round($nt['V1']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
            </tr>
            <!-- <tr><td colspan="4"><?php echo $_SESSION['query']; ?></td></tr> -->
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $(document).on('change','#filter-q', function(){
    if($(this).val()!=0)
    {
      $('#filter-period-start').val("");
    }
  });
  $(document).on('change','#filter-period-start', function(){
    $("#filter-q").val(0).change();
  });
});
</script>