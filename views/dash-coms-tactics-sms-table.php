                <thead>
                  <tr>
                    <th>----</th>
                    <th>Recibidos</th>
                    <th>% CLICS</th>
                    <th>Tickets</th>
                    <th>Ingreso</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  ini_set('memory_limit', '-1');
                  if(!isset($_SESSION['query_coms_tacs_0']))
                  {
                   
                    $query="
                      (
                         SELECT
                            tactic as type,
                            SUM(recieved) as total_recieved,
                            ((SUM(opened)*100)/SUM(recieved)) as percent_opened,
                            SUM(tickets) as total_tickets,
                            SUM(revenue) as total_revenue

                          FROM
                          (
                            (SELECT
                                tactic_sms_agent_tactic as tactic,
                                tactic_sms_agent_basic as basic,
                                tactic_sms_agent_recived as recieved, 
                                tactic_sms_agent_clic as opened, 
                                (
                                  SELECT COUNT( DISTINCT report_agent_flight_origin, report_agent_flight_destiny ) AS cont
                                  FROM report_agent
                                  WHERE report_agent_agent = tactic_sms_agent_agent
                                ) as tickets,
                                (
                                  SELECT SUM(report_agent_copa_revenue) AS sm
                                  FROM report_agent
                                  WHERE report_agent_agent =  tactic_sms_agent_agent
                                ) as revenue
                              FROM
                                tactic_communications_sms_agent
                              )UNION(
                              SELECT
                            tactic_sms_agency_tactic as tactic,
                            tactic_sms_agency_basic as basic,
                            tactic_sms_agency_recived as recieved, 
                            tactic_sms_agency_clic as opened, 
                            (
                              SELECT COUNT( DISTINCT report_agent_flight_origin, report_agent_flight_destiny ) AS cont
                              FROM report_agent
                              INNER JOIN agent
                              ON report_agent_agent=agent_id
                              WHERE agent_agency = tactic_sms_agency_agency
                            ) as tickets,
                            (
                              SELECT SUM(report_agent_copa_revenue) AS sm
                              FROM report_agent
                              INNER JOIN agent
                              ON report_agent_agent=agent_id
                              WHERE agent_agency = tactic_sms_agency_agency
                            ) as revenue
                          FROM
                            tactic_communications_sms_agency
                              )
                          ) drv
                          WHERE
                            tactic='1'
                        )UNION(
                         SELECT
                            tactic as type,
                            SUM(recieved) as total_recieved,
                            ((SUM(opened)*100)/SUM(recieved)) as percent_opened,
                            SUM(tickets) as total_tickets,
                            SUM(revenue) as total_revenue

                          FROM
                          (
                            (SELECT
                                tactic_sms_agent_tactic as tactic,
                                tactic_sms_agent_basic as basic,
                                tactic_sms_agent_recived as recieved, 
                                tactic_sms_agent_clic as opened, 
                                (
                                  SELECT COUNT( DISTINCT report_agent_flight_origin, report_agent_flight_destiny ) AS cont
                                  FROM report_agent
                                  WHERE report_agent_agent = tactic_sms_agent_agent
                                ) as tickets,
                                (
                                  SELECT SUM(report_agent_copa_revenue) AS sm
                                  FROM report_agent
                                  WHERE report_agent_agent =  tactic_sms_agent_agent
                                ) as revenue
                              FROM
                                tactic_communications_sms_agent
                              )UNION(
                              SELECT
                            tactic_sms_agency_tactic as tactic,
                            tactic_sms_agency_basic as basic,
                            tactic_sms_agency_recived as recieved, 
                            tactic_sms_agency_clic as opened, 
                            (
                              SELECT COUNT( DISTINCT report_agent_flight_origin, report_agent_flight_destiny ) AS cont
                              FROM report_agent
                              INNER JOIN agent
                              ON report_agent_agent=agent_id
                              WHERE agent_agency = tactic_sms_agency_agency
                            ) as tickets,
                            (
                              SELECT SUM(report_agent_copa_revenue) AS sm
                              FROM report_agent
                              INNER JOIN agent
                              ON report_agent_agent=agent_id
                              WHERE agent_agency = tactic_sms_agency_agency
                            ) as revenue
                          FROM
                            tactic_communications_sms_agency
                              )
                          ) drv

                          WHERE
                           tactic='0'
                        )

                    ";

                    $_SESSION['query_coms_tacs_0']=$query;
                    //echo $query;
                  }
                  else
                  {
                    $query=$_SESSION['query_coms_tacs_0'];
                  }

                  if($sta=$db->prepare($query))
                  {
                    $sta->execute();

                    $sta->bind_result($type,$recieved,$opened,$tickets,$revenue);

                    while ($sta->fetch()) 
                    {
                ?>
                  <tr>
                    <td><?php if($type==1){echo "Tactica"; }else{ echo "Basica"; } ?></td> 
                    <td class="aright"><?php if($recieved==NULL){$recieved='0'; echo $recieved; }else{ echo number_format((float)round($recieved, 2, PHP_ROUND_HALF_ODD), 0, '.', ','); } ?></td> 
                    <td class="aright"><?php if($opened==NULL){$opened='0'; echo $opened; }else{ echo number_format((float)round($opened, 2, PHP_ROUND_HALF_ODD), 0, '.', ','); } ?></td> 
                    <td class="aright"><?php if($tickets==NULL){$tickets='0'; echo $tickets; }else{ echo number_format((float)round($tickets, 2, PHP_ROUND_HALF_ODD), 0, '.', ','); } ?></td> 
                    <td class="aright"><?php if($revenue==NULL){$revenue='0.00'; echo "$ ".$revenue; }else{ echo "$ ".number_format((float)round($revenue, 2, PHP_ROUND_HALF_ODD), 2, '.', ','); } ?></td> 
                  </tr>
                <?php
                    }
                    $sta->close();
                  }
                  
                ?>
                <!-- <tr><td colspan="3"><?php //echo $_SESSION['query_coms_tacs_0']; ?></td></tr> -->
                </tbody>