 <?php
 include('vfunctions.php');
 ?>
 <div class="row">
        
        <?php
          include("sidebar.php");
        ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          
          <h1 class="page-header">Top Agentes</h1>
          <h4>Filtros:</h4>
            <div class="text-left col-md-12">
                <div class="col-sm-2">
                  <select id="filter-neighborhood" class="form-control input-sm loyalty-control">
                    <option value="0">Vecindarios</option>
                    <?php
                      if($sta=$db->prepare('SELECT neighborhood_id,neighborhood_name FROM  neighborhood'))
                      {
                        $sta->execute();
                        $sta->bind_result($id,$name);
         
                        while ($sta->fetch()) 
                        {
                    ?>
                    <option value="<?php echo $id; ?>"><?php echo neighborhoodsName($id); ?></option>
                    <?php
                        }
                        $sta->close();
                      }
                    ?>
                  </select>
                </div>

                <div class="col-sm-2">
                  <select id="filter-treatment" class="form-control input-sm loyalty-control">
                    <option value="0">Ambos Grupos</option>
                    <option value="1">Grupo RM</option>
                    <option value="2">Grupo Ctrl</option>
                  </select>
                </div>
                <div class="col-sm-1">
                  <h5>Período:</h5>
                </div>
                <div class="col-sm-2">
                  <div class="input-group date p">
                    <input id="filter-period-start" class="form-control input-sm loyalty-control" type="text" 
                    readonly>
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                  </div>
                </div>
                <!--
                <div class="col-sm-2">
                  <div id="dp2" class="input-group date" data-date="01-2014" data-date-format="mm-yyyy" data-date-viewmode="years" data-date-minviewmode="months">
                    <input id="filter-period-finish" class="form-control input-sm loyalty-control" type="text" readonly>
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                  </div>
                </div>
                -->
                <div class="col-sm-2">
                  <select id="filter-q" class="form-control input-sm loyalty-control">
                    <option value="0">Q</option>
                    <option value="Q1">Q1 (ENE, FEB, MAR)</option>
                    <option value="Q2">Q2 (ABR, MAY, JUN)</option>
                    <option value="Q3">Q3 (JUL, AGO, SEP)</option>
                    <option value="Q4">Q4 (OCT, NOV, DIC)</option>
                  </select>
                </div>

                <div class="col-sm-1">
                  <a id="filter-top-agents" href="javascript:void(0)"  class="btn btn-primary btn-sm">Filtrar</a>
                </div>
                <div class="col-sm-1">
                      <a id="clean-top-agents" href="javascript:void(0)" class="btn btn-primary btn-sm">Reset</a>
                </div>
                <div class="col-sm-1">
                      <a href="inc/excelTopAgents.php" class="btn btn-primary btn-sm">Descargar Excel</a>
                </div>
            </div>
            <div class="row">
              <table id="top-table" class="table table-bordered table-hover tagents">
                <thead>
                  <tr>
                    <th>ID Agente</th>
                    <th>ID Agencia</th>
                    <th>Vecindario</th>
                    <th>Básica ($)</th>
                    <th>Básica (Ticket)</th>
                    <th>Táctica ($)</th>
                    <th>Táctica (Ticket)</th>
                    <th>Ingreso Total ($)</th>
                    <th>Alas Básicas</th>
                    <th>Alas Tácticas</th>
                    <!-- <th>Azules</th> -->
                    <th>Alas Liquidadas</th>
                    <th>Alas Redimidas</th>
                    <th>Alas Disponibles</th>
                    <th>Alas Liquidadas ($)</th>
                    <th>Alas Redimidas ($)</th>
                    <th>Alas Disponibles ($)</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  ini_set('memory_limit', '-1');
                  if(!isset($_SESSION['query_top_agents']))
                  {
                    $sql="
                        SELECT
                          dn_month,dn_year
                        FROM
                          dashboard_neighborhood_agent
                        ORDER BY
                          dn_year DESC,
                          dn_month DESC
                        LIMIT 0,1
                    ";
                    if($sta=$db->prepare($sql))
                    {
                      $sta->execute();
                      $sta->bind_result($m,$y);
                      $sta->fetch();
                      $sta->close();
                    }

                    $query="
                        SELECT
                          dn_agent,
                          dn_agency,
                          dn_neighborhood,
                          dn_month,
                          dn_year,
                          basic_routes_revenue,
                          basic_routes_tickets,
                          tactic_routes_revenue,
                          tactic_routes_tickets,
                          copa_revenue,
                          white_wings,
                          white_plus_wings,
                          blue_wings,
                          redeem_wings,
                          available_wings,
                          white_wings_total,
                          cost_wings,
                          cost_redeem_wings,
                          cost_available_wings
                        FROM
                          (
                            SELECT
                              *,
                              (SELECT SUM(rpa0.report_agent_copa_revenue) FROM report_agent as rpa0 WHERE (rpa0.report_agent_agent=dnat.dn_agent) AND (rpa0.report_agent_basic_route='1')) as basic_routes_revenue,
                              (SELECT COUNT(*) FROM report_agent as rpa0 WHERE (rpa0.report_agent_agent=dnat.dn_agent) AND (rpa0.report_agent_basic_route='1')) as basic_routes_tickets,
                              (SELECT SUM(rpa0.report_agent_copa_revenue) FROM report_agent as rpa0 WHERE (rpa0.report_agent_agent=dnat.dn_agent) AND (rpa0.report_agent_tatic_route='1')) as tactic_routes_revenue,
                              (SELECT COUNT(*) FROM report_agent as rpa0 WHERE (rpa0.report_agent_agent=dnat.dn_agent) AND (rpa0.report_agent_tatic_route='1')) as tactic_routes_tickets,
                              (SELECT SUM(rpa0.report_agent_copa_revenue) FROM report_agent as rpa0 WHERE (rpa0.report_agent_agent=dnat.dn_agent) ) as copa_revenue,
                              (SELECT SUM(rpa0.report_agent_white_wings) FROM report_agent as rpa0 WHERE (rpa0.report_agent_agent=dnat.dn_agent) ) as white_wings,
                              (SELECT SUM(rpa0.report_agent_white_plus) FROM report_agent as rpa0 WHERE (rpa0.report_agent_agent=dnat.dn_agent) ) as white_plus_wings,
                              (SELECT ag0.agent_blue_wings_total FROM agent as ag0 WHERE (ag0.agent_id=dnat.dn_agent) ) as blue_wings,
                              (SELECT SUM(redeem_product_white_wings) FROM redeem_agent INNER JOIN redeem_products ON redeem_agent_product=redeem_product_id WHERE (redeem_agent_agent=dnat.dn_agent) ) as redeem_wings,
                              (SELECT SUM(rpa0.report_agent_white_wings+rpa0.report_agent_white_plus) FROM report_agent as rpa0 WHERE (rpa0.report_agent_agent=dnat.dn_agent) ) as available_wings,
                              (SELECT SUM(rpa0.report_agent_white_wings) FROM report_agent as rpa0 WHERE (rpa0.report_agent_agent=dnat.dn_agent)) as white_wings_total,
                              (SELECT (SUM(ag0.agent_blue_wings_total + ag0.agent_white_wings_total)*4000) FROM agent as ag0 WHERE (ag0.agent_id=dnat.dn_agent) ) as cost_wings,
                              (SELECT (SUM(ag0.agent_white_wings_redeemed + ag0.agent_blue_wings_redeemed)*4000) FROM agent as ag0 WHERE (ag0.agent_id=dnat.dn_agent) ) as cost_redeem_wings,
                              (SELECT (SUM(ag0.agent_white_wings_available + ag0.agent_blue_wings_available)*4000) FROM agent as ag0 WHERE (ag0.agent_id=dnat.dn_agent) ) as cost_available_wings
                            FROM
                              dashboard_neighborhood_agent as dnat
                          ) as main
                          WHERE
                            (dn_month>=".$m." AND dn_year>=".$y.") AND (dn_month<=".$m." AND dn_year<=".$y.")
                          ORDER BY
                            dn_month DESC, dn_year DESC, dn_neighborhood DESC
                    ";

                    $_SESSION['query_top_agents']=$query;
                    //echo $query;
                  }
                  else
                  {
                    $query=$_SESSION['query_top_agents'];
                  }

                  $trows=$db->query($query)->num_rows;
                  $rows=100;

                  $pag = (isset($_POST["pag"]) && ! empty($_POST["pag"]) && is_numeric($_POST["pag"])) ? strip_tags(trim($_POST["pag"])) : false;

                  if(!$pag)
                  {
                    $init=0;
                    $pag=1;
                  }
                  else
                  {
                    $init=($pag-1)*$rows;
                  }

                  $tpag=ceil($trows/$rows);

                  $lmt=" LIMIT ".$init.",".$rows;

                  $query.=$lmt;

                  if($sta=$db->prepare($query))
                  {
                    $sta->execute();

                    $sta->bind_result($code,$codea,$neighborhood,$month,$year,$basicRoutesRevenue,$basicRoutesTickets,$tacticRoutesRevenue,$tacticRoutesTickets,$copaRevenue,$whiteWings,$whitePlusWings,$blueWings,$redeemWings,$availableWings,$totalWings,$costWings,$costRedeemWings,$costAvailableWings);

                    while ($sta->fetch()) 
                    {
                ?>
                  <tr>
                    <td class="aleft"><?php echo $code; ?></td>
                    <td class="aleft"><?php echo $codea; ?></td>
                    <td class="aleft"><?php echo neighborhoodsName($neighborhood); ?></td>
                    <td class="aright"><?php echo number_format((float)round($basicRoutesRevenue, 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
                    <td class="aright"><?php echo number_format((float)round($basicRoutesTickets, 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
                    <td class="aright"><?php if($tacticRoutesRevenue==NULL){$tacticRoutesRevenue='0.00'; echo "$ ".$tacticRoutesRevenue; }else{ echo "$ ".number_format((float)round($tacticRoutesRevenue, 2, PHP_ROUND_HALF_ODD), 2, '.', ','); } ?></td> 
                    <td class="aright"><?php echo number_format((float)round($tacticRoutesTickets, 2, PHP_ROUND_HALF_ODD), 0, '.', ','); ?></td>
                    <td class="aright"><?php echo "$ ".number_format((float)round($copaRevenue, 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
                    <td class="aright"><?php echo number_format((float)round($whiteWings, 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
                    <td class="aright"><?php echo number_format((float)round($whitePlusWings, 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
                    <!-- <td class="aright"><?php //echo number_format((float)round($blueWings, 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>  -->
                    <td class="aright"><?php echo number_format((float)round($availableWings, 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
                    <td class="aright"><?php echo number_format((float)round($redeemWings, 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
                    <td class="aright"><?php echo number_format((float)round(($availableWings-$redeemWings), 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
                    <td class="aright"><?php echo "$ ".number_format((float)round(($availableWings*4000), 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
                    <td class="aright"><?php echo "$ ".number_format((float)round(($redeemWings*4000), 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
                    <td class="aright"><?php echo "$ ".number_format((float)round((($availableWings-$redeemWings)*4000), 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td> 
                  </tr>
                <?php
                    }
                ?>
                <tr>
                  <td colspan="16">
                    <?php if( $tpag > 1 ){ ?>
                    <ul class="pagination">
                      <?php if ($pag == 1 || empty($pag)){ ?>
                        <li class="disabled"><a href="#">&laquo;</a></li>
                      <?php }else{ ?>
                        <li ><a id="top-table_<?php echo ($pag-1);?>" href="#top-agent_<?php echo ($pag-1);?>" class="dash-pag">&laquo;</a></li>
                      <?php } ?>

                      <?php for ($i=1; $i <= $tpag; $i++){ ?>
                        <?php if($i == $pag){ ?>
                          <li class="active"><a id="top-agent-table_<?php echo $i;?>" href="#top-agent_<?php echo $i; ?>" class="dash-pag"><?php echo $i; ?></a></li>
                        <?php }else{ ?>
                          <li><a id="top-table_<?php echo $i;?>" href="#top-agent_<?php echo $i; ?>" class="dash-pag"><?php echo $i; ?></a></li>
                        <?php } ?>
                        
                        <?php } ?>
                      
                      <?php if ($pag != $tpag){ ?>
                        <li><a id="top-table_<?php echo ($pag+1);?>" href="#top-agent_<?php echo ($pag+1); ?>" class="dash-pag">&raquo;</a></li>
                      <?php }else{ ?>
                        <li class="disabled"><a href="#">&raquo;</a></li>
                      <?php } ?>
                    </ul>     
                    <?php } ?>
                  </td>
                </tr>
                <?php
                    $sta->close();
                  }
                  
                ?>
                <!-- <tr><td colspan="15"><?php //echo $_SESSION['query_top_agents']; ?></td></tr> -->
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <script type="text/javascript">
        $(document).ready(function(){
          $(document).on('change','#filter-q', function(){
            if($(this).val()!=0)
            {
              $('#filter-period-start').val("");
            }
          });
          $(document).on('change','#filter-period-start', function(){
            $("#filter-q").val(0).change();
          });
        });
        </script>