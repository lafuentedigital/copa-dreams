        <?php
            $active = array(
              'dashboard' => 'n-active', 
              'agent-load' => 'n-active', 
              'agency-load' => 'n-active', 
              'top-agents' => 'n-active',
              'top-agencies' => 'n-active',
              'usuarios' => 'n-active', 
              'kpi' => 'n-active', 
              'kpi-load' => 'n-active', 
              'top-angecies' => 'n-active',
              'top-agent' => 'n-active',
              'coms' => 'n-active',
              'coms-load' => 'n-active',
              'coms-tmail' => 'n-active',
              'coms-tsms' => 'n-active',
              'routes-growth' => 'n-active',
              'define-neighborhood' => 'n-active',
              'program' => 'n-active',
              'loyalty' => 'n-active',
              'participant' => 'n-active',
              'behavior' => 'n-active',
              'neighborhood' => 'n-active',
              'group-ctrl' => 'n-active',
              'communications' => 'n-active'
            );

            if(isset($_SESSION['active']))
            {
                $kActive = (string)$_SESSION['active'];

                foreach ($active as $key => $value) {
                    if($key==$kActive)
                    {
                      $active[$key]='active';
                    }
                    else
                    {
                      $active[$key]='n-active';
                    }
                }

                foreach ($active as $key => $value)
                {
                  if(in_array($kActive,array('top-agent','top-agencies','loyalty')))
                  {
                    $m1='display:block';
                  }
                  else
                  {
                    $m1='display:none';
                  }
                }

                foreach ($active as $key => $value)
                {
                  if(in_array($kActive,array('kpi')))
                  {
                    $m5='display:block';
                  }
                  else
                  {
                    $m5='display:none';
                  }
                }

                foreach ($active as $key => $value)
                {
                  if(in_array($kActive,array('coms')))
                  {
                    $m2='display:block';
                  }
                  else
                  {
                    $m2='display:none';
                  }
                }

                foreach ($active as $key => $value)
                {
                  if(in_array($kActive,array('coms-tmail','coms-tsms')))
                  {
                    $m3='display:block';
                  }
                  else
                  {
                    $m3='display:none';
                  }
                }

                foreach ($active as $key => $value)
                {
                  if(in_array($kActive,array('kpi-load','usuarios','agent-load','agency-load','coms-load','define-neighborhood')))
                  {
                    $m4='display:block';
                  }
                  else
                  {
                    $m4='display:none';
                  }
                }

                foreach ($active as $key => $value)
                {
                  if(in_array($kActive,array('participant','behavior','neighborhood','group-ctrl','communications')))
                  {
                    $m6='display:block';
                  }
                  else
                  {
                    $m6='display:none';
                  }
                }
            }
            else
            {
                foreach ($active as $key  => $value) {
                    if($key=='loyalty')
                    {
                      $active[$key]='active';
                    }
                    else
                    {
                      $active[$key]='n-active';
                    }
                }

                $m1='display:block';
                $m2='display:none';
                $m3='display:none';
                $m4='display:none';
                $m5='display:none';
                $m6='display:none';
            }
        ?>

        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li id="menu-1" class="menu-title">Loyalty</li>
            <div id="menu-1-list" style="<?php echo $m1; ?>">
              <li class="<?php echo $active['top-agent']; ?>"><a href="#top-agent" class="dash-link">Top Agentes</a></li>
              <li class="<?php echo $active['top-agencies']; ?>"><a href="#top-agencies" class="dash-link">Top Agencias</a></li>
              <li class="<?php echo $active['loyalty']; ?>"><a href="#loyalty" class="dash-link">Resumen</a></li>
            </div>
          </ul>
          <ul class="nav nav-sidebar">
            <li id="menu-5" class="menu-title">KPI</li>
            <div id="menu-5-list" style="<?php echo $m5; ?>">
              <li class="<?php echo $active['kpi']; ?>"><a href="#kpi" class="dash-link">Consultar</a></li>
            </div>
          </ul>
          <ul class="nav nav-sidebar">
            <li id="menu-2" class="menu-title">Comunicaciones Generales</li>
            <div id="menu-2-list" style="<?php echo $m2; ?>">
              <li class="<?php echo $active['coms']; ?>"><a href="#coms" class="dash-link">Email</a></li>
            </div>
          </ul>
          <ul class="nav nav-sidebar">
            <li id="menu-3" class="menu-title">Comunicaciones Tácticas</li>
            <div id="menu-3-list" style="<?php echo $m3; ?>">
              <li class="<?php echo $active['coms-tmail']; ?>"><a href="#coms-tmail" class="dash-link">Email</a></li>
              <li class="<?php echo $active['coms-tsms']; ?>"><a href="#coms-tsms" class="dash-link">SMS</a></li>
            </div>
          </ul>
          <ul class="nav nav-sidebar">
            <li id="menu-6" class="menu-title">Graficas</li>
            <div id="menu-6-list" style="<?php echo $m6; ?>">
              <li class="<?php echo $active['participant']; ?>"><a href="#participant" class="dash-link">Participantes</a></li>
              <li class="<?php echo $active['behavior']; ?>"><a href="#behavior" class="dash-link">Comportamiento</a></li>
              <li class="<?php echo $active['neighborhood']; ?>"><a href="#neighborhood" class="dash-link">Vecindarios Lealtad</a></li>
              <li class="<?php echo $active['group-ctrl']; ?>"><a href="#group-ctrl" class="dash-link">Grupo Control</a></li>
              <li class="<?php echo $active['communications']; ?>"><a href="#communications" class="dash-link">Comunicaciones</a></li>
            </div>
          </ul>
        <?php

          if(in_array('write', $_SESSION['capabilities']))
          {
            
        ?>
          <ul class="nav nav-sidebar">
            <li id="menu-4" class="menu-title">Administración</li>
            <div id="menu-4-list" style="<?php echo $m4; ?>">
              <li class="<?php echo $active['usuarios']; ?>"><a href="#usuarios" class="dash-link">Usuarios</a></li>
              <!-- <li class="<?php echo $active['kpi-load']; ?>"><a href="#kpi-load" class="dash-link">Cargar KPI</a></li> -->
              <li class="<?php echo $active['agent-load']; ?>"><a href="#agent-load" class="dash-link">Cargar Reportes (Agentes)</a></li>
              <li class="<?php echo $active['agency-load']; ?>"><a href="#agency-load" class="dash-link">Cargar Reportes (Agencias)</a></li>
              <li class="<?php echo $active['coms-load']; ?>"><a href="#coms-load" class="dash-link">Cargar Comunicaciones</a></li>
              <li class="<?php echo $active['define-neighborhood']; ?>"><a href="#define-neighborhood" class="dash-link">Definir Vecindarios</a></li>
            </div>
          </ul>
        <?php

          }

        ?>
        </div>