<?php

	include("inc/db.php");

	require('inc/classes/session.class.php');

	$session = new session();
	$session->start_session('_s', false, $db);

	//$session->destroy();

	session_destroy();

	header("Location: /");

?>