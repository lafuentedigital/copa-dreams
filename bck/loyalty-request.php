<?php
	include("db.php");

	require('classes/session.class.php');
	$session = new session();
	$session->start_session('_s', false, $db);

	if(isset($_POST['filters']) && $_POST['filters'])
	{
		$conditional="";

        if(isset($_POST['byQ']))
		{
			$currenYear=date('Y');
			if(!$per)
			{
				switch ($_POST['byQ']) {
					
					case 'Q1':{
						$conditional.="WHERE (dn_month>='1' AND dn_year>=".$currenYear.") AND (dn_month<='3' AND dn_year<=".$currenYear.")";
						break;
					}
					
					case 'Q2':{
						$conditional.="WHERE (dn_month>='4' AND dn_year>=".$currenYear.") AND (dn_month<='6' AND dn_year<=".$currenYear.")";
						break;
					}

					case 'Q3':{
						$conditional.="WHERE (dn_month>='7' AND dn_year>=".$currenYear.") AND (dn_month<='9' AND dn_year<=".$currenYear.")";
						break;
					}

					case 'Q4':{
						$conditional.="WHERE (dn_month>='10' AND dn_year>=".$currenYear.") AND (dn_month<='12' AND dn_year<=".$currenYear.")";
						break;
					}
				}
			}
		}

		$select="
		  SELECT 
		    dn_neighborhood as neighborhood,
		    COUNT(dn_agent) as total_agent, 
		    SUM(dn_revenue) as total_revenue, 
		    SUM(dn_routes_sold) as total_routes_sold 
		  FROM 
		    dashboard_neighborhood_agent
		  ".$conditional." 
		  GROUP BY 
		    dn_neighborhood 
		  ORDER BY 
		    dn_neighborhood DESC
		";

		$query=$select;
		$_SESSION['query']=$query;
		//echo $query;
		echo "1";
	}

	if (isset($_POST['countrys'])) 
	{
		if($_POST['countrys']!=0)
		{
			$country=$_POST['countrys'];
			
			echo '<option value="0">Ciudad</option>';

			if($sta=$db->prepare("SELECT city_id,city_name FROM  city WHERE city_country='".$country."';"))
          	{
            	$sta->execute();
            	$sta->bind_result($id,$name);

            	

            	while ($sta->fetch()) 
            	{
            		echo '<option value="'.$id.'">'.$name.'</option>';
            	}
            	$sta->close();
            }
        }
        else
        {
        	echo '<option value="0">Selecione un Pais</option>';
        }
	}

	if(isset($_POST['clean']))
	{
		unset($_SESSION['query']);
		if(!isset($_SESSION['query']))
		{
			echo "1";
		}
	}
?>