<div class="row">
        
<?php
  include("sidebar.php");
?>

  <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    
    <h1 class="page-header">Resumen</h1>
    <h4>Filtros:</h4>
      <div class="text-left col-md-9">
          <div class="col-sm-2">
            <select id="filter-q" class="form-control input-sm loyalty-control">
              <option value="0">Q</option>
              <option value="Q1">Q1 (ENE, FEB, MAR)</option>
              <option value="Q2">Q2 (ABR, MAY, JUN)</option>
              <option value="Q3">Q3 (JUL, AGO, SEP)</option>
              <option value="Q4">Q4 (OCT, NOV, DIC)</option>
            </select>
          </div>

          <div class="col-sm-1">
            <a id="filter-loyalty" href="javascript:void(0)"  class="btn btn-primary btn-sm">Filtrar</a>
          </div>
          <div class="col-sm-1">
            <a id="clean-filter" href="javascript:void(0)" class="btn btn-primary btn-sm">Reset</a>
          </div>
      </div>

        
      <div class="row">
        <table id="loyalty-table" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Vecindario</th>
              <th>Total Agentes</th>
              <th>Total Ingresos Copa</th>
              <th>Número de Tickets</th>
            </tr>
          </thead>
          <tbody>
          <?php
            ini_set('memory_limit', '-1');
            if(!isset($_SESSION['query']))
            {
              

              $query="
                  SELECT 
                    dn_neighborhood as neighborhood,
                    COUNT(dn_agent) as total_agent, 
                    SUM(dn_revenue) as total_revenue, 
                    SUM(dn_routes_sold) as total_routes_sold 
                  FROM 
                    dashboard_neighborhood_agent 
                  GROUP BY 
                    dn_neighborhood 
                  ORDER BY 
                    dn_neighborhood DESC
              ";

              $_SESSION['query']=$query;
              //echo $query;
            }
            else
            {
              $query=$_SESSION['query'];
            }

            if($sta=$db->prepare($query))
            {
              $sta->execute();

              $sta->bind_result($neighborhood,$totalAgent,$totalRevenue,$totalRoutes);

              while ($sta->fetch()) 
              {
          ?>
            <tr>
              <td><?php echo $neighborhood; ?></td>
              <td class="aright"><?php echo number_format((float)round($totalAgent, 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
              <td class="aright"><?php echo "$ ".number_format((float)round($totalRevenue, 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
              <td class="aright"><?php echo number_format((float)round($totalRoutes, 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
            </tr>
          <?php
              }
              $sta->close();
            }
            
          ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>