                 <thead>
                  <tr>
                    <th>Vecindario</th>
                    <th><?php if(isset($_SESSION['agency']) && $_SESSION['agency']){ echo "Agencia"; }else{ echo "Agente"; } ?></th>
                    <th>Periodo</th>
                    <th>Ganancia Copa</th>
                    <th>Rutas Vendidas</th>
                    <th>Ciudad</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  ini_set('memory_limit', '-1');
                  if(!isset($_SESSION['query']))
                  {
                    $sql="
                        SELECT
                          dn_month,dn_year
                        FROM
                          dashboard_neighborhood_agent
                        ORDER BY
                          dn_year DESC,
                          dn_month DESC
                        LIMIT 0,1
                    ";
                    if($sta=$db->prepare($sql))
                    {
                      $sta->execute();
                      $sta->bind_result($m,$y);
                      $sta->fetch();
                      $sta->close();
                    }

                    $query="
                        SELECT
                          (SELECT city_name FROM city INNER JOIN agent ON city_id = agent_city_agency WHERE agent_id=dn_agent) as dn_city, dn_neighborhood,dn_date_record,dn_revenue,dn_routes_sold,dn_month,dn_year,dn_agent
                        FROM
                          dashboard_neighborhood_agent
                        INNER JOIN
                            (
                              SELECT 
                                agent_id, agent_city_agency, report_agent_flight_origin, report_agent_flight_destiny, report_agent_tatic_route, report_agent_basic_route, report_agent_agent 
                              FROM 
                                          agent
                                    INNER JOIN 
                                          report_agent 
                                    ON 
                                          agent_id=report_agent_agent
                              ) as sb1 
                        ON 
                              dn_agent=sb1.agent_id
                        WHERE
                          (dn_month = '".$m."') AND (dn_year = '".$y."')
                        GROUP BY
                          dn_agent
                        ORDER BY
                          dn_year DESC,
                          dn_month DESC
                    ";

                    $_SESSION['query']=$query;
                    //echo $query;
                  }
                  else
                  {
                    $query=$_SESSION['query'];
                  }

                  $trows=$db->query($query)->num_rows;
                  $rows=15;

                  $pag = (isset($_POST["navpag"]) && ! empty($_POST["navpag"]) && is_numeric($_POST["navpag"])) ? strip_tags(trim($_POST["navpag"])) : false;

                  if(!$pag)
                  {
                    $init=0;
                    $pag=1;
                  }
                  else
                  {
                    $init=($pag-1)*$rows;
                  }

                  $tpag=ceil($trows/$rows);

                  $lmt=" LIMIT ".$init.",".$rows;

                  $query.=$lmt;

                  if($sta=$db->prepare($query))
                  {
                    $sta->execute();

                    $sta->bind_result($city,$neighborhood,$dateRecord,$revenue,$routesSold,$month,$year,$code);

                    while ($sta->fetch()) 
                    {
                ?>
                  <tr>
                    <td><?php echo $neighborhood; ?></td>
                    <td><?php echo $code; ?></td>
                    <td><?php echo $month."-".$year; ?></td>
                    <td><?php echo $revenue; ?></td>
                    <td><?php echo $routesSold; ?></td> 
                    <td><?php echo $city; ?></td> 
                  </tr>
                 <?php
                    }
                ?>
                <tr>
                  <td colspan="6">
                    <?php if( $tpag > 1 ){ ?>
                    <ul class="pagination">
                      <?php if ($pag == 1 || empty($pag)){ ?>
                        <li class="disabled"><a href="#">&laquo;</a></li>
                      <?php }else{ ?>
                        <li ><a id="loyalty-table_<?php echo ($pag-1);?>" href="#loyalty-<?php echo ($pag-1);?>" class="dash-pag">&laquo;</a></li>
                      <?php } ?>

                      <?php for ($i=1; $i <= $tpag; $i++){ ?>
                        <?php if($i == $pag){ ?>
                          <li class="active"><a id="loyalty-table_<?php echo $i;?>" href="#loyalty-<?php echo $i; ?>" class="dash-pag"><?php echo $i; ?></a></li>
                        <?php }else{ ?>
                          <li><a id="loyalty-table_<?php echo $i;?>" href="#loyalty-<?php echo $i; ?>" class="dash-pag"><?php echo $i; ?></a></li>
                        <?php } ?>
                        
                        <?php } ?>
                      
                      <?php if ($pag != $tpag){ ?>
                        <li><a id="loyalty-table_<?php echo ($pag+1);?>" href="#loyalty-<?php echo ($pag+1); ?>" class="dash-pag">&raquo;</a></li>
                      <?php }else{ ?>
                        <li class="disabled"><a href="#">&raquo;</a></li>
                      <?php } ?>
                    </ul>     
                    <?php } ?>
                  </td>
                </tr>
                <?php    
                    $sta->close();
                  }
                  
                ?>
                <!--<tr><td colspan="6"><?php //echo $_SESSION['query']; ?></td></tr>-->
                </tbody>