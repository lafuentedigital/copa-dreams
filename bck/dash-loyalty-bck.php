<div class="row">
        
<?php
  include("sidebar.php");
?>

  <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    
    <h1 class="page-header">Loyalty Framework</h1>
    <h4>Filtros:</h4>
      <div class="text-left col-md-9">

        <div class="row" style="padding-bottom: 8px">

          <div class="col-sm-2">
            <select id="filter-condition" class="form-control input-sm loyalty-control">
              <option value="1">Agente</option>
              <option value="2">Agencia</option>
            </select>
          </div>

          <div class="col-sm-3">
            <select id="filter-treatment" class="form-control input-sm loyalty-control">
              <option value="0">Ambos</option>
              <option value="1">Con Tratamiento</option>
              <option value="2">Sin Tratamiento</option>
            </select>
          </div>
          <div class="col-sm-1">
            <h5>Periodo:</h5>
          </div>
          <div class="col-sm-3">
            <div class="input-group date p">
              <input id="filter-period-start" class="form-control input-sm loyalty-control" type="text" 
              readonly>
              <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
          </div>

           <div class="col-sm-2">
            <select id="filter-rute" class="form-control input-sm loyalty-control">
              <option value="0">Ruta</option>
              <?php
                if($sta=$db->prepare('SELECT report_agent_flight_origin,report_agent_flight_destiny FROM report_agent GROUP BY report_agent_flight_origin,report_agent_flight_destiny'))
                {
                  $sta->execute();
                  $sta->bind_result($origin,$destiny);
   
                  while ($sta->fetch()) 
                  {
              ?>
              <option value="<?php echo $origin.'-'.$destiny; ?>"><?php echo $origin.'-'.$destiny; ?></option>
              <?php
                  }
                  $sta->close();
                }
              ?>
            </select>
          </div>
        </div>

        <div class="row" style="padding-bottom: 10px">

          <div class="col-sm-2">
            <select id="filter-location-country" class="form-control input-sm loyalty-control">
              <option value="0">Pais</option>
              <?php
                if($sta=$db->prepare('SELECT country_id,country_name FROM  country'))
                {
                  $sta->execute();
                  $sta->bind_result($id,$name);
   
                  while ($sta->fetch()) 
                  {
              ?>
              <option value="<?php echo $id; ?>"><?php echo $name; ?></option>
              <?php
                  }
                  $sta->close();
                }
              ?>
            </select>
          </div>

          <div class="col-sm-2">
            <select id="filter-location-city" class="form-control input-sm loyalty-control">
              <option value="0">Ciudad</option>
            </select>
          </div>

          <div class="col-sm-3">
            <select id="filter-rute-type" class="form-control input-sm loyalty-control">
              <option value="0">Tipo de Ruta</option>
              <option value="1">Tactica</option>
              <option value="2">Basica</option>
            </select>
          </div>

        </div>
      </div>
      <div class="text-right col-sm-1">
        <div class="row">
          <a id="filter-loyalty" href="javascript:void(0)"  class="btn btn-primary btn-sm">Filtrar</a>
        </div>
        <div class="row">
          <a id="clean-filter" href="javascript:void(0)" style="margin-top: 12px" class="btn btn-primary btn-sm">Reset</a>
        </div>
      </div>
      <?php

          if(in_array('write', $_SESSION['capabilities']))
          {

        ?>
      <div class="text-right col-sm-2">
        <div class="row">
          <a id="define-neighborhoods" href="javascript:void(0)" style="margin-top: 20px" class="btn btn-primary btn-sm">Definir Vecindarios</a>
        </div>
      </div>
      <?php

          }

        ?>
      <div class="row">
        <table id="loyalty-table" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Vecindario</th>
              <th><?php if(isset($_SESSION['agency']) && $_SESSION['agency']){ echo "Agencia"; }else{ echo "Agente"; } ?></th>
              <th>Periodo</th>
              <th>Ganancia Copa</th>
              <th>Rutas Vendidas</th>
              <th>Ciudad</th>
            </tr>
          </thead>
          <tbody>
          <?php
            ini_set('memory_limit', '-1');
            if(!isset($_SESSION['query']))
            {
              $sql="
                  SELECT
                    dn_month,dn_year
                  FROM
                    dashboard_neighborhood_agent
                  ORDER BY
                    dn_year DESC,
                    dn_month DESC
                  LIMIT 0,1
              ";
              if($sta=$db->prepare($sql))
              {
                $sta->execute();
                $sta->bind_result($m,$y);
                $sta->fetch();
                $sta->close();
              }

              $query="
                  SELECT
                    (SELECT city_name FROM city INNER JOIN agent ON city_id = agent_city_agency WHERE agent_id=dn_agent) as dn_city,
                     dn_neighborhood,
                     dn_date_record,
                     dn_revenue,
                     dn_routes_sold,
                     dn_month,
                     dn_year,
                     dn_agent
                  FROM
                    dashboard_neighborhood_agent
                  INNER JOIN
                      (
                        SELECT 
                          agent_id, agent_city_agency
                        FROM 
                                    agent
                              INNER JOIN 
                                    report_agent 
                              ON 
                                    agent_id=report_agent_agent
                        ) as sb1 
                  ON 
                        dn_agent=sb1.agent_id
                  WHERE
                    (dn_month = '".$m."') AND (dn_year = '".$y."')
                  GROUP BY
                    dn_agent
                  ORDER BY
                    dn_year DESC,
                    dn_month DESC
              ";

              $_SESSION['query']=$query;
              //echo $query;
            }
            else
            {
              $query=$_SESSION['query'];
            }

            $trows=$db->query($query)->num_rows;
            $rows=15;

            $pag = (isset($_POST["pag"]) && ! empty($_POST["pag"]) && is_numeric($_POST["pag"])) ? strip_tags(trim($_POST["pag"])) : false;

            if(!$pag)
            {
              $init=0;
              $pag=1;
            }
            else
            {
              $init=($pag-1)*$rows;
            }

            $tpag=ceil($trows/$rows);

            $lmt=" LIMIT ".$init.",".$rows;

            $query.=$lmt;

            if($sta=$db->prepare($query))
            {
              $sta->execute();

              $sta->bind_result($city,$neighborhood,$dateRecord,$revenue,$routesSold,$month,$year,$code);

              while ($sta->fetch()) 
              {
          ?>
            <tr>
              <td><?php echo $neighborhood; ?></td>
              <td><?php echo $code; ?></td>
              <td><?php echo $month."-".$year; ?></td>
              <td><?php echo $revenue; ?></td>
              <td><?php echo $routesSold; ?></td> 
              <td><?php echo $city; ?></td> 
            </tr>
          <?php
              }
          ?>
          <tr>
            <td colspan="6">
              <?php if( $tpag > 1 ){ ?>
              <ul class="pagination">
                <?php if ($pag == 1 || empty($pag)){ ?>
                  <li class="disabled"><a href="#">&laquo;</a></li>
                <?php }else{ ?>
                  <li ><a id="loyalty-table_<?php echo ($pag-1);?>" href="#loyalty-<?php echo ($pag-1);?>" class="dash-pag">&laquo;</a></li>
                <?php } ?>

                <?php for ($i=1; $i <= $tpag; $i++){ ?>
                  <?php if($i == $pag){ ?>
                    <li class="active"><a id="loyalty-table_<?php echo $i;?>" href="#loyalty-<?php echo $i; ?>" class="dash-pag"><?php echo $i; ?></a></li>
                  <?php }else{ ?>
                    <li><a id="loyalty-table_<?php echo $i;?>" href="#loyalty-<?php echo $i; ?>" class="dash-pag"><?php echo $i; ?></a></li>
                  <?php } ?>
                  
                  <?php } ?>
                
                <?php if ($pag != $tpag){ ?>
                  <li><a id="loyalty-table_<?php echo ($pag+1);?>" href="#loyalty-<?php echo ($pag+1); ?>" class="dash-pag">&raquo;</a></li>
                <?php }else{ ?>
                  <li class="disabled"><a href="#">&raquo;</a></li>
                <?php } ?>
              </ul>     
              <?php } ?>
            </td>
          </tr>
          <?php    
              $sta->close();
            }
            
          ?>
          <!--<tr><td colspan="6"><?php //echo $_SESSION['query']; ?></td></tr>-->
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>