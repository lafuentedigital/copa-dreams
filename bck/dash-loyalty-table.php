          <thead>
            <tr>
              <th>Vecindario</th>
              <th>Total Agentes</th>
              <th>Total Ingresos Copa</th>
              <th>Número de Tickets</th>
            </tr>
          </thead>
          <tbody>
          <?php
            ini_set('memory_limit', '-1');
            if(!isset($_SESSION['query']))
            {
              

              $query="
                  SELECT 
                    dn_neighborhood as neighborhood,
                    COUNT(dn_agent) as total_agent, 
                    SUM(dn_revenue) as total_revenue, 
                    SUM(dn_routes_sold) as total_routes_sold 
                  FROM 
                    dashboard_neighborhood_agent 
                  GROUP BY 
                    dn_neighborhood 
                  ORDER BY 
                    dn_neighborhood DESC
              ";

              $_SESSION['query']=$query;
              //echo $query;
            }
            else
            {
              $query=$_SESSION['query'];
            }

            if($sta=$db->prepare($query))
            {
              $sta->execute();

              $sta->bind_result($neighborhood,$totalAgent,$totalRevenue,$totalRoutes);

              while ($sta->fetch()) 
              {
          ?>
            <tr>
              <td><?php echo $neighborhood; ?></td>
              <td class="aright"><?php echo number_format((float)round($totalAgent, 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
              <td class="aright"><?php echo "$ ".number_format((float)round($totalRevenue, 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
              <td class="aright"><?php echo number_format((float)round($totalRoutes, 2, PHP_ROUND_HALF_ODD), 2, '.', ','); ?></td>
            </tr>
          <?php
              }
              $sta->close();
            }
            
          ?>
          </tbody>