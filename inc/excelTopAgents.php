<?php 
	include("db.php");
	require('classes/session.class.php');

	include('../views/vfunctions.php');
    
	$session = new session();
	$session->start_session('_s', false, $db);
	
	set_include_path(get_include_path() . PATH_SEPARATOR . 'xls/Classes/');
	include 'PHPExcel.php';
	$query = $_SESSION['query_top_agents'];


	//Se crea el objeto PHP EXCEL, que tendra todos los atributos y demas para nuestro spreadsheet
	$objPHPExcel = new PHPExcel();

	//propiedades del spreadsheet
	$objPHPExcel->getProperties()->setCreator("Copa");
	$objPHPExcel->getProperties()->setLastModifiedBy("LFD");
	$objPHPExcel->getProperties()->setTitle("Reporte");
	$objPHPExcel->getProperties()->setSubject("Reporte de Copa");
	$objPHPExcel->getProperties()->setDescription("Reporte de Copa Top Agents");

	$reportName = "REPORTE TOP AGENTES"; //titulo
	$colName = array('IDAgentes','IDAgencias','Vecindario','Básica ($)','Básica (Ticket)','Táctica ($)','Táctica (Ticket)','Ingreso Total($)',
					'Alas Básicas','Alas Tácticas','Alas Liquidadas','Alas Redimidas','Alas Disponibles','Alas Liquidadas($)',
					'Alas Redimidas($)','Alas Disponibles($)'); //titulos de la columna

	//unimos los cells
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:P1');

	//agregamos el titulo a las celdas
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $reportName); 

    
    $lista= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P');
    
    //cambiamos los tamaños a algunas celdas
    //for que plasma el titulo de las columnas y les aplica un tamaño auto 
    for($u=0;$u<sizeof($lista);$u++){
    	$objPHPExcel->getActiveSheet()->getColumnDimension($lista[$u])->setAutoSize(true);
    	$objPHPExcel->getActiveSheet()->getStyle($lista[$u])->getAlignment()
    				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($lista[$u].'2',  $colName[$u]);    	
    }	

    //var de recorrido que comienza en 3 porq desde esa fila se comenzara a 
    //imprimir la info, las dos anteriores estan ocupadas por los titulos.
    $i=3; 
	
	//con el query usado para los datos de la tabla extraemos los datos con el q crearemos el spreadsheet
	if($sta=$db->prepare($query)){

        $sta->execute();                 
        $sta->bind_result($code,$codea,$neighborhood,$month,$year,$basicRoutesRevenue,$basicRoutesTickets,
          	$tacticRoutesRevenue,$tacticRoutesTickets,$copaRevenue,$whiteWings,$whitePlusWings,$blueWings,
          	$redeemWings,$availableWings,$totalWings,$costWings,$costRedeemWings,$costAvailableWings);

        while ($sta->fetch()){

        	$neighborhood= neighborhoodsName($neighborhood);
        	$basicRoutesRevenue = number_format((float)round($basicRoutesRevenue, 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
        	$basicRoutesTickets = number_format((float)round($basicRoutesTickets, 2, PHP_ROUND_HALF_ODD), 0, '.', ',');

        	if($tacticRoutesRevenue==NULL)
        		$tacticRoutesRevenue = '0.00'; 
        	else 
        		$tacticRoutesRevenue = '$ '.number_format((float)round($tacticRoutesRevenue, 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
        	
        	$tacticRoutesTickets = number_format((float)round($tacticRoutesTickets, 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
        	$copaRevenue = '$ '.number_format((float)round($copaRevenue, 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
        	$whiteWings = number_format((float)round($whiteWings, 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
        	$whitePlusWings = number_format((float)round($whitePlusWings, 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
        	$redeemWings = number_format((float)round($redeemWings, 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
        	$availableWings = number_format((float)round($availableWings, 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
        	$totalWings = number_format((float)round($totalWings, 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
        	$costWings = '$ '.number_format((float)round($costWings, 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
        	$costRedeemWings = '$ '.number_format((float)round($costRedeemWings, 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
        	$costAvailableWings = '$ '.number_format((float)round($costAvailableWings, 2, PHP_ROUND_HALF_ODD), 2, '.', ',');

        	$objPHPExcel->setActiveSheetIndex(0)
        	->setCellValue('A'.$i, $code)
        	->setCellValue('B'.$i, $codea)
        	->setCellValue('C'.$i, $neighborhood)
        	->setCellValue('D'.$i, $basicRoutesRevenue)
        	->setCellValue('E'.$i, $basicRoutesTickets)
        	->setCellValue('F'.$i, $tacticRoutesRevenue)
        	->setCellValue('G'.$i, $tacticRoutesTickets)
        	->setCellValue('H'.$i, $copaRevenue)
        	->setCellValue('I'.$i, $whiteWings)
        	->setCellValue('J'.$i, $whitePlusWings)
        	->setCellValue('K'.$i, $redeemWings)
        	->setCellValue('L'.$i, $availableWings)
        	->setCellValue('M'.$i, $totalWings)
        	->setCellValue('N'.$i, $costWings)
        	->setCellValue('O'.$i, $costRedeemWings)
        	->setCellValue('P'.$i, $costAvailableWings);
        	$i++;
        }
        $sta->close();
    }

    //Ahora se le coloca los estilos a la tabla
    //Emepzamos con el titulo
    $esReportName = array(
	    'font' => array(
	        'name'      => 'Times New Roman',
	        'bold'      => true,
	        'italic'    => false,
	        'strike'    => false,
	        'size'      => 12,
	        'color'     => array('rgb' => 'FFFFFF')
	    ),
	    'fill' => array(
	        'type'  => PHPExcel_Style_Fill::FILL_SOLID,
	        'color' => array('rgb' => '006699')
	    ),
	    'borders' => array(
	        'allborders' => array(
	            'style' => PHPExcel_Style_Border::BORDER_NONE
	        )
	    ),
	    'alignment' => array(
	        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	        'rotation' => 0,
	        'wrap' => TRUE
	    )
	);
 	$objPHPExcel->getActiveSheet()->getStyle('A1:P1')->applyFromArray($esReportName);

    $esColName = array(
	    'font' => array(
	        'name'  => 'Times new Roman',
	        'bold'  => false,
	        'size'  => 12,
	        'color' => array(
	            'rgb' => '333333'
	        )
	    ),
	    'fill' => array(
	        'type'  => PHPExcel_Style_Fill::FILL_SOLID,
	        'color' => array('rgb' => 'cdcdcd')
	    ),
	    'borders' => array(
	        'allborders' => array(
	            'style' => PHPExcel_Style_Border::BORDER_NONE
	        )
	    ),
	    
	);
	$objPHPExcel->getActiveSheet()->getStyle('A2:P2')->applyFromArray($esColName);    

	$objPHPExcel->getActiveSheet()->setTitle('Reporte Top Agentes');	 
	$objPHPExcel->setActiveSheetIndex(0);	 

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="reporteTA.xls"');
	header('Cache-Control: max-age=0');

	require_once 'PHPExcel/IOFactory.php';
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
	$objWriter->save('php://output',__FILE__);
	exit;
	
?>