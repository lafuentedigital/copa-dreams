<?php
	include("db.php");

	require('classes/session.class.php');
	$session = new session();
	$session->start_session('_s', false, $db);

    //var_dump($_POST);

	if (isset($_POST['kpi_date']))
	{
        $allok=true;
        if (!is_numeric($_POST['basic_amount'])) 
        {
            echo "Solo se permiten numeros para los montos.";
            $allok=false;
        }
        else if (!is_numeric($_POST['basic_ticket']))
        {
            echo "Solo se permiten numeros para los tickets.";
            $allok=false;
        }
        else if (!is_numeric($_POST['tactic_amount']))
        {
            echo "Solo se permiten numeros para los montos.";
            $allok=false;
        }
        else if (!is_numeric($_POST['tactic_ticket']))
        {
            echo "Solo se permiten numeros para los tickets.";
            $allok=false;
        }
        else if (!is_numeric($_POST['copa_revenue']))
        {
            echo "Solo se permiten numeros para los montos.";
            $allok=false;
        }
        else if (!is_numeric($_POST['roi']))
        {
            echo "Solo se permiten numeros para los montos.";
            $allok=false;
        }
        else if (!is_numeric($_POST['cost_amount']))
        {
            echo "Solo se permiten numeros para los montos.";
            $allok=false;
        }

        if($allok)
        {
            $date=explode("-", $_POST['kpi_date']);

            if($sta=$db->prepare("INSERT INTO kpi (kpi_basic_amount,kpi_basic_ticket,kpi_tactic_amount,kpi_tactic_ticket,kpi_copa_amount,kpi_roi_amount,kpi_cost_amount,kpi_tactic_treatment,kpi_month,kpi_year) VALUES (?,?,?,?,?,?,?,?,?,?)"))
            {
                $sta->bind_param('dididddiii',$_POST['basic_amount'],$_POST['basic_ticket'],$_POST['tactic_amount'],$_POST['tactic_ticket'],$_POST['copa_revenue'],$_POST['roi'],$_POST['cost_amount'],$_POST['treatment'],$date[0],$date[1]);

                if(!$sta->execute())
                {
                    echo "Error al cargar en la base de datos.";
                }
                else
                {
                    echo "1";
                }
                $sta->close();
            }
        }

	}
    else
    {
        echo "Error de conexion.";
    }
?>