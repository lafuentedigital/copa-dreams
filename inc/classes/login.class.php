<?php
class login {

	private $db;

	public $logged;

	public $capabilities;

	function __construct($user,$pass,$conex)
	{
		ini_set('memory_limit', '-1');
		$this->db = $conex;
		$tmpq = $this->db->prepare("SELECT du_id,du_mail,du_pass,du_key,du_capabilities FROM dashboard_users WHERE du_user = ? LIMIT 1");

		$tmpq->bind_param('s',$user);
		$tmpq->execute();
		$tmpq->bind_result($id,$mail,$passH,$key,$capab);
		$tmpq->fetch();
		$tmpq->close();

		$chkKey=hash('sha512', $user.$pass.$mail.$id);

		$chkPass=hash('sha512', $pass);

		$this->capabilities=$capab;

		if($chkKey==$key)
		{
			if($chkPass==$passH)
			{
				$this->logged = 1;
			}
			else
			{
				$this->logged = 0;	
			}
		}
		else
		{
			$this->logged = 0;
		}

	}

	function get_capabilities()
	{
		$capab = explode(',', $this->capabilities);

		return $capab;
	}
}
?>