<?php
	function openXls($inputFileName)
	{
		ini_set('memory_limit', '-1');
		set_include_path(get_include_path() . PATH_SEPARATOR . 'xls/Classes/');
		include 'PHPExcel/IOFactory.php';
		try {
			$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
   			$cacheSettings = array( ' memoryCacheSize ' => '5120MB');
    		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
			$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		
		$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
		
		return $sheetData;
		/*
		$objWorksheet = $objPHPExcel->getActiveSheet();

	    //Get last row and column that have data
	    $lastRow = $objWorksheet->getHighestDataRow();

	    $lastCol = $objWorksheet->getHighestDataColumn();
        //Change Column letter to column number
        $lastCol = PHPExcel_Cell::columnIndexFromString($lastCol); 

	    //Get Data Array
	    $dataArray = array();
	    $lts=array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');

	    for ($currentRow = 2; $currentRow <= $lastRow; $currentRow++){
	        for ($currentCol = 0; $currentCol < $lastCol; $currentCol++){
	        	$thisCol=$lts[$currentCol];
	            $dataArray[$currentRow][$thisCol] = $objWorksheet->getCellByColumnAndRow($currentCol,$currentRow)->getValue();
	        }
	    }
		return $dataArray;
		*/
	}

	function sanitizeNull($var)
	{
		if($var==null || empty($var)){
			$var='0';
		}

		return $var;
	}

	function roi($ingresos,$costos)
	{
		if($costos!=0)
		{
			$roiv=(($ingresos-$costos)/$costos);

			return $roiv;
		}
		else
		{
			return 0;
		}
		
	}
?>