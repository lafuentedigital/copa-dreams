<?php 
	//para mayor entendimiento dirijase a la pag excelTopAgents.php 		
	include("db.php");
	require('classes/session.class.php');

	include('../views/vfunctions.php');
    
	$session = new session();
	$session->start_session('_s', false, $db);
	
	set_include_path(get_include_path() . PATH_SEPARATOR . 'xls/Classes/');
	include 'PHPExcel.php';
	$query = $_SESSION['query_top_agencies'];

	$objPHPExcel = new PHPExcel();

	$objPHPExcel->getProperties()->setCreator("Copa");
	$objPHPExcel->getProperties()->setLastModifiedBy("LFD");
	$objPHPExcel->getProperties()->setTitle("Reporte");
	$objPHPExcel->getProperties()->setSubject("Reporte de Copa");
	$objPHPExcel->getProperties()->setDescription("Reporte de Copa Top Agencias");

	$reportName = "REPORTE TOP AGENCIAS";

	$colName = array('IDAgencias','Vecindario','Básica ($)','Básica (Ticket)','Táctica ($)','Táctica (Ticket)','Ingreso Total($)',
					'Alas Básicas','Alas Tácticas','Alas Redimidas','Alas Disponibles','Alas Liquidadas($)',
					'Alas Redimidas($)','Alas Disponibles($)'); 

	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:N1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $reportName); 

	$lista= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N');

	for($u=0;$u<sizeof($lista);$u++){
    	$objPHPExcel->getActiveSheet()->getColumnDimension($lista[$u])->setAutoSize(true);
    	$objPHPExcel->getActiveSheet()->getStyle($lista[$u])->getAlignment()
    				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($lista[$u].'2',  $colName[$u]);    	
    }

    $i=3;

    if($sta=$db->prepare($query)){

        $sta->execute();                 
        $sta->bind_result($code,$neighborhood,$month,$year,$basicRoutesRevenue,$basicRoutesTickets,$tacticRoutesRevenue,
        	$tacticRoutesTickets,$copaRevenue,$whiteWings,$redeemWings,$availableWings,$costWings,$costRedeemWings,$costAvailableWings);

        while ($sta->fetch()){

        	if($basicRoutesRevenue == NULL || $basicRoutesRevenue==0){
        		$basicRoutesRevenue = '0.00'; 
        		$basicRoutesRevenue = "$ ".$basicRoutesRevenue;  
        	}else{
        		$basicRoutesRevenue = "$ ".number_format((float)round($basicRoutesRevenue, 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
        	}

        	if($basicRoutesTickets == NULL || $basicRoutesTickets==0){
        		$basicRoutesTickets = '0'; 
        	}else{ 
        		$basicRoutesTickets = number_format((float)round($basicRoutesTickets, 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
        	}

        	if($tacticRoutesRevenue == NULL || $tacticRoutesRevenue==0){
        		$tacticRoutesRevenue = '0.00'; 
        		$tacticRoutesRevenue = "$ ".$tacticRoutesRevenue; 
        	}else{ 
        		$tacticRoutesRevenue = "$ ".number_format((float)round($tacticRoutesRevenue, 2, PHP_ROUND_HALF_ODD), 2, '.', ','); 
        	}

        	if($tacticRoutesTickets == NULL || $tacticRoutesTickets==0){
        		$tacticRoutesTickets ='0'; 
        	}else{ 
        		$tacticRoutesTickets = number_format((float)round($tacticRoutesTickets, 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
			}

			$neighborhood = neighborhoodsName($neighborhood);
        	$copaRevenue = "$ ".number_format((float)round($copaRevenue, 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
        	$whiteWings = number_format((float)round($whiteWings, 2, PHP_ROUND_HALF_ODD), 2, '.', ','); 
        	$redeemWings = number_format((float)round($redeemWings, 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
        	$availableWings = number_format((float)round($availableWings, 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
        	$costWings = "$ ".number_format((float)round($costWings, 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
        	$costAvailableWings = number_format((float)round($availableWings, 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
        	$costRedeemWings = "$ ".number_format((float)round($costRedeemWings, 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
        	$costAvailableWings = "$ ".number_format((float)round($costAvailableWings, 2, PHP_ROUND_HALF_ODD), 2, '.', ',');        	

        	$objPHPExcel->setActiveSheetIndex(0)
        	->setCellValue('A'.$i, $code)
        	->setCellValue('B'.$i, $neighborhood)
        	->setCellValue('C'.$i, $basicRoutesRevenue)
        	->setCellValue('D'.$i, $basicRoutesTickets)
        	->setCellValue('E'.$i, $tacticRoutesRevenue)
        	->setCellValue('F'.$i, $tacticRoutesTickets)
        	->setCellValue('G'.$i, $copaRevenue)
        	->setCellValue('H'.$i, $whiteWings)
        	->setCellValue('I'.$i, $redeemWings)
        	->setCellValue('J'.$i, $availableWings)
        	->setCellValue('K'.$i, $costWings)
        	->setCellValue('L'.$i, $costAvailableWings)
        	->setCellValue('M'.$i, $costRedeemWings)
        	->setCellValue('N'.$i, $costAvailableWings);

        	$i++;
        }
    }

    $esReportName = array(
	    'font' => array(
	        'name'      => 'Times New Roman',
	        'bold'      => true,
	        'italic'    => false,
	        'strike'    => false,
	        'size'      => 12,
	        'color'     => array('rgb' => 'FFFFFF')
	    ),
	    'fill' => array(
	        'type'  => PHPExcel_Style_Fill::FILL_SOLID,
	        'color' => array('rgb' => '006699')
	    ),
	    'borders' => array(
	        'allborders' => array(
	            'style' => PHPExcel_Style_Border::BORDER_NONE
	        )
	    ),
	    'alignment' => array(
	        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	        'rotation' => 0,
	        'wrap' => TRUE
	    )
	);
 	$objPHPExcel->getActiveSheet()->getStyle('A1:N1')->applyFromArray($esReportName);

    $esColName = array(
	    'font' => array(
	        'name'  => 'Times new Roman',
	        'bold'  => false,
	        'size'  => 12,
	        'color' => array(
	            'rgb' => '333333'
	        )
	    ),
	    'fill' => array(
	        'type'  => PHPExcel_Style_Fill::FILL_SOLID,
	        'color' => array('rgb' => 'cdcdcd')
	    ),
	    'borders' => array(
	        'allborders' => array(
	            'style' => PHPExcel_Style_Border::BORDER_NONE
	        )
	    ),
	    
	);
	$objPHPExcel->getActiveSheet()->getStyle('A2:N2')->applyFromArray($esColName); 

    $objPHPExcel->getActiveSheet()->setTitle('Reporte Top Agencias');	 
	$objPHPExcel->setActiveSheetIndex(0);	 

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="reporteTopAgencies.xls"');
	header('Cache-Control: max-age=0');

	require_once 'PHPExcel/IOFactory.php';
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
	$objWriter->save('php://output',__FILE__);
	exit;

?>