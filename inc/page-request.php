<?php
	include("db.php");

	if(!isset($session)){
		require('classes/session.class.php');
		$session = new session();
		$session->start_session('_s', false, $db);
	}

	if(isset($_POST['page']))
	{
		$_SESSION['active'] = str_replace ('#','',$_POST['page']);
	}

	if(isset($_SESSION['active']))
	{
		$page = "#".$_SESSION['active'];
		
		$_SESSION['logged'] = true; //set you've logged in
  		$_SESSION['activity'] = time(); //your last activity was now, having logged in.

		switch ($page) 
		{
			case '#dashboard':
				include("../views/dash-main.php");
				break;

			case '#cargar-datos':
				include("../views/dash-upload.php");
				break;

			case '#usuarios':
				include("../views/dash-users.php");
				break;

			case '#add-user':
				include("../views/dash-user-add.php");
				break;

			/* DASHBOARD */

			case '#loyalty':
				unset($_SESSION['query_coms']);
				unset($_SESSION['query_coms_0']);
				unset($_SESSION['query_coms_tac_0']);
				unset($_SESSION['query_coms_tac_1']);
				unset($_SESSION['query_coms_tacs_0']);
				unset($_SESSION['query_coms_tacs_1']);
				unset($_SESSION['query_top_agents']);
				unset($_SESSION['query_top_agencies']);
				include("../views/dash-loyalty.php");
				break;

			case '#loyalty-table':
				include("../views/dash-loyalty-table.php");
				break;

			case '#kpi':
				unset($_SESSION['query']);
				unset($_SESSION['query_coms']);
				unset($_SESSION['query_coms_0']);
				unset($_SESSION['query_coms_tac_0']);
				unset($_SESSION['query_coms_tac_1']);
				unset($_SESSION['query_coms_tacs_0']);
				unset($_SESSION['query_coms_tacs_1']);
				unset($_SESSION['query_top_agents']);
				unset($_SESSION['query_top_agencies']);
				include("../views/dash-kpi.php");
				break;

			case '#top-agent':
				unset($_SESSION['query']);
				unset($_SESSION['query_coms']);
				unset($_SESSION['query_coms_0']);
				unset($_SESSION['query_coms_tac_0']);
				unset($_SESSION['query_coms_tac_1']);
				unset($_SESSION['query_coms_tacs_0']);
				unset($_SESSION['query_coms_tacs_1']);
				unset($_SESSION['query_top_agencies']);
				include("../views/dash-top-agents.php");
				break;

			case '#top-agent-table':
				include("../views/dash-top-agents-table.php");
				break;

			case '#top-agencies':
				unset($_SESSION['query']);
				unset($_SESSION['query_coms']);
				unset($_SESSION['query_coms_0']);
				unset($_SESSION['query_coms_tac_0']);
				unset($_SESSION['query_coms_tac_1']);
				unset($_SESSION['query_coms_tacs_0']);
				unset($_SESSION['query_coms_tacs_1']);
				unset($_SESSION['query_top_agents']);
				include("../views/dash-top-agencies.php");
				break;

			case '#top-agencies-table':
				include("../views/dash-top-agencies-table.php");
				break;

			case '#coms':
				unset($_SESSION['query']);
				unset($_SESSION['query_coms_tac_0']);
				unset($_SESSION['query_coms_tac_1']);
				unset($_SESSION['query_coms_tacs_0']);
				unset($_SESSION['query_coms_tacs_1']);
				unset($_SESSION['query_top_agents']);
				unset($_SESSION['query_top_agencies']);
				include("../views/dash-comunications.php");
				break;

			case '#coms-table':
				include("../views/dash-comunications-table.php");
				break;
			case '#coms-table-0':
				include("../views/dash-comunications-table-1.php");
				break;

			case '#coms-tmail':
				unset($_SESSION['query']);
				unset($_SESSION['query_coms']);
				unset($_SESSION['query_coms_0']);
				unset($_SESSION['query_coms_tacs_0']);
				unset($_SESSION['query_coms_tacs_1']);
				unset($_SESSION['query_top_agents']);
				unset($_SESSION['query_top_agencies']);
				include("../views/dash-coms-tactics-mail.php");
				break;

			case '#coms-tmail-table':
				include("../views/dash-coms-tactics-mail-table.php");
				break;

			case '#coms-tmail-table-0':
				include("../views/dash-coms-tactics-mail-table-1.php");
				break;

			case '#coms-tsms':
				unset($_SESSION['query']);
				unset($_SESSION['query_coms']);
				unset($_SESSION['query_coms_0']);
				unset($_SESSION['query_coms_tac_0']);
				unset($_SESSION['query_coms_tac_1']);
				unset($_SESSION['query_top_agents']);
				unset($_SESSION['query_top_agencies']);
				include("../views/dash-coms-tactics-sms.php");
				break;
			
			case '#coms-tsms-table':
				include("../views/dash-coms-tactics-sms-table.php");
				break;

			case '#coms-tsms-table-0':
				include("../views/dash-coms-tactics-sms-table-1.php");
				break;

			case '#define-neighborhood':
				include("../views/dash-define-neighborhood.php");
				break;


			/* CARGAR DATOS */
			case '#kpi-load':
				include("../views/dash-kpi-submit.php");
				break;

			case '#agent-load':
				$_SESSION['load-data']="agent-load";
				include("../views/dash-xls-submit.php");
				break;

			case '#agency-load':
				$_SESSION['load-data']="agency-load";
				include("../views/dash-xls-submit.php");
				break;

			case '#agent-commail-b-load':
				$_SESSION['load-data']="agent-commail-b-load";
				include("../views/dash-xls-submit.php");
				break;

			case '#agency-commail-b-load':
				$_SESSION['load-data']="agency-commail-b-load";
				include("../views/dash-xls-submit.php");
				break;

			case '#agent-commail-t-load':
				$_SESSION['load-data']="agent-commail-t-load";
				include("../views/dash-xls-submit.php");
				break;

			case '#agency-commail-t-load':
				$_SESSION['load-data']="agency-commail-t-load";
				include("../views/dash-xls-submit.php");
				break;

			case '#agent-comsms-t-load':
				$_SESSION['load-data']="agent-comsms-t-load";
				include("../views/dash-xls-submit.php");
				break;

			case '#agency-comsms-t-load':
				$_SESSION['load-data']="agency-comsms-t-load";
				include("../views/dash-xls-submit.php");
				break;

			case '#coms-load':
				$_SESSION['load-data']="coms-load";
				include("../views/dash-xls-submit.php");
				break;

			/* GRAFICAS */

			case '#participant':
				include("../views/dash-graph-participant.php");
				break;

			case '#behavior':
				include("../views/dash-graph-behavior.php");
				break;

			case '#neighborhood':
				include("../views/dash-graph-neighborhood.php");
				break;

			case '#group-ctrl':
				include("../views/dash-graph-group-ctrl.php");
				break;

			case '#communications':
				include("../views/dash-graph-communications.php");
				break;

			default:
				$_SESSION['active']='loyalty';
				include("../views/dash-loyalty.php");
				break;
		}
	}
	else
	{
		//unset($_SESSION['active']);
		include("views/login.php");
		//var_dump($_SESSION);
	}
	//var_dump($session);
?>