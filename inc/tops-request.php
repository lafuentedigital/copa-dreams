<?php
	include("db.php");

	require('classes/session.class.php');
	$session = new session();
	$session->start_session('_s', false, $db);

	if(isset($_POST['filtersAgents']) && $_POST['filtersAgents'])
	{
		$sql="
            SELECT
              dn_month,dn_year
            FROM
              dashboard_neighborhood_agent
            ORDER BY
              dn_year DESC,
              dn_month DESC
            LIMIT 0,1
        ";

        if($sta=$db->prepare($sql))
        {
          $sta->execute();
          $sta->bind_result($m,$y);
          $sta->fetch();
          $sta->close();
        }

		$join="";
		$conditional=" ";
		$whr=false;
		$order="";
		$group="";
		$and=false;
		$per=false;
		$filterDate="";

		if(isset($_POST['byNeighborhood']))
		{
			if($_POST['byNeighborhood']!='0')
			{
				$v=$_POST['byNeighborhood'];
				$conditional.="WHERE (dn_neighborhood='".$v."')";
				$and=true;
			}
		}

		if(isset($_POST['byTreatment']))
		{
			switch ($_POST['byTreatment']) 
			{
				case '1':{
						$join.="
						INNER JOIN
						        (
						        SELECT 
						                agent_id
						        FROM
						                agent
						        INNER JOIN
						                basic_communications_email_agent
						        ON
						                agent_id=basic_email_agent_agent
						        INNER JOIN
						                tactic_communications_email_agent
						        ON
						                agent_id=tactic_email_agent_agent
						        INNER JOIN
						                tactic_communications_sms_agent
						        ON
						                agent_id=tactic_sms_agent_agent
						        ) as sb2
						ON
						        dn_agent=sb2.agent_id
						";
					break;
				}
				case '2':{

						$join.="
						INNER JOIN
						        (
						        SELECT 
						                agent_id
						        FROM
						                agent
						        INNER JOIN
						                basic_communications_email_agent
						        ON
						                agent_id=basic_email_agent_agent
						        INNER JOIN
						                tactic_communications_email_agent
						        ON
						                agent_id=tactic_email_agent_agent
						        INNER JOIN
						                tactic_communications_sms_agent
						        ON
						                agent_id=tactic_sms_agent_agent
						        ) as sb2
						ON
						        dn_agent<>sb2.agent_id
						";
					break;
				}
			}
		}

		//if(isset($_POST['byPeriodStart']) && isset($_POST['byPeriodFinish']))
		if(isset($_POST['byPeriodStart']))
		{
			$start=explode('-', $_POST['byPeriodStart']);
			//$finish=explode('-', $_POST['byPeriodFinish']);

			if($start[0]>0 && $start[1]>1999)
			{

				if($and){ $conditional.=" AND "; }else{ $conditional=" WHERE "; }
				$conditional.=" (dn_month>=".(int)$start[0]." AND dn_year>=".(int)$start[1].") AND (dn_month<=".$start[0]." AND dn_year<=".$start[1].")";

				$bfrDate=$start[1]."-".$start[0]."-01";
				$nM=$start[0]+1;
				if($start[0]=='12')
				{
					$start[1]=(int)$start[1]+1;
					$nM=1;
				}
				if($nM<10)
				{
					$afrDate=$start[1]."-0".$nM."-01";
				}
				else
				{
					$afrDate=$start[1]."-".$nM."-01";
				}
				$filterDate="AND (rpa0.report_agent_date_flown>='".$bfrDate."' AND rpa0.report_agent_date_flown<'".$afrDate."')";
				$filterReDate="AND (redeem_agent_date_redeem>='".$bfrDate."' AND redeem_agent_date_redeem<'".$afrDate."')";
				$and=true;
				$per=true;
			}
			/*
			if($finish[0]>0 && $finish[1]>1999)
			{
				$conditional.="AND (dn_month<=".$finish[0]." AND dn_year<=".$finish[1].")";
				$and=true;
				$per=true;
			}
			*/

		}

		if(isset($_POST['byQ']))
		{
			$currenYear=(int)date('Y');
			$nextYear=$currenYear+1;

			if(!$per)
			{
				switch ($_POST['byQ']) {
					
					case 'Q1':{
						if($and){ $conditional.=" AND "; }else{ $conditional=" WHERE "; }
						$conditional.=" (dn_month>='1' AND dn_year>=".$currenYear.") AND (dn_month<='3' AND dn_year<=".$currenYear.")";
						$filterDate="AND (rpa0.report_agent_date_flown>='".$currenYear."-01-01' AND rpa0.report_agent_date_flown<'".$currenYear."-04-01')";
						$filterReDate="AND (redeem_agent_date_redeem>='".$currenYear."-01-01' AND redeem_agent_date_redeem<'".$currenYear."-04-01')";
						break;
					}
					
					case 'Q2':{
						if($and){ $conditional.=" AND "; }else{ $conditional=" WHERE "; }
						$conditional.=" (dn_month>='4' AND dn_year>=".$currenYear.") AND (dn_month<='6' AND dn_year<=".$currenYear.")";
						$filterDate="AND (rpa0.report_agent_date_flown>='".$currenYear."-04-01' AND rpa0.report_agent_date_flown<'".$currenYear."-07-01')";
						$filterReDate="AND (redeem_agent_date_redeem>='".$currenYear."-01-01' AND redeem_agent_date_redeem<'".$currenYear."-04-01')";
						break;
					}

					case 'Q3':{
						if($and){ $conditional.=" AND "; }else{ $conditional=" WHERE "; }
						$conditional.=" (dn_month>='7' AND dn_year>=".$currenYear.") AND (dn_month<='9' AND dn_year<=".$currenYear.")";
						$filterDate="AND (rpa0.report_agent_date_flown>='".$currenYear."-07-01' AND rpa0.report_agent_date_flown<'".$currenYear."-10-01')";
						$filterReDate="AND (redeem_agent_date_redeem>='".$currenYear."-01-01' AND redeem_agent_date_redeem<'".$currenYear."-04-01')";
						break;
					}

					case 'Q4':{
						if($and){ $conditional.=" AND "; }else{ $conditional=" WHERE "; }
						$conditional.=" (dn_month>='10' AND dn_year>=".$currenYear.") AND (dn_month<='12' AND dn_year<=".$currenYear.")";
						$filterDate="AND (rpa0.report_agent_date_flown>='".$currenYear."-10-01' AND rpa0.report_agent_date_flown<'".$nextYear."-01-01')";
						$filterReDate="AND (redeem_agent_date_redeem>='".$currenYear."-01-01' AND redeem_agent_date_redeem<'".$nextYear."-01-01')";
						break;
					}
				}
			}
		}

		$order.=" ORDER BY dn_neighborhood DESC";
		$select="
        SELECT
          dn_agent,
          dn_agency,
          dn_neighborhood,
          dn_month,
          dn_year,
          basic_routes_revenue,
          basic_routes_tickets,
          tactic_routes_revenue,
          tactic_routes_tickets,
          copa_revenue,
          white_wings,
          white_plus_wings,
          blue_wings,
          redeem_wings,
          available_wings,
          white_wings_total,
          cost_wings,
          cost_redeem_wings,
          cost_available_wings
        FROM
          (
            SELECT
              *,
              (SELECT SUM(rpa0.report_agent_copa_revenue) FROM report_agent as rpa0 WHERE (rpa0.report_agent_agent=dnat.dn_agent) AND (rpa0.report_agent_basic_route='1')".$filterDate.") as basic_routes_revenue,
              (SELECT COUNT(*) FROM report_agent as rpa0 WHERE (rpa0.report_agent_agent=dnat.dn_agent) AND (rpa0.report_agent_basic_route='1')".$filterDate.") as basic_routes_tickets,
              (SELECT SUM(rpa0.report_agent_copa_revenue) FROM report_agent as rpa0 WHERE (rpa0.report_agent_agent=dnat.dn_agent) AND (rpa0.report_agent_tatic_route='1')".$filterDate.") as tactic_routes_revenue,
              (SELECT COUNT(*) FROM report_agent as rpa0 WHERE (rpa0.report_agent_agent=dnat.dn_agent) AND (rpa0.report_agent_tatic_route='1')".$filterDate.") as tactic_routes_tickets,
              (SELECT SUM(rpa0.report_agent_copa_revenue) FROM report_agent as rpa0 WHERE (rpa0.report_agent_agent=dnat.dn_agent)".$filterDate.") as copa_revenue,
              (SELECT SUM(rpa0.report_agent_white_wings) FROM report_agent as rpa0 WHERE (rpa0.report_agent_agent=dnat.dn_agent)".$filterDate.") as white_wings,
              (SELECT SUM(rpa0.report_agent_white_plus) FROM report_agent as rpa0 WHERE (rpa0.report_agent_agent=dnat.dn_agent)".$filterDate.") as white_plus_wings,
              (SELECT SUM(rpa0.report_agent_blue_wings) FROM report_agent as rpa0 WHERE (rpa0.report_agent_agent=dnat.dn_agent)".$filterDate.") as blue_wings,
              (SELECT SUM(redeem_product_white_wings) FROM redeem_agent INNER JOIN redeem_products ON redeem_agent_product=redeem_product_id WHERE (redeem_agent_agent=dnat.dn_agent) ".$filterReDate.") as redeem_wings,
              (SELECT SUM(rpa0.report_agent_white_wings+rpa0.report_agent_white_plus) FROM report_agent as rpa0 WHERE (rpa0.report_agent_agent=dnat.dn_agent)".$filterDate.") as available_wings,
              (SELECT SUM(rpa0.report_agent_white_wings) FROM report_agent as rpa0 WHERE (rpa0.report_agent_agent=dnat.dn_agent)".$filterDate.") as white_wings_total,
              (SELECT (SUM(ag0.agent_blue_wings_total + ag0.agent_white_wings_total)*4000) FROM agent as ag0 WHERE (ag0.agent_id=dnat.dn_agent) ) as cost_wings,
              (SELECT (SUM(ag0.agent_white_wings_redeemed + ag0.agent_blue_wings_redeemed)*4000) FROM agent as ag0 WHERE (ag0.agent_id=dnat.dn_agent) ) as cost_redeem_wings,
              (SELECT (SUM(ag0.agent_white_wings_available + ag0.agent_blue_wings_available)*4000) FROM agent as ag0 WHERE (ag0.agent_id=dnat.dn_agent) ) as cost_available_wings
		    FROM
		      dashboard_neighborhood_agent as dnat
		  ) as main
        ";

		$query=$select.$join.$conditional.$order;
		$_SESSION['query_top_agents']=$query;
		//echo $query;
		echo "1";
	}


	if(isset($_POST['filtersAgencies']) && $_POST['filtersAgencies'])
	{
		$sql="
            SELECT
              dna_month,dna_year
            FROM
              dashboard_neighborhood_agency
            ORDER BY
              dna_year DESC,
              dna_month DESC
            LIMIT 0,1
        ";

        if($sta=$db->prepare($sql))
        {
          $sta->execute();
          $sta->bind_result($m,$y);
          $sta->fetch();
          $sta->close();
        }

		$join="";
		$conditional=" ";
		$whr=false;
		$order="";
		$group="";
		$and=false;
		$per=false;

		if(isset($_POST['byNeighborhood']))
		{
			if($_POST['byNeighborhood']!='0')
			{
				$v=$_POST['byNeighborhood'];
				$conditional.="WHERE (dna_neighborhood='".$v."')";
				$and=true;
			}
		}

		if(isset($_POST['byTreatment']))
		{
			switch ($_POST['byTreatment']) 
			{
				case '1':{
						$join.="
						INNER JOIN
						        (
						        SELECT 
						                agent_agency
						        FROM
						                agent
						        INNER JOIN
						                basic_communications_email_agent
						        ON
						                agent_id=basic_email_agent_agent
						        INNER JOIN
						                tactic_communications_email_agent
						        ON
						                agent_id=tactic_email_agent_agent
						        INNER JOIN
						                tactic_communications_sms_agent
						        ON
						                agent_id=tactic_sms_agent_agent
						        ) as sb2
						ON
						        dna_agency=sb2.agent_agency
						";
					break;
				}
				case '2':{

						$join.="
						INNER JOIN
						        (
						        SELECT 
						                agent_agency
						        FROM
						                agent
						        INNER JOIN
						                basic_communications_email_agent
						        ON
						                agent_id=basic_email_agent_agent
						        INNER JOIN
						                tactic_communications_email_agent
						        ON
						                agent_id=tactic_email_agent_agent
						        INNER JOIN
						                tactic_communications_sms_agent
						        ON
						                agent_id=tactic_sms_agent_agent
						        ) as sb2
						ON
						        dna_agency<>sb2.agent_agency
						";
					break;
				}
			}
		}

		//if(isset($_POST['byPeriodStart']) && isset($_POST['byPeriodFinish']))
		if(isset($_POST['byPeriodStart']))
		{
			$start=explode('-', $_POST['byPeriodStart']);
			//$finish=explode('-', $_POST['byPeriodFinish']);

			if($start[0]>0 && $start[1]>1999)
			{

				if($and){ $conditional.=" AND "; }else{ $conditional=" WHERE "; }
				$conditional.=" (dna_month>=".(int)$start[0]." AND dna_year>=".(int)$start[1].") AND (dna_month<=".$start[0]." AND dna_year<=".$start[1].")";

				$bfrDate=$start[1]."-".$start[0]."-01";
				$nM=$start[0]+1;
				if($start[0]=='12')
				{
					$start[1]=(int)$start[1]+1;
					$nM=1;
				}
				if($nM<10)
				{
					$afrDate=$start[1]."-0".$nM."-01";
				}
				else
				{
					$afrDate=$start[1]."-".$nM."-01";
				}

				$filterDate="AND (rpa0.report_agent_date_flown>='".$bfrDate."' AND rpa0.report_agent_date_flown<'".$afrDate."')";
				$and=true;
				$per=true;
			}
/*
			if($finish[0]>0 && $finish[1]>1999)
			{
				$conditional.="AND (dna_month<=".$finish[0]." AND dna_year<=".$finish[1].")";
				$and=true;
				$per=true;
			}
*/
		}

		if(isset($_POST['byQ']))
		{
			$currenYear=(int)date('Y');
			$nextYear=$currenYear+1;
			if(!$per)
			{
				switch ($_POST['byQ']) {
					
					case 'Q1':{
						if($and){ $conditional.=" AND "; }else{ $conditional=" WHERE "; }
						$conditional.=" (dna_month>='1' AND dna_year>=".$currenYear.") AND (dna_month<='3' AND dna_year<=".$currenYear.")";
						$filterDate="AND (rpa0.report_agent_date_flown>='".$currenYear."-01-01' AND rpa0.report_agent_date_flown<'".$currenYear."-04-01')";
						break;
					}
					
					case 'Q2':{
						if($and){ $conditional.=" AND "; }else{ $conditional=" WHERE "; }
						$conditional.=" (dna_month>='4' AND dna_year>=".$currenYear.") AND (dna_month<='6' AND dna_year<=".$currenYear.")";
						$filterDate="AND (rpa0.report_agent_date_flown>='".$currenYear."-04-01' AND rpa0.report_agent_date_flown<'".$currenYear."-07-01')";
						break;
					}

					case 'Q3':{
						if($and){ $conditional.=" AND "; }else{ $conditional=" WHERE "; }
						$conditional.=" (dna_month>='7' AND dna_year>=".$currenYear.") AND (dna_month<='9' AND dna_year<=".$currenYear.")";
						$filterDate="AND (rpa0.report_agent_date_flown>='".$currenYear."-07-01' AND rpa0.report_agent_date_flown<'".$currenYear."-10-01')";
						break;
					}

					case 'Q4':{
						if($and){ $conditional.=" AND "; }else{ $conditional=" WHERE "; }
						$conditional.=" (dna_month>='10' AND dna_year>=".$currenYear.") AND (dna_month<='12' AND dna_year<=".$currenYear.")";
						$filterDate="AND (rpa0.report_agent_date_flown>='".$currenYear."-10-01' AND rpa0.report_agent_date_flown<'".$nextYear."-01-01')";
						break;
					}
				}
			}
		}

		$order.=" ORDER BY dna_neighborhood DESC";

		$select="
        SELECT
          dna_agency,
          dna_neighborhood,
          dna_month,
          dna_year,
          basic_routes_revenue,
          basic_routes_tickets,
          tactic_routes_revenue,
          tactic_routes_tickets,
          copa_revenue,
          white_wings,
          redeem_wings,
          available_wings,
          cost_wings,
          cost_redeem_wings,
          cost_available_wings
        FROM
          (
            SELECT
              *,
              (SELECT SUM(rpa0.report_agent_copa_revenue) FROM report_agent as rpa0 INNER JOIN agent as agJ ON rpa0.report_agent_agent=agJ.agent_id WHERE (agJ.agent_agency=dnat.dna_agency) AND (rpa0.report_agent_basic_route='1') ".$filterDate.") as basic_routes_revenue,
              (SELECT COUNT(*) FROM report_agent as rpa0 INNER JOIN agent as agJ ON rpa0.report_agent_agent=agJ.agent_id WHERE (agJ.agent_agency=dnat.dna_agency) AND (rpa0.report_agent_basic_route='1')".$filterDate.") as basic_routes_tickets,
              (SELECT SUM(rpa0.report_agent_copa_revenue) FROM report_agent as rpa0 INNER JOIN agent as agJ ON rpa0.report_agent_agent=agJ.agent_id WHERE (agJ.agent_agency=dnat.dna_agency) AND (rpa0.report_agent_tatic_route='1')".$filterDate.") as tactic_routes_revenue,
              (SELECT COUNT(*) FROM report_agent as rpa0 INNER JOIN agent as agJ ON rpa0.report_agent_agent=agJ.agent_id WHERE (agJ.agent_agency=dnat.dna_agency) AND (rpa0.report_agent_tatic_route='1')".$filterDate.") as tactic_routes_tickets,
              (SELECT SUM(rpa0.report_agent_copa_revenue) FROM report_agent as rpa0 INNER JOIN agent as agJ ON rpa0.report_agent_agent=agJ.agent_id WHERE (agJ.agent_agency=dnat.dna_agency)".$filterDate.") as copa_revenue,
              (SELECT ag0.agency_white_wings_total FROM agency as ag0 WHERE (ag0.agency_id=dnat.dna_agency) ) as white_wings,
              (SELECT ag0.agency_white_wings_redeemed FROM agency as ag0 WHERE (ag0.agency_id=dnat.dna_agency) ) as redeem_wings,
              (SELECT ag0.agency_white_wings_available FROM agency as ag0 WHERE (ag0.agency_id=dnat.dna_agency) ) as available_wings,
              (SELECT (ag0.agency_white_wings_total*4000) FROM agency as ag0 WHERE (ag0.agency_id=dnat.dna_agency) ) as cost_wings,
              (SELECT (ag0.agency_white_wings_redeemed*4000) FROM agency as ag0 WHERE (ag0.agency_id=dnat.dna_agency) ) as cost_redeem_wings,
              (SELECT (ag0.agency_white_wings_available*4000) FROM agency as ag0 WHERE (ag0.agency_id=dnat.dna_agency) ) as cost_available_wings
            FROM
              dashboard_neighborhood_agency as dnat
          ) as main
        ";

		$query=$select.$join.$conditional.$order;
		$_SESSION['query_top_agencies']=$query;
		//echo $query;
		echo "1";
	}

	if(isset($_POST['cleanAg']) && $_POST['cleanAg'])
	{
		unset($_SESSION['query_top_agents']);
		if(!isset($_SESSION['query_top_agents']))
		{
			echo "1";
		}
	}

	if(isset($_POST['cleanAgCy']) && $_POST['cleanAgCy'])
	{
		unset($_SESSION['query_top_agencies']);
		if(!isset($_SESSION['query_top_agencies']))
		{
			echo "1";
		}
	}
?>