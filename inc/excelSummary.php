<?php 
	//para mayor entendimiento dirijase al file excelTopAgent.php

	include("db.php");
	require('classes/session.class.php');

	include('../views/vfunctions.php');
    
	$session = new session();
	$session->start_session('_s', false, $db);
	
	set_include_path(get_include_path() . PATH_SEPARATOR . 'xls/Classes/');
	include 'PHPExcel.php';
	$query = $_SESSION['query'];
	//echo $query;

	$objPHPExcel = new PHPExcel();

	$objPHPExcel->getProperties()->setCreator("Copa");
	$objPHPExcel->getProperties()->setLastModifiedBy("LFD");
	$objPHPExcel->getProperties()->setTitle("Reporte");
	$objPHPExcel->getProperties()->setSubject("Reporte de Copa");
	$objPHPExcel->getProperties()->setDescription("Reporte de Copa Resumen");

	$reportName = "REPORTE RESUMEN"; 
	$colName = array('Vecindario','Total Agentes','Total Ingresos Copa','Cantidad de Tickets');
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:D1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $reportName);
	$lista= array('A','B','C','D');

	for($u=0;$u<sizeof($lista);$u++){
    	$objPHPExcel->getActiveSheet()->getColumnDimension($lista[$u])->setAutoSize(true);
    	$objPHPExcel->getActiveSheet()->getStyle($lista[$u])->getAlignment()
    				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($lista[$u].'2',  $colName[$u]);    	
    }	 


	if($sta=$db->prepare($query)){
        
        $sta->execute(); 
        $sta->bind_result($city,$neighborhood,$dateRecord,$revenue,$routes,$month,$year,$code);

        while ($sta->fetch()){
        	$nt[$neighborhood]['title']=$neighborhood;
            $nt[$neighborhood]['code'][$code]=$code;
            $nt[$neighborhood]['revenue']+=$revenue;
            $nt[$neighborhood]['routes']+=$routes;
        }

        $sta->close();
	}
    
    	
    	if(empty($nt['V8']['title'])){
    		$veci8 = neighborhoodsName('V8');
    		$code = number_format((float)round(sizeof($nt['V8']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$revenue = "$ ".number_format((float)round($nt['V8']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
    		$routes = r_format((float)round($nt['V8']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'A3', $veci8);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'B3', $code);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'C3', $revenue);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'D3', $routes);
    	}else{
    		$veci8 = neighborhoodsName($nt['V8']['title']);
    		$code = number_format((float)round(sizeof($nt['V8']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$revenue = "$ ".number_format((float)round($nt['V8']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
    		$routes = number_format((float)round($nt['V8']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'A3', $veci8);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'B3', $code);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'C3', $revenue);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'D3', $routes);
    	}

    	if(empty($nt['V7']['title'])){
    		$veci7 = neighborhoodsName('V7'); 
    		$code = number_format((float)round(sizeof($nt['V7']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$revenue = "$ ".number_format((float)round($nt['V7']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
    		$routes = number_format((float)round($nt['V7']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'A4', $veci7);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'B4', $code);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'C4', $revenue);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'D4', $routes);
    	}else{ 
    		$veci7 = neighborhoodsName($nt['V7']['title']);
    		$code = number_format((float)round(sizeof($nt['V7']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$revenue = "$ ".number_format((float)round($nt['V7']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
    		$routes = number_format((float)round($nt['V7']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'A4', $veci7); 
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'B4', $code);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'C4', $revenue);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'D4', $routes);
    	}

    	if(empty($nt['V6']['title'])){
    		$veci6 = neighborhoodsName('V6'); 
    		$code = number_format((float)round(sizeof($nt['V6']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$revenue = "$ ".number_format((float)round($nt['V6']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
    		$routes = number_format((float)round($nt['V6']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'A5', $veci6); 
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'B5', $code);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'C5', $revenue);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'D5', $routes);
    	}else{ 
    		$veci6 = neighborhoodsName($nt['V6']['title']);
    		$code = number_format((float)round(sizeof($nt['V6']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$revenue = "$ ".number_format((float)round($nt['V6']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
    		$routes = number_format((float)round($nt['V6']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'A5', $veci6); 
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'B5', $code);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'C5', $revenue);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'D5', $routes);
    	}

    	if(empty($nt['V5']['title'])){
    		$veci5 = neighborhoodsName('V5'); 
    		$code = number_format((float)round(sizeof($nt['V5']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$revenue = "$ ".number_format((float)round($nt['V5']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
    		$routes = number_format((float)round($nt['V5']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'A6', $veci5); 
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'B6', $code);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'C6', $revenue);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'D6', $routes);
    	}else{ 
    		$veci5 = neighborhoodsName($nt['V5']['title']); 
    		$code = number_format((float)round(sizeof($nt['V5']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$revenue = "$ ".number_format((float)round($nt['V5']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
    		$routes = number_format((float)round($nt['V5']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'A6', $veci5);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'B6', $code); 
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'C6', $revenue);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'D6', $routes);
    	}

    	if(empty($nt['V4']['title'])){
    		$veci4 = neighborhoodsName('V4'); 
    		$code = number_format((float)round(sizeof($nt['V4']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$revenue = "$ ".number_format((float)round($nt['V4']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
    		$routes = number_format((float)round($nt['V4']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'A7', $veci4);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'B7', $code); 
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'C7', $revenue);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'D7', $routes);
    	}else{
    		$veci4 = neighborhoodsName($nt['V4']['title']);
    		$code = number_format((float)round(sizeof($nt['V4']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$revenue = "$ ".number_format((float)round($nt['V4']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
    		$routes = number_format((float)round($nt['V4']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'A7', $veci4);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'B7', $code); 
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'C7', $revenue);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'D7', $routes);
    	}

    	if(empty($nt['V3']['title'])){
    		$veci3 = neighborhoodsName('V3'); 
    		$code = number_format((float)round(sizeof($nt['V3']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$revenue = "$ ".number_format((float)round($nt['V3']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
    		$routes = number_format((float)round($nt['V3']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'A8', $veci3); 
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'B8', $code);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'C8', $revenue);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'D8', $routes);
    	}else{
    		$veci3 = neighborhoodsName($nt['V3']['title']); 
    		$code = number_format((float)round(sizeof($nt['V3']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$revenue = "$ ".number_format((float)round($nt['V3']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
    		$routes = number_format((float)round($nt['V3']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'A8', $veci3); 
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'B8', $code);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'C8', $revenue);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'D8', $routes);
    	}

    	if(empty($nt['V2']['title'])){
    		$veci2 = neighborhoodsName('V2'); 
    		$code = number_format((float)round(sizeof($nt['V2']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$revenue = "$ ".number_format((float)round($nt['V2']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
    		$routes = number_format((float)round($nt['V2']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'A9', $veci2); 
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'B9', $code);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'C9', $revenue);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'D9', $routes);    		
    	}else{
    		$veci2 = neighborhoodsName($nt['V2']['title']); 
    		$code = number_format((float)round(sizeof($nt['V2']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$revenue = "$ ".number_format((float)round($nt['V2']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
    		$routes = number_format((float)round($nt['V2']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'A9', $veci2); 
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'B9', $code);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'C9', $revenue);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'D9', $routes);
    	}

    	if(empty($nt['V1']['title'])){
    		$veci1 = neighborhoodsName('V1'); 
    		$code = number_format((float)round(sizeof($nt['V1']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$revenue = "$ ".number_format((float)round($nt['V1']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
    		$routes = number_format((float)round($nt['V1']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'A10', $veci1); 
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'B10', $code);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'C10', $revenue);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'D10', $routes);
    	}else{ 
    		$veci1 = neighborhoodsName($nt['V1']['title']); 
    		$code = number_format((float)round(sizeof($nt['V1']['code']), 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$revenue = "$ ".number_format((float)round($nt['V1']['revenue'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
    		$routes = number_format((float)round($nt['V1']['routes'], 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'A10', $veci1); 
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'B10', $code);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'C10', $revenue);
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( 'D10', $routes);
    	}

    $esReportName = array(
	    'font' => array(
	        'name'      => 'Times New Roman',
	        'bold'      => true,
	        'italic'    => false,
	        'strike'    => false,
	        'size'      => 12,
	        'color'     => array('rgb' => 'FFFFFF')
	    ),
	    'fill' => array(
	        'type'  => PHPExcel_Style_Fill::FILL_SOLID,
	        'color' => array('rgb' => '006699')
	    ),
	    'borders' => array(
	        'allborders' => array(
	            'style' => PHPExcel_Style_Border::BORDER_NONE
	        )
	    ),
	    'alignment' => array(
	        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	        'rotation' => 0,
	        'wrap' => TRUE
	    )
	);
 	$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($esReportName);

    $esColName = array(
	    'font' => array(
	        'name'  => 'Times new Roman',
	        'bold'  => false,
	        'size'  => 12,
	        'color' => array(
	            'rgb' => '333333'
	        )
	    ),
	    'fill' => array(
	        'type'  => PHPExcel_Style_Fill::FILL_SOLID,
	        'color' => array('rgb' => 'cdcdcd')
	    ),
	    'borders' => array(
	        'allborders' => array(
	            'style' => PHPExcel_Style_Border::BORDER_NONE
	        )
	    ),
	    
	);
	$objPHPExcel->getActiveSheet()->getStyle('A2:D2')->applyFromArray($esColName);

    $objPHPExcel->getActiveSheet()->setTitle('Reporte Resumen');	 
	$objPHPExcel->setActiveSheetIndex(0);	 

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="reporteResumen.xls"');
	header('Cache-Control: max-age=0');

	require_once 'PHPExcel/IOFactory.php';
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
	$objWriter->save('php://output',__FILE__);
	exit;


?>