<?php
	include("db.php");

	require('classes/session.class.php');
	$session = new session();
	$session->start_session('_s', false, $db);

	if(isset($_POST['filters']) && $_POST['filters'])
	{
		$sql="
            SELECT
              dn_month,dn_year
            FROM
              dashboard_neighborhood_agent
            ORDER BY
              dn_year DESC,
              dn_month DESC
            LIMIT 0,1
        ";

        if($sta=$db->prepare($sql))
        {
          $sta->execute();
          $sta->bind_result($m,$y);
          $sta->fetch();
          $sta->close();
        }

        $select="";
		$join="";
		$conditional=" WHERE ";
		$order="";
		$group="";
		$and=false;
		$loc=false;
		$rou=false;
		$per=false;

		if(isset($_POST['byCondition']))
		{
			switch ($_POST['byCondition']) 
			{
				case '1':{
					$select="
					SELECT
							(SELECT city_name FROM city INNER JOIN agent ON city_id = agent_city_agency WHERE agent_id=dn_agent) as dn_city,
		                     dn_neighborhood,
		                     dn_date_record,
		                     dn_revenue,
		                     dn_routes_sold,
		                     dn_month,
		                     dn_year,
		                     dn_agent
					FROM
					        dashboard_neighborhood_agent
					";
					/*$join.="
					INNER JOIN
					    (
					    	SELECT 
					    		agent_id, agent_city_agency
					    	FROM 
                        		agent
                			INNER JOIN 
                        		report_agent 
                			ON 
                        		agent_id=report_agent_agent
        				) as ag0 
					ON 
        				dn_agent=ag0.agent_id
					";*/
					$_SESSION['agency']=false;
					break;
				}
				case '2':{
					$select="
					SELECT
							(SELECT city_name FROM city INNER JOIN agent ON city_id = agent_city_agency WHERE agent_agency=dna_agency LIMIT 0,1) as dna_city,
					        dna_neighborhood,
							dna_date_record,
							dna_revenue,
							dna_routes_sold,
							dna_month,
							dna_year,
							dna_agency
					FROM
					        dashboard_neighborhood_agency
					";
					/*$join.="
					INNER JOIN
					    (
					    	SELECT 
					    		agent_id, agent_city_agency, agent_agency
					    	FROM 
                        		agent
                			INNER JOIN 
                        		report_agent 
                			ON 
                        		agent_id=report_agent_agent
        				) as ag1 
					ON 
        				dna_agency=ag1.agent_agency
					";*/
					$_SESSION['agency']=true;
					break;
				}
			}
		}
		else
		{
			$select="
			SELECT
					(SELECT city_name FROM city INNER JOIN agent ON city_id = agent_city_agency WHERE agent_id=dn_agent) as dn_city,
					dn_neighborhood,
					dn_date_record,
					dn_revenue,
					dn_routes_sold,
					dn_month,
					dn_year,
					dn_agent
			FROM
			        dashboard_neighborhood_agent
			";
			/*$join.="
			INNER JOIN
			    (
			    	SELECT 
			    		agent_id, agent_city_agency
			    	FROM 
                		agent
        			INNER JOIN 
                		report_agent 
        			ON 
                		agent_id=report_agent_agent
				) as ag0 
			ON 
				dn_agent=ag0.agent_id
			";*/
			$_SESSION['agency']=false;
		}

		if(isset($_POST['byTreatment']))
		{
			switch ($_POST['byTreatment']) 
			{
				case '1':{
					if($_SESSION['agency'])
					{
						$join.="
						INNER JOIN
						        (
						        SELECT 
						                agency_id
						        FROM
						                agency
						        INNER JOIN
						                basic_communications_email_agency
						        ON
						                agency_id=basic_email_agency_agency
						        INNER JOIN
						                tactic_communications_email_agency
						        ON
						                agency_id=tactic_email_agency_agency
						        INNER JOIN
						                tactic_communications_sms_agency
						        ON
						                agency_id=tactic_sms_agency_agency
						        ) as trt0
						ON
						        dna_agency=trt0.agency_id
						";
					}
					else
					{
						$join.="
						INNER JOIN
						        (
						        SELECT 
						                agent_id
						        FROM
						                agent
						        INNER JOIN
						                basic_communications_email_agent
						        ON
						                agent_id=basic_email_agent_agent
						        INNER JOIN
						                tactic_communications_email_agent
						        ON
						                agent_id=tactic_email_agent_agent
						        INNER JOIN
						                tactic_communications_sms_agent
						        ON
						                agent_id=tactic_sms_agent_agent
						        ) as trt0
						ON
						        dn_agent=trt0.agent_id
						";
					}
					break;
				}
				case '2':{
					if($_SESSION['agency'])
					{
						$join.="
						INNER JOIN
						        (
						        SELECT 
						                agency_id
						        FROM
						                agency
						        INNER JOIN
						                basic_communications_email_agency
						        ON
						                agency_id=basic_email_agency_agency
						        INNER JOIN
						                tactic_communications_email_agency
						        ON
						                agency_id=tactic_email_agency_agency
						        INNER JOIN
						                tactic_communications_sms_agency
						        ON
						                agency_id=tactic_sms_agency_agency
						        ) as trt0
						ON
						        dna_agency<>trt0.agency_id
						";
					}
					else
					{
						$join.="
						INNER JOIN
						        (
						        SELECT 
						                agent_id
						        FROM
						                agent
						        INNER JOIN
						                basic_communications_email_agent
						        ON
						                agent_id=basic_email_agent_agent
						        INNER JOIN
						                tactic_communications_email_agent
						        ON
						                agent_id=tactic_email_agent_agent
						        INNER JOIN
						                tactic_communications_sms_agent
						        ON
						                agent_id=tactic_sms_agent_agent
						        ) as trt0
						ON
						        dn_agent<>trt0.agent_id
						";
					}
					break;
				}
			}
		}

		if(isset($_POST['byPeriodStart']) && $_POST['byPeriodStart']!="")
		{
			$start=explode('-', $_POST['byPeriodStart']);
			if($_SESSION['agency'])
			{
				$per=true;
				if($start[0]>0 && $start[1]>1999)
				{
					if($and){ $conditional.=" AND "; }
					$conditional.=" (dna_month>=".(int)$start[0]." AND dna_year>=".(int)$start[1].") AND (dna_month<=".(int)$start[0]." AND dna_year<=".(int)$start[1].")";
					$and=true;
				}
				else
				{
					if($and){ $conditional.=" AND "; }
					$conditional.=" (dna_month>=".$m." AND dna_year>=".$y.") AND (dna_month<=".$m." AND dna_year<=".$y.")";
					$and=true;
				}
			}
			else
			{
				$per=true;
				if($start[0]>0 && $start[1]>1999)
				{
					if($and){ $conditional.=" AND "; }
					$conditional.=" (dn_month>=".(int)$start[0]." AND dn_year>=".(int)$start[1].") AND (dn_month<=".(int)$start[0]." AND dn_year<=".(int)$start[1].")";
					$and=true;
				}
				else
				{
					if($and){ $conditional.=" AND "; }
					$conditional.=" (dn_month>=".$m." AND dn_year>=".$y.") AND (dn_month<=".$m." AND dn_year<=".$y.")";
					$and=true;
				}
			}

		}

		if(isset($_POST['byLocation']))
		{
			if ($_POST['byLocation']!=0) 
			{

				$city=$_POST['byLocation'];

				if($_SESSION['agency'])
				{
					if($and){ $conditional.=" AND "; }
					$join.="
					INNER JOIN
					        (
					        SELECT
					                agent_agency,
					                agent_city_agency,
					                report_agent_flight_origin,
					                report_agent_flight_destiny,
					                report_agent_tatic_route,
					                report_agent_basic_route,
					                report_agent_agent
					        FROM
					                agent
					        INNER JOIN
					                report_agent
					        ON
					                agent_id=report_agent_agent
					        ) as loc0
					ON
					        dna_agency=loc0.agent_agency
					";

					$conditional.=" (agent_city_agency = '".$city."')";
					$and=true;
					$loc=true;
				}
				else
				{
					if($and){ $conditional.=" AND "; }

					$join.="
					INNER JOIN
					        (
					        SELECT
					                agent_id,
					                agent_city_agency,
					                report_agent_flight_origin,
					                report_agent_flight_destiny,
					                report_agent_tatic_route,
					                report_agent_basic_route,
					                report_agent_agent
					        FROM
					                agent
					        INNER JOIN
					                report_agent
					        ON
					                agent_id=report_agent_agent
					        ) as loc0
					ON
					        dn_agent=loc0.agent_id
					";

					$conditional.=" (agent_city_agency = '".$city."')";
					$and=true;
					$loc=true;
				}
			}
		}

		if(isset($_POST['byRoute']))
		{

			if($_POST['byRoute']!='0')
			{
				$ro=explode("-", $_POST['byRoute']);
				if($and){ $conditional.=" AND "; }

				if($_SESSION['agency'])
				{
					if($loc)
					{
						$rou=true;
					}
					else
					{
						$join.="
						INNER JOIN
						        (
						        SELECT
						                agent_agency,
						                agent_city_agency,
						                report_agent_flight_origin,
						                report_agent_flight_destiny,
						                report_agent_tatic_route,
						                report_agent_basic_route,
						                report_agent_agent
						        FROM
						                agent
						        INNER JOIN
						                report_agent
						        ON
						                agent_id=report_agent_agent
						        ) as rou0
						ON
						        dna_agency=rou0.agent_agency
						";
						$rou=true;
					}
					$group.=" GROUP BY dna_agency,dna_month,dna_year,report_agent_flight_origin,report_agent_flight_destiny";
					$conditional.=" (report_agent_flight_origin = '".$ro[0]."') AND (report_agent_flight_destiny = '".$ro[1]."')";
					$and=true;
				}
				else
				{
					if($loc)
					{
						$rou=true;
					}
					else
					{
						$join.="
						INNER JOIN
						        (
						        SELECT
						                agent_id,
						                agent_city_agency,
						                report_agent_flight_origin,
						                report_agent_flight_destiny,
						                report_agent_tatic_route,
						                report_agent_basic_route,
						                report_agent_agent
						        FROM
						                agent
						        INNER JOIN
						                report_agent
						        ON
						                agent_id=report_agent_agent
						        ) as rou0
						ON
						        dn_agent=rou0.agent_id
						";
						$rou=true;
					}
					$group.=" GROUP BY dn_agent,dn_month,dn_year,report_agent_flight_origin,report_agent_flight_destiny";
					$conditional.=" (report_agent_flight_origin = '".$ro[0]."') AND (report_agent_flight_destiny = '".$ro[1]."')";
					$and=true;
				}
			}
		}

		if(isset($_POST['byRouteType']))
		{
			switch ($_POST['byRouteType']) {
				case '1':{
					if($and){ $conditional.=" AND "; }
					if(!$loc && !$rou)
					{
						if($_SESSION['agency'])
						{
							$join.="
							INNER JOIN
							        (
							        SELECT
							                agent_agency,
							                agent_city_agency,
							                report_agent_flight_origin,
							                report_agent_flight_destiny,
							                report_agent_tatic_route,
							                report_agent_basic_route,
							                report_agent_agent
							        FROM
							                agent
							        INNER JOIN
							                report_agent
							        ON
							                agent_id=report_agent_agent
							        ) as rou1
							ON
							        dna_agency=rou1.agent_agency
							";
						}
						else
						{
							$join.="
							INNER JOIN
							        (
							        SELECT
							                agent_id,
							                agent_city_agency,
							                report_agent_flight_origin,
							                report_agent_flight_destiny,
							                report_agent_tatic_route,
							                report_agent_basic_route,
							                report_agent_agent
							        FROM
							                agent
							        INNER JOIN
							                report_agent
							        ON
							                agent_id=report_agent_agent
							        ) as rou1
							ON
							        dn_agent=rou1.agent_id
							";
						}	
					}
					$conditional.=" (report_agent_tatic_route = '1')";
					$and=true;
					break;
				}
				case '2':{
					if($and){ $conditional.=" AND "; }
					if(!$loc && !$rou)
					{
						if($_SESSION['agency'])
						{
							$join.="
							INNER JOIN
							        (
							        SELECT
							                agent_agency,
							                agent_city_agency,
							                report_agent_flight_origin,
							                report_agent_flight_destiny,
							                report_agent_tatic_route,
							                report_agent_basic_route,
							                report_agent_agent
							        FROM
							                agent
							        INNER JOIN
							                report_agent
							        ON
							                agent_id=report_agent_agent
							        ) as rou1
							ON
							        dna_agency=rou1.agent_agency
							";
						}
						else
						{
							$join.="
							INNER JOIN
							        (
							        SELECT
							                agent_id,
							                agent_city_agency,
							                report_agent_flight_origin,
							                report_agent_flight_destiny,
							                report_agent_tatic_route,
							                report_agent_basic_route,
							                report_agent_agent
							        FROM
							                agent
							        INNER JOIN
							                report_agent
							        ON
							                agent_id=report_agent_agent
							        ) as rou1
							ON
							        dn_agent=rou1.agent_id
							";
						}	
					}
					$conditional.=" (report_agent_basic_route = '1')";
					$and=true;
					break;
				}
			}
		}

		if(isset($_POST['byQ']))
		{
			$currenYear=date('Y');
			
			if($_SESSION['agency'])
			{
				switch ($_POST['byQ']) {
					
					case 'Q1':{
						if($and){ $conditional.=" AND "; }
						$per=true;
						$conditional.=" (dna_month>='1' AND dna_year>=".$currenYear.") AND (dna_month<='3' AND dna_year<=".$currenYear.")";
						break;
					}
					
					case 'Q2':{
						if($and){ $conditional.=" AND "; }
						$per=true;
						$conditional.=" (dna_month>='4' AND dna_year>=".$currenYear.") AND (dna_month<='6' AND dna_year<=".$currenYear.")";
						break;
					}

					case 'Q3':{
						if($and){ $conditional.=" AND "; }
						$per=true;
						$conditional.=" (dna_month>='7' AND dna_year>=".$currenYear.") AND (dna_month<='9' AND dna_year<=".$currenYear.")";
						break;
					}

					case 'Q4':{
						if($and){ $conditional.=" AND "; }
						$per=true;
						$conditional.=" (dna_month>='10' AND dna_year>=".$currenYear.") AND (dna_month<='12' AND dna_year<=".$currenYear.")";
						break;
					}
				}
			}
			else
			{

				switch ($_POST['byQ']) {
					
					case 'Q1':{
						if($and){ $conditional.=" AND "; }
						$per=true;
						$conditional.=" (dn_month>='1' AND dn_year>=".$currenYear.") AND (dn_month<='3' AND dn_year<=".$currenYear.")";
						break;
					}
					
					case 'Q2':{
						if($and){ $conditional.=" AND "; }
						$per=true;
						$conditional.=" (dn_month>='4' AND dn_year>=".$currenYear.") AND (dn_month<='6' AND dn_year<=".$currenYear.")";
						break;
					}

					case 'Q3':{
						if($and){ $conditional.=" AND "; }
						$per=true;
						$conditional.=" (dn_month>='7' AND dn_year>=".$currenYear.") AND (dn_month<='9' AND dn_year<=".$currenYear.")";
						break;
					}

					case 'Q4':{
						if($and){ $conditional.=" AND "; }
						$per=true;
						$conditional.=" (dn_month>='10' AND dn_year>=".$currenYear.") AND (dn_month<='12' AND dn_year<=".$currenYear.")";
						break;
					}
				}
			}
		}

		if($_SESSION['agency'])
		{
			if($group==""){
				$group.=" GROUP BY dna_agency";
			}
			$order.=" ORDER BY dna_agency DESC";
			if(!$per)
			{
				if($and){ $conditional.=" AND "; }
				$conditional.="(dna_month = '".$m."') AND (dna_year = '".$y."')";
			}
		}
		else
		{
			if($group==""){
				$group.=" GROUP BY dn_agent";
			}
			$order.=" ORDER BY dn_agent DESC";

			if(!$per)
			{
				if($and){ $conditional.=" AND "; }
				$conditional.="(dn_month = '".$m."') AND (dn_year = '".$y."')";
			}
		}


		$query=$select.$join.$conditional.$group.$order;
		$_SESSION['query']=$query;
		//echo $query;
		echo "1";
	}

	if (isset($_POST['countrys'])) 
	{
		if($_POST['countrys']!=0)
		{
			$country=$_POST['countrys'];
			
			echo '<option value="0">Ciudad</option>';

			if($sta=$db->prepare("SELECT city_id,city_name FROM  city WHERE city_country='".$country."';"))
          	{
            	$sta->execute();
            	$sta->bind_result($id,$name);

            	

            	while ($sta->fetch()) 
            	{
            		echo '<option value="'.$id.'">'.$name.'</option>';
            	}
            	$sta->close();
            }
        }
        else
        {
        	echo '<option value="0">Selecione un Pais</option>';
        }
	}

	if(isset($_POST['clean']))
	{
		unset($_SESSION['query']);
		if(!isset($_SESSION['query']))
		{
			echo "1";
		}
	}
?>