<?php
	include("db.php");

	require('classes/session.class.php');
	$session = new session();
	$session->start_session('_s', false, $db);

	ini_set('max_execution_time', '-1');

	if(isset($_POST['neighborhood']) && isset($_POST['date']))
	{	
		$today=date('Y-m-d');
		$todayM=(int)date('m');
		$todayY=(int)date('Y');

		$limitAgent=0;
		$eachAgent=0;
		$totalAgents=0;

		$limitAgencies=0;
		$eachAgencies=0;
		$totalAgencies=0;

		$groups=array();
		$agroups=array();
		$agents=array();
		$agencies=array();
		$whereDate="";

		$sql="SELECT main.report_agent_date_flown FROM report_agent as main GROUP BY main.report_agent_date_flown ORDER BY main.report_agent_date_flown DESC";

		$theDateList=array();
		$cnt=1;

		if($stmt=$db->prepare($sql))
		{
		  $stmt->execute();
		  $stmt->bind_result($getDate);

		  while($stmt->fetch())
		  {
		    $d=explode("-", $getDate);
		    $theDate=$d[1]."-".$d[0];

		    if(empty($theDateList[$theDate]))
		    {
		      $sqli="SELECT dn_id FROM dashboard_neighborhood_agent WHERE dn_month=? AND dn_year=? LIMIT 0,1";
		      $id=0;
		      if($stm=$dbi->prepare($sqli))
		      {
		        $stm->bind_param('ii',$d[1],$d[0]);
		        $stm->execute();
		        $stm->bind_result($id);
		        $stm->fetch();
	            if($id==0)
	            {
	              $theDateList[$theDate]['opt']=$cnt;
	              $theDateList[$theDate]['date']=$theDate;
	              $cnt++;
	            }
		        $stm->close();
		      }
		    }
		  }
		  
		  $stmt->close();
		}

		foreach ($theDateList as $row) {
			$d=explode("-", $row['date']);
			if($_POST['date']==$row['opt']){
				$bfrDate=$d[1]."-".$d[0]."-01";
				$nM=$d[0]+1;
				if($d[0]=='12')
				{
					$d[1]=(int)$d[1]+1;
					$nM=1;
				}
				if($nM<10)
				{
					$afrDate=$d[1]."-0".$nM."-01";
				}
				else
				{
					$afrDate=$d[1]."-".$nM."-01";
				}
				
				$found=true;
			}
		}

		//var_dump($bfrDate);
		//var_dump($afrDate);

		/*if (!$found) {
			exit('007');
		}

		$sql="
		SELECT 
			report_agent_date_flown
		FROM 
			report_agent
		ORDER BY 
			report_agent_date_flown DESC 
		LIMIT 0 , 1
		";

		//print_r("EXP1");

		if($stac3=$db->prepare($sql))
        {
          	$stac3->execute();
          	$stac3->bind_result($lChkDate);

      		if($stac3->fetch())
      		{
      			$ltmp=explode("-", $lChkDate);
      			$lChkM=$ltmp[1];
      			$lChkY=$ltmp[0];
      		}

          $stac3->close();
        }

		$sql="
            SELECT
              dn_month,dn_year
            FROM
              dashboard_neighborhood_agent
            ORDER BY
              dn_year DESC,
              dn_month DESC
            LIMIT 1,1
        ";

       //print_r("EXP2");

        if($stac1=$db->prepare($sql))
        {
          	$stac1->execute();
          	$stac1->bind_result($chkM,$chkY);

      		if($stac1->fetch())
      		{
      			if(!empty($chkM) && !empty($chkY))
      			{
	      			if($chkM==$todayM)
	      			{
	      				$chkDate=true;
	      				
	      			}
	      			else if($chkM==$lChkM)
	      			{
	      				$chkDate=true;
	      				
	      			}
	      			else
	      			{
	      				$chkDate=false;
	      				if($chkM==12)
	      				{
	      					$addY=$chkY+1;
	      					$addDate=$addY."-01-01";
	      					$whereDate="
		      				WHERE
		      					(main.report_agent_date_flown>='".$addDate."') 
		      				";
	      				}
	      				else
	      				{
	      					$addM=$chkM+1;
	      					$addDate=$chkY."-".$addM."-01";
	      					$whereDate="
		      				WHERE
		      					(main.report_agent_date_flown>='".$addDate."') 
		      				";
	      				}
	      				
	      				//print_r("DATE: FALSE 1");
	      			}
	      		}
		  		else
		  		{
		  			$chkDate=false;
		  			//print_r("DATE: FALSE 2 ");
		  		}
      		}
      		else
	  		{
	  			$chkDate=false;
	  			//print_r("DATE: FALSE 3");
	  		}
          $stac1->close();
        }
        
        //print_r($whereDate);*/


        if(!$found)
        {
        	echo "007";
        	//print_r("DATE: TRUE");
        }
        else
        {

			$sql="
			SELECT
			    COUNT(DISTINCT report_agent_agent)
			FROM
		        report_agent
			";

			if($stap1 = $db->prepare($sql))
			{
				$stap1->execute();
				$stap1->bind_result($totalAgents);
				$stap1->fetch();

				$limitAgent=$totalAgents;
				$eachAgent=ceil($totalAgents/5);
				$eachAgent=$eachAgent-1;

				$stap1->close();
			}

			if($limitAgent>0)
			{
				$currentRow=0;
				do{
					$currentRow+=$eachAgent;

					$groups[]=$currentRow;

				}while ($currentRow<$limitAgent);
			}
			else
			{
				exit("005");
			}

			/*// ULTIMA FECHA CARGADA
			
			$sql="
	            SELECT
	              dn_month,dn_year
	            FROM
	              dashboard_neighborhood_agent
	            ORDER BY
	              dn_year DESC,
	              dn_month DESC
	            LIMIT 1,1
	        ";

	       
	        if($stac=$db->prepare($sql))
	        {
	          	$stac->execute();
	          	$stac->bind_result($lM,$lY);

	      		if($stac->fetch())
	      		{
	      			//$lVDate=$lM."-".$lY;	
	      			if(!empty($lM) && !empty($lY))
	      			{
		      			if((int)$lM!=12)
		      			{	
		      				$lM=$lM+1;
		      				$lVDate=$lY."-".$lM."-01";

		      			}
		      			else
		      			{
		      				$lM=1;
		      				$lY=$lY+1;
		      				$lVDate=$lY."-".$lM."-01";
		      			}
		      		}
			  		else
			  		{
			  			$lVDate=NULL;
			  		}
	      		}
	      		else
		  		{
		  			$lVDate=NULL;
		  		}
	          $stac->close();
	        }

	        // PRIMERA FECHA CARGADA

	        $sql="
	            SELECT
	              dn_month,dn_year
	            FROM
	              dashboard_neighborhood_agent
	            ORDER BY
	              dn_year ASC,
	              dn_month ASC
	            LIMIT 1,1
	        ";

	       
	        if($stac=$db->prepare($sql))
	        {
	          	$stac->execute();
	          	$stac->bind_result($fM,$fY);

	      		if($stac->fetch())
	      		{
	      			//$lVDate=$lM."-".$lY;	
	      			if(!empty($fM) && !empty($fY))
	      			{
		      			$fVDate=$fY."-".$fM."-01";
		      		}
			  		else
			  		{
			  			$fVDate=NULL;
			  		}
	      		}
	      		else
		  		{
		  			$fVDate=NULL;
		  		}
	          $stac->close();
	        }*/


	        # AQUI SE COMIENZA A SELECCIONAR EL GRUPO EN  y PARA LAS FECHAS

				$sql="
					SELECT
						main.report_agent_agent,
						(
							SELECT x.agent_agency FROM agent as x WHERE x.agent_id=main.report_agent_agent
						) as report_agent_agency,
						(
							SELECT 
								COUNT(DISTINCT route_count.report_agent_flight_origin,route_count.report_agent_flight_destiny)
							FROM 
								report_agent as route_count
							WHERE 
								(route_count.report_agent_agent = main.report_agent_agent) AND (route_count.report_agent_date_flown>=? AND route_count.report_agent_date_flown<?)
						) as report_agent_routes_sales,
						(	SELECT
								date_flown.report_agent_date_flown
							FROM
								report_agent as date_flown
							WHERE
								(date_flown.report_agent_agent=main.report_agent_agent) AND (date_flown.report_agent_date_flown>=? AND date_flown.report_agent_date_flown<?)
							ORDER BY
								date_flown.report_agent_date_flown DESC
							LIMIT 0,1
						) as report_agent_date
					FROM
						report_agent as main
					GROUP BY
						main.report_agent_agent
					ORDER BY
						report_agent_routes_sales DESC
				";

			/*$sql='
			SELECT
				main.report_agent_agent,
				(
					SELECT x.agent_agency FROM agent as x WHERE x.agent_id=main.report_agent_agent
				) as report_agent_agency,
				(
					SELECT 
						COUNT(DISTINCT route_count.report_agent_flight_origin,route_count.report_agent_flight_destiny)
					FROM 
						report_agent as route_count
					WHERE 
						(route_count.report_agent_agent = main.report_agent_agent) AND (route_count.report_agent_date_flown = main.report_agent_date_flown)
				) as report_agent_routes_sales,
                main.report_agent_date_flown as report_agent_date
			FROM
				report_agent as main
			GROUP BY
				report_agent_date
			ORDER BY
				report_agent_routes_sales DESC
			';*/

			if($sta = $db->prepare($sql))
			{
				$sta->bind_param('ssss',$bfrDate,$afrDate,$bfrDate,$afrDate);
				$sta->execute();
				$sta->bind_result($agent,$agency,$routes,$vDate);
				$thisRow=0;
				while($sta->fetch())
				{
					$routes=(int)$routes;

					#New format date mm-yyyy
					if($vDate==NULL)
					{
						$vDate=$bfrDate;
					}

					$d=explode("-", $vDate);
					$vPeriod=$d[1]."-".$d[0];

	                $todayMY=$todayY."-".$todayM."-01";

	                $rowChk=false;

	            	// CHEQUEAMOS SI LA FECHA YA FUE CARGADA.
	       			$chkM=$d[1];
	       			$chkY=$d[0];

			        if($stmt = $dbi->prepare('SELECT dn_id FROM dashboard_neighborhood_agent WHERE (dn_month=? AND dn_year=?) LIMIT 0,1'))
			        {
			        	$response='';
			        	$stmt->bind_param("ii",$chkM,$chkY);
			        	$stmt->execute();
			        	$stmt->bind_result($response);
			        	$stmt->fetch();
			        	$stmt->close();
			        }
			        else
			        {
			        	//echo "ERROR!\n";
			        }

	            	if($response==0 && $vDate<$todayMY)
	                {
	                	//echo $chkY."::".$chkM."::".$response."\n";

	                	$agents[$agent][$vPeriod]['agent']=$agent;
						$agents[$agent][$vPeriod]['agency']=$agency;

		            	if(empty($agents[$agent][$vPeriod]['routes']))
		            	{
		            		$agents[$agent][$vPeriod]['routes']=$routes;
		            	}
		            	else
		            	{
		            		$agents[$agent][$vPeriod]['routes']+=$routes;
		            	}
		            	$rowChk=true;
	                }

		            if($rowChk)
		            {
			            if($thisRow<=$groups[0]) //-=176
						{
							$agents[$agent][$vPeriod]['z']=5;
						}
						else if($thisRow>$groups[0] && $thisRow<=$groups[1]) // +176 -=352
						{
							$agents[$agent][$vPeriod]['z']=4;
						}
						else if ($thisRow>$groups[1] && $thisRow<=$groups[2]) { // +352 -=528
							$agents[$agent][$vPeriod]['z']=3;
						}
						else if ($thisRow>$groups[2] && $thisRow<=$groups[3]) { // +528 -=704
							$agents[$agent][$vPeriod]['z']=2;
						}
						else if ($thisRow>$groups[3]) { // +704
							$agents[$agent][$vPeriod]['z']=1;
						}

						$agents[$agent][$vPeriod]['z_pos']=$thisRow;
					}
					$thisRow++;
				}
				$sta->reset();
			}

			# AHORA CON LAS FECHAS

			$sql="
				SELECT
					main.report_agent_agent,
					(	SELECT
							date_flown.report_agent_date_flown
						FROM
							report_agent as date_flown
						WHERE
							(date_flown.report_agent_agent=main.report_agent_agent) AND (date_flown.report_agent_date_flown>=? AND date_flown.report_agent_date_flown<?)
						ORDER BY
							date_flown.report_agent_date_flown DESC
						LIMIT 0,1
					) as report_agent_date
				FROM
					report_agent as main
				GROUP BY
					main.report_agent_agent
				ORDER BY
					report_agent_date DESC
			";

			/*$sql='
			SELECT
				main.report_agent_agent,
                main.report_agent_date_flown as report_agent_date
			FROM
				report_agent as main
			GROUP BY
				report_agent_date
			ORDER BY
				report_agent_routes_sales DESC
			';*/

			if($sta = $db->prepare($sql))
			{
				$sta->bind_param('ss',$bfrDate,$afrDate);
				$sta->execute();
				$sta->bind_result($agent,$vDate);
				$thisRow=0;
				while($sta->fetch())
				{
					#New format date mm-yyyy
					if($vDate==NULL){
						$vDate=$bfrDate;
					}

					$d=explode("-", $vDate);
					$vPeriod=$d[1]."-".$d[0];

	                $todayMY=$todayY."-".$todayM."-01";

	                $rowChk=false;
	                
	            	// RECHEQUEAMOS SI LA FECHA YA FUE CARGADA.
	       			$chkM=$d[1];
	       			$chkY=$d[0];

			        if($stmt = $dbi->prepare('SELECT dn_id FROM dashboard_neighborhood_agent WHERE (dn_month=? AND dn_year=?) LIMIT 0,1'))
			        {
			        	$response='';
			        	$stmt->bind_param("ii",$chkM,$chkY);
			        	$stmt->execute();
			        	$stmt->bind_result($response);
			        	$stmt->fetch();
			        	$stmt->close();
			        }
			        else
			        {
			        	//echo "ERROR!\n";
			        }

	            	if($response==0 && $vDate<$todayMY)
	                {
	                	if(empty($agents[$agent][$vPeriod]['period']))
	                	{
	                		$agents[$agent][$vPeriod]['period']=$vDate;
	                	}
	                	else
	                	{
	                		if($agents[$agent][$vPeriod]['period']>$vDate)
	                		{
	                			$agents[$agent][$vPeriod]['period']=$vDate;
	                		}
	                	}
	                	$rowChk=true;
	                }

		            if($rowChk)
		            {
			            if($thisRow<=$groups[0]) //-=176
						{
							$agents[$agent][$vPeriod]['y']=5;
						}
						else if($thisRow>$groups[0] && $thisRow<=$groups[1]) // +176 -=352
						{
							$agents[$agent][$vPeriod]['y']=4;
						}
						else if ($thisRow>$groups[1] && $thisRow<=$groups[2]) { // +352 -=528
							$agents[$agent][$vPeriod]['y']=3;
						}
						else if ($thisRow>$groups[2] && $thisRow<=$groups[3]) { // +528 -=704
							$agents[$agent][$vPeriod]['y']=2;
						}
						else if ($thisRow>$groups[3]) { // +704
							$agents[$agent][$vPeriod]['y']=1;
						}
						$agents[$agent][$vPeriod]['y_pos']=$thisRow;
					}
					$thisRow++;
				}
				$sta->reset();
			}

			#AHORA CON LOS MONTOS A GANAR

			$sql="
				SELECT
					main.report_agent_agent,
					(	SELECT
							date_flown.report_agent_date_flown
						FROM
							report_agent as date_flown
						WHERE
							(date_flown.report_agent_agent=main.report_agent_agent) AND (date_flown.report_agent_date_flown>=? AND date_flown.report_agent_date_flown<?)
						ORDER BY
							date_flown.report_agent_date_flown DESC
						LIMIT 0,1
					) as report_agent_date,
					(
						SELECT 
							SUM(revenue.report_agent_copa_revenue) 
						FROM 
							report_agent as revenue
						WHERE 
							(revenue.report_agent_agent = main.report_agent_agent) AND (revenue.report_agent_date_flown>=? AND revenue.report_agent_date_flown<?)
					) as report_agent_revenue
				FROM
					report_agent as main
				GROUP BY
					main.report_agent_agent
				ORDER BY
					report_agent_revenue DESC
			";

			/*$sql='
			SELECT
				main.report_agent_agent,
                main.report_agent_date_flown as report_agent_date,
                (
					SELECT 
						SUM(revenue.report_agent_copa_revenue) 
					FROM 
						report_agent as revenue
					WHERE 
						(revenue.report_agent_agent = main.report_agent_agent) AND (route_count.report_agent_date_flown = main.report_agent_date_flown)
				) as report_agent_revenue
			FROM
				report_agent as main
			GROUP BY
				report_agent_date
			ORDER BY
				report_agent_routes_sales DESC
			';*/

			if($sta = $db->prepare($sql))
			{
				$sta->bind_param('ssss',$bfrDate,$afrDate,$bfrDate,$afrDate);
				$sta->execute();
				$sta->bind_result($agent,$vDate,$revenue);
				$thisRow=0;
				while($sta->fetch())
				{

					#New format date mm-yyyy
					if($vDate==NULL){
						$vDate=$bfrDate;
					}

					if($revenue==NULL){
						$revenue=0;
					}

					$d=explode("-", $vDate);
					$vPeriod=$d[1]."-".$d[0];

	                $todayMY=$todayY."-".$todayM."-01";
	                
	                $rowChk=false;

	            	// RECHEQUEAMOS SI LA FECHA YA FUE CARGADA.
	       			$chkM=$d[1];
	       			$chkY=$d[0];

			        if($stmt = $dbi->prepare('SELECT dn_id FROM dashboard_neighborhood_agent WHERE (dn_month=? AND dn_year=?) LIMIT 0,1'))
			        {
			        	$response='';
			        	$stmt->bind_param("ii",$chkM,$chkY);
			        	$stmt->execute();
			        	$stmt->bind_result($response);
			        	$stmt->fetch();
			        	$stmt->close();
			        }
			        else
			        {
			        	//echo "ERROR!\n";
			        }

	            	if($response==0 && $vDate<$todayMY)
	                {
	                	if(empty($agents[$agent][$vPeriod]['revenue']))
		            	{
		            		$agents[$agent][$vPeriod]['revenue']=$revenue;
		            	}
		            	else
		            	{
		            		$agents[$agent][$vPeriod]['revenue']+=$revenue;
		            	}
		            	$rowChk=true;
		            }

		            if($rowChk)
		            {
			            if($thisRow<=$groups[0]) //-=176
						{
							$agents[$agent][$vPeriod]['x']=5;
						}
						else if($thisRow>$groups[0] && $thisRow<=$groups[1]) // +176 -=352
						{
							$agents[$agent][$vPeriod]['x']=4;
						}
						else if ($thisRow>$groups[1] && $thisRow<=$groups[2]) { // +352 -=528
							$agents[$agent][$vPeriod]['x']=3;
						}
						else if ($thisRow>$groups[2] && $thisRow<=$groups[3]) { // +528 -=704
							$agents[$agent][$vPeriod]['x']=2;
						}
						else if ($thisRow>$groups[3]) { // +704
							$agents[$agent][$vPeriod]['x']=1;
						}
						$agents[$agent][$vPeriod]['x_pos']=$thisRow;
					}
					$thisRow++;
				}
				$sta->reset();
			}

			//print_r($groups);
			//print_r("---------------------------");
			//print_r($agents);
			$allRows=0;
			$error=0;
			$succes=0;
			
			if(!empty($agents))
			{
				foreach ($agents as $r) 
				{
					foreach ($r as $row)
					{
					
						if($row['y']<=2 && $row['x']<=2 && $row['z']<=2)
						{
							$nbh='V1';
						}
						else if ($row['y']<=2 && $row['x']<=2 && $row['z']>=3) {
							$nbh='V2';
						}
						else if ($row['y']<=2 && $row['x']>=3 && $row['z']<=2) {
							$nbh='V3';
						}
						else if ($row['y']<=2 && $row['x']>=3 && $row['z']>=3) {
							$nbh='V4';
						}
						else if ($row['y']>=3 && $row['x']<=2 && $row['z']<=2) {
							$nbh='V5';
						}
						else if ($row['y']>=3 && $row['x']<=2 && $row['z']>=3) {
							$nbh='V6';
						}
						else if ($row['y']>=3 && $row['x']>=3 && $row['z']<=2) {
							$nbh='V7';
						}
						else if ($row['y']>=3 && $row['x']>=3 && $row['z']>=3) {
							$nbh='V8';
						}

						#New format date mm-yyyy
						$d=explode("-", $row['period']);
						$m=$d[1];
						$y=$d[0];

						// print_r("#----------------");
						// print_r($row);
						// print_r($nbh);

						if($sta2 = $db->prepare("INSERT INTO dashboard_neighborhood_agent (dn_neighborhood,dn_agent,dn_agency,dn_date_record,dn_month,dn_year,dn_revenue,dn_routes_sold) VALUES (?,?,?,?,?,?,?,?)"))
						{
							$sta2->bind_param('ssisiidi',$nbh,$row['agent'],$row['agency'],$today,$m,$y,$row['revenue'],$row['routes']);
						
							if(!$sta2->execute())
			                {
			                    $error++;
			     // 				print_r("#----------------");
								// print_r($row);
								// print_r($nbh);
			     // 				print_r("::0::");
			                }
			                else
			                {
			                    $succes++;
			     // 				print_r("#----------------");
								// print_r($row);
								// print_r($nbh);
			     // 				print_r("::1::");
			                }
							$sta2->close();
						}

						$allRows++;
					}
				}
			}
			
			//echo $allRows."::".$succes."::".$error;
			//echo "::".$allRows."::";
			
			if($allRows==$succes)
			{
				$allRows=0;
				$error=0;
				$succes=0;

				$sql="
				SELECT 
					COUNT( DISTINCT dn_agency ) 
				FROM 
					dashboard_neighborhood_agent
				";

				if($stap1 = $db->prepare($sql))
				{
					$stap1->execute();
					$stap1->bind_result($totalAgencies);
					$stap1->fetch();

					$limitAgencies=$totalAgencies;
					$eachAgencies=ceil($totalAgencies/5);
					$eachAgencies=$eachAgencies-1;

					$stap1->close();
				}

				if($limitAgencies>0)
				{
					$currentRow=0;
					do{
						$currentRow+=$eachAgencies;

						$agroups[]=$currentRow;

					}while ($currentRow<$limitAgencies);
				}
				else
				{
					exit("006");
				}

				#Evaluar vencindarios por agencias.
				/*$sql="
	                SELECT
	                  dna_month,dna_year
	                FROM
	                  dashboard_neighborhood_agency
	                ORDER BY
	                  dna_year DESC,
	                  dna_month DESC
	                LIMIT 0,1
	            ";

	            if($staa=$db->prepare($sql))
	            {
					$staa->execute();
					$staa->bind_result($laM,$laY);

					if($staa->fetch())
					{
						//$laVDate=$laM."-".$laY;
						if(!empty($laM) && !empty($laY))
		      			{
			      			if((int)$laM!=12)
			      			{	
			      				$laM=$laM+1;
			      				$laVDate=$laY."-".$laM."-01";

			      			}
			      			else
			      			{
			      				$laM=1;
			      				$laY=$laY+1;
			      				$laVDate=$laY."-".$laM."-01";
			      			}
			      		}
				  		else
				  		{
				  			$laVDate=NULL;
				  		}
					}
					else
					{
					$laVDate=NULL;
					}
					$staa->reset();
	            }

	            // PRIMERA FECHA PARA LAS AGENCIAS

	            $sql="
	                SELECT
	                  dna_month,dna_year
	                FROM
	                  dashboard_neighborhood_agency
	                ORDER BY
	                  dna_year ASC,
	                  dna_month ASC
	                LIMIT 0,1
	            ";

	            if($staa=$db->prepare($sql))
	            {
					$staa->execute();
					$staa->bind_result($faM,$faY);

					if($staa->fetch())
					{
						//$laVDate=$laM."-".$laY;
						if(!empty($faM) && !empty($faY))
		      			{
			      			$faVDate=$faY."-".$faM."-01";
			      		}
				  		else
				  		{
				  			$faVDate=NULL;
				  		}
					}
					else
					{
						$faVDate=NULL;
					}
					$staa->reset();
	            }*/

	            # AHORA VAMOS CON EL CALCULO DE VECINDARIOS PARA LAS AGENCIAS, COMENZAMOS CON EL EJE x

	            $dAg=explode("-", $bfrDate);
	            $m=(int)$dAg[1]; 
	            $y=(int)$dAg[0];
	            //echo $m."\n";
	            //echo $y."\n";

				$sql1="SELECT t.dn_agency, (SELECT SUM(x.dn_revenue) FROM dashboard_neighborhood_agent as x WHERE (x.dn_agency=t.dn_agency) AND (x.dn_month=? AND x.dn_year=?)) as revenue, (SELECT report_agent_date_flown FROM report_agent INNER JOIN agent ON report_agent_agent=agent_id WHERE (agent_agency=dn_agency) AND (report_agent_date_flown>=? AND report_agent_date_flown<?) ORDER BY report_agent_date_flown DESC LIMIT 0,1) as dn_last_sale FROM dashboard_neighborhood_agent as t GROUP BY t.dn_agency ORDER BY revenue DESC";

				if($stad = $db->prepare($sql1))
				{
					$stad->bind_param('iiss',$m,$y,$bfrDate,$afrDate);
					$stad->execute();
					$stad->bind_result($aagency, $arevenue, $avDate);
					$thisRow=0;


					while($stad->fetch())
					{

						if($avDate==NULL){
							$avDate=$bfrDate;
						}

						if($arevenue==NULL){
							$arevenue=0;
						}

						//echo "revenue: ".$arevenue." date: ".$avDate."\n";

						$d=explode("-", $avDate);
						$vDate=$d[1]."-".$d[0];

		                $todayMY=$todayY."-".$todayM."-01";

		                $rowChk=false;

						// RECHEQUEAMOS SI LA FECHA YA FUE CARGADA.
		       			$chkM=$d[1];
		       			$chkY=$d[0];

				        if($stmt = $dbi->prepare('SELECT dna_id FROM dashboard_neighborhood_agency WHERE (dna_month=? AND dna_year=?) LIMIT 0,1'))
				        {
				        	$response='';
				        	$stmt->bind_param("ii",$chkM,$chkY);
				        	$stmt->execute();
				        	$stmt->bind_result($response);
				        	$stmt->fetch();
				        	$stmt->close();
				        }
				        else
				        {
				        	//echo "ERROR!\n";
				        }

		            	if($response==0 && $avDate<$todayMY)
		                {
							$agencies[$aagency][$vDate]['agency']=$aagency;

							//$agencies[$aag][$vDate]['period']=$vDate;

							if(empty($agencies[$aag][$vDate]['revenue']))
			            	{
			            		$agencies[$aagency][$vDate]['revenue']=$arevenue;
			            	}
			            	else
			            	{
			            		$agencies[$aagency][$vDate]['revenue']+=$arevenue;
			            	}
			            	$rowChk=true;
						}

						if($rowChk)
						{
							if(empty($agencies[$aagency][$vDate]['x']))
							{
								if($thisRow<=$agroups[0]) //-=176
								{
									$agencies[$aagency][$vDate]['x']=5;
								}
								else if($thisRow>$agroups[0] && $thisRow<=$agroups[1]) // +176 -=352
								{
									$agencies[$aagency][$vDate]['x']=4;
								}
								else if ($thisRow>$agroups[1] && $thisRow<=$agroups[2]) { // +352 -=528
									$agencies[$aagency][$vDate]['x']=3;
								}
								else if ($thisRow>$agroups[2] && $thisRow<=$agroups[3]) { // +528 -=704
									$agencies[$aagency][$vDate]['x']=2;
								}
								else if ($thisRow>$agroups[3]) { // +704
									$agencies[$aagency][$vDate]['x']=1;
								}
							}

							if(empty($agencies[$aagency][$vDate]['x_pos']))
							{
								$agencies[$aagency][$vDate]['x_pos']=$thisRow;
							}
						}

						$thisRow++;
					}
					$stad->reset();
				}

				# AHORA VAMOS CON LAS FECHAS EN y

				$sql1='SELECT dn_agency, (SELECT report_agent_date_flown FROM report_agent INNER JOIN agent ON report_agent_agent=agent_id WHERE (agent_agency=dn_agency) AND (report_agent_date_flown>=? AND report_agent_date_flown<?) ORDER BY report_agent_date_flown DESC LIMIT 0,1) as dn_last_sale FROM dashboard_neighborhood_agent GROUP BY dn_agency ORDER BY dn_last_sale DESC';

				if($stad = $db->prepare($sql1))
				{
					$stad->bind_param('ss',$bfrDate,$afrDate);
					$stad->execute();
					$stad->bind_result($aagency, $avDate);
					$thisRow=0;

					while($stad->fetch())
					{
						if($avDate==NULL){
							$avDate=$bfrDate;
						}

						$d=explode("-", $avDate);
						$vDate=$d[1]."-".$d[0];

		                $todayMY=$todayY."-".$todayM."-01";

		                $rowChk=false;

						// RECHEQUEAMOS SI LA FECHA YA FUE CARGADA.
		       			$chkM=$d[1];
		       			$chkY=$d[0];

				        if($stmt = $dbi->prepare('SELECT dna_id FROM dashboard_neighborhood_agency WHERE (dna_month=? AND dna_year=?) LIMIT 0,1'))
				        {
				        	$response='';
				        	$stmt->bind_param("ii",$chkM,$chkY);
				        	$stmt->execute();
				        	$stmt->bind_result($response);
				        	$stmt->fetch();
				        	$stmt->close();
				        }
				        else
				        {
				        	//echo "ERROR!\n";
				        }

		            	if($response==0 && $avDate<$todayMY)
		                {
		                	if(empty($agencies[$aagency][$vDate]['period']))
		                	{
		                		$agencies[$aagency][$vDate]['period']=$avDate;
		                	}
		                	else
		                	{
		                		if($agencies[$aagency][$vDate]['period']>$avDate)
		                		{
		                			$agencies[$aagency][$vDate]['period']=$avDate;
		                		}
		                	}
		                	$rowChk=true;
		                }

						if($rowChk)
						{

							if(empty($agencies[$aagency][$vDate]['y']))
							{
								if($thisRow<=$agroups[0]) //-=176
								{
									$agencies[$aagency][$vDate]['y']=5;
								}
								else if($thisRow>$agroups[0] && $thisRow<=$agroups[1]) // +176 -=352
								{
									$agencies[$aagency][$vDate]['y']=4;
								}
								else if ($thisRow>$agroups[1] && $thisRow<=$agroups[2]) { // +352 -=528
									$agencies[$aagency][$vDate]['y']=3;
								}
								else if ($thisRow>$agroups[2] && $thisRow<=$agroups[3]) { // +528 -=704
									$agencies[$aagency][$vDate]['y']=2;
								}
								else if ($thisRow>$agroups[3]) { // +704
									$agencies[$aagency][$vDate]['y']=1;
								}
							}
							if(empty($agencies[$aagency][$vDate]['y_pos']))
							{
								$agencies[$aagency][$vDate]['y_pos']=$thisRow;
							}
						}

						$thisRow++;

					}

					$stad->reset();
				}

				# AHORA VAMOS CON LAS RUTAS EN z

				$sql1='SELECT t.dn_agency, (SELECT SUM(x.dn_routes_sold) FROM dashboard_neighborhood_agent as x WHERE (x.dn_agency=t.dn_agency) AND (x.dn_month=? AND x.dn_year=?)) as routes , (SELECT report_agent_date_flown FROM report_agent INNER JOIN agent ON report_agent_agent=agent_id WHERE (agent_agency=dn_agency) AND (report_agent_date_flown>=? AND report_agent_date_flown<?) ORDER BY report_agent_date_flown DESC LIMIT 0,1) as dn_last_sale FROM dashboard_neighborhood_agent as t GROUP BY t.dn_agency ORDER BY routes DESC';

				if($stad = $db->prepare($sql1))
				{
					$stad->bind_param('iiss',$m,$y,$bfrDate,$afrDate);
					$stad->execute();
					$stad->bind_result($aagency, $aroutes, $avDate);
					$thisRow=0;

					while($stad->fetch())
					{
						if($avDate==NULL){
							$avDate=$bfrDate;
						}

						$d=explode("-", $avDate);
						$vDate=$d[1]."-".$d[0];

		                $todayMY=$todayY."-".$todayM."-01";

		                $rowChk=false;

						// RECHEQUEAMOS SI LA FECHA YA FUE CARGADA.
		       			$chkM=$d[1];
		       			$chkY=$d[0];

				        if($stmt = $dbi->prepare('SELECT dna_id FROM dashboard_neighborhood_agency WHERE (dna_month=? AND dna_year=?) LIMIT 0,1'))
				        {
				        	$response='';
				        	$stmt->bind_param("ii",$chkM,$chkY);
				        	$stmt->execute();
				        	$stmt->bind_result($response);
				        	$stmt->fetch();
				        	$stmt->close();
				        }
				        else
				        {
				        	//echo "ERROR!\n";
				        }

		            	if($response==0 && $avDate<$todayMY)
		                {
		                	if(empty($agencies[$aagency][$vDate]['routes']))
			            	{
			            		$agencies[$aagency][$vDate]['routes']=$aroutes;
			            	}
			            	else
			            	{
			            		$agencies[$aagency][$vDate]['routes']+=$aroutes;
			            	}
			            	$rowChk=true;
			            }

						if($rowChk)
						{

							if(empty($agencies[$aagency][$vDate]['z']))
							{
								if($thisRow<=$agroups[0]) //-=176
								{
									$agencies[$aagency][$vDate]['z']=5;
								}
								else if($thisRow>$agroups[0] && $thisRow<=$agroups[1]) // +176 -=352
								{
									$agencies[$aagency][$vDate]['z']=4;
								}
								else if ($thisRow>$agroups[1] && $thisRow<=$agroups[2]) { // +352 -=528
									$agencies[$aagency][$vDate]['z']=3;
								}
								else if ($thisRow>$agroups[2] && $thisRow<=$agroups[3]) { // +528 -=704
									$agencies[$aagency][$vDate]['z']=2;
								}
								else if ($thisRow>$agroups[3]) { // +704
									$agencies[$aagency][$vDate]['z']=1;
								}
							}

							if(empty($agencies[$aagency][$vDate]['z_pos']))
							{

								$agencies[$aagency][$vDate]['z_pos']=$thisRow;
							}
						}

						$thisRow++;
					}

					$stad->reset();
				}

				//print_r($agencies);
				$row=null;

				if(!empty($agencies))
				{
					foreach ($agencies as $re) 
					{
						foreach ($re as $row) 
						{
						
							if($row['y']<=2 && $row['x']<=2 && $row['z']<=2)
							{
								$nbh='V1';
							}
							else if ($row['y']<=2 && $row['x']<=2 && $row['z']>=3) {
								$nbh='V2';
							}
							else if ($row['y']<=2 && $row['x']>=3 && $row['z']<=2) {
								$nbh='V3';
							}
							else if ($row['y']<=2 && $row['x']>=3 && $row['z']>=3) {
								$nbh='V4';
							}
							else if ($row['y']>=3 && $row['x']<=2 && $row['z']<=2) {
								$nbh='V5';
							}
							else if ($row['y']>=3 && $row['x']<=2 && $row['z']>=3) {
								$nbh='V6';
							}
							else if ($row['y']>=3 && $row['x']>=3 && $row['z']<=2) {
								$nbh='V7';
							}
							else if ($row['y']>=3 && $row['x']>=3 && $row['z']>=3) {
								$nbh='V8';
							}

							#New format date mm-yyyy
							$d=explode("-", $row['period']);
							$m=$d[1];
							$y=$d[0];

							if($sta3 = $db->prepare("INSERT INTO dashboard_neighborhood_agency (dna_neighborhood,dna_agency,dna_date_record,dna_month,dna_year,dna_revenue,dna_routes_sold) VALUES (?,?,?,?,?,?,?)"))
							{
								$sta3->bind_param('sisiidi',$nbh,$row['agency'],$today,$m,$y,$row['revenue'],$row['routes']);
							
								if(!$sta3->execute())
				                {
				                    $error++;
				     // 				print_r("#----------------");
									// print_r($row);
									// print_r($nbh);
				     // 				print_r("::0::");
				                }
				                else
				                {
				                    $succes++;
				     //                print_r("#----------------");
									// print_r($row);
									// print_r($nbh);
				     //                print_r("::1::");
				                }
								$sta3->close();
							}

							$allRows++;
						}
					}
				}
				else
				{
					echo "0";
				}

			
				//echo $allRows.".".$succes.".".$error;

				if($allRows==$succes)
				{
					echo "1";
				}
				else if ($allRows==$error) 
				{
					echo "004";
					//echo "004::".$allRows."::".$succes."::".$error;
				}
				else
				{
					echo "003";
				}
			}
			else if ($allRows==$error) 
			{
				echo "002";
				//echo "002::".$allRows."::".$succes."::".$error;
			}
			else
			{
				echo "001";
			}
		}
	}

?>