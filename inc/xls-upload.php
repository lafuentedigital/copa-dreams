<?php

  include("db.php");

  ini_set('max_execution_time', '-1');
  ini_set('memory_limit', '-1');

  require('classes/session.class.php');
  require('classes/login.class.php');
  require('functions.php');

  $session = new session();
  $session->start_session('_s', false, $db);

  $table=false;
  $tableReport=false;
  $sheet=false;
  $rowError=0;
  $rowSucces=0;
  $totalRows=0;
  $error=false;

  if(isset($_FILES["file"]["name"]))
  {
    $allowedExts = array("xls", "xlsx");
    @$temp = explode(".", $_FILES["file"]["name"]);
    $extension = end($temp);
    $date = date ("d-m-y-si-z");
    $newName=$date."-".rand(10,99).".".$extension;
    $filePath="../uploads/".$newName;

    if((($_FILES["file"]["type"] == "application/vnd.ms-excel") || ($_FILES["file"]["type"] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) || in_array($extension, $allowedExts))
    {
      if ($_FILES["file"]["error"] > 0) 
      {
        echo "004:".$_FILES["file"]["error"];
      }
      else
      {
        if (file_exists($filePath)) 
        {
        echo "003";
        }
        else
        {
          move_uploaded_file($_FILES["file"]["tmp_name"],$filePath);

          if(isset($_SESSION['load-data']))
          {

            if($_SESSION['load-data']=='coms-load')
            {
              $_SESSION['load-data']=$_POST['condition']."-".$_POST['type'];
            }

            //$sheet=openXls($filePath);
            //var_dump($sheet);
            
            switch ($_SESSION['load-data']) 
            {

              case 'agent-load':
              {

                $tableReport="master_report";
                $dateInsert=date('Y-m-d H:i:s');

                if($sta = $db->prepare("INSERT INTO ".$tableReport." (master_report_name_file,master_report_date_added) VALUES (?,?)"))
                {    
                  $sta->bind_param("ss",$newName,$dateInsert);
                  $sta->execute();
                }

                $sta->close();

                $tableReportID=$db->insert_id;

                $table="report_agent";

                $tableFields="report_agent_eq_basic,report_agent_eq_tactic,report_agent_ticket_number,report_agent_white_wings,report_agent_white_plus,report_agent_blue_wings,report_agent_tatic_route,report_agent_basic_route,report_agent_flight_origin,report_agent_flight_destiny,report_agent_date_buy,report_agent_copa_revenue,report_agent_agent,report_agent_flight_number,report_agent_date_flown,report_agent_master_id";

                $sheet=openXls($filePath);

                $db->autocommit(FALSE);

                //ini_set('max_execution_time', '-1');

                if($sta = $db->prepare("INSERT INTO ".$table." (".$tableFields.") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);"))
                {  
                  foreach ($sheet as $row)
                  {
                    if($row['H']=='SI')
                    {
                      $row['H']=1;
                    }
                    else
                    {
                      $row['H']=0;
                    }

                    if($row['I']=='SI')
                    {
                      $row['I']=1;
                    }
                    else
                    {
                      $row['I']=0;
                    }

                    $row['M']=date('Y-m-d', strtotime(str_replace('-', '/', $row['M'])));
                    $row['N']=date('Y-m-d', strtotime(str_replace('-', '/', $row['N'])));
                    $row['O']=(float)str_replace(' ', '', str_replace('$', '', $row['O']));

                    $sta->bind_param("ddsdiiiisssdsssi",$row['B'],$row['C'],$row['D'],$row['E'],$row['F'],$row['G'],$row['I'],$row['H'],$row['K'],$row['L'],$row['M'],$row['O'],$row['A'],$row['J'],$row['N'],$tableReportID);
                  
                    if(!$sta->execute())
                    {
                      $rowError++;
                    }
                    else
                    {
                      $rowSucces++;

                      $sqlVar="SELECT @wwa:=agent_white_wings_available, @wwt:=agent_white_wings_total, @bwa:=agent_blue_wings_available, @bwt:=agent_blue_wings_total FROM agent WHERE agent_id='".$row['A']."';";

                      $db->query($sqlVar);

                      $ww=(float)$row['E']+(float)$row['F'];
                      $bw=(float)$row['G'];

                      if($staWings = $db->prepare("UPDATE agent SET agent_white_wings_available = (SELECT SUM(@wwa + ?)), agent_white_wings_total = (SELECT SUM(@wwt + ?)), agent_blue_wings_available = (SELECT SUM(@bwa + ?)), agent_blue_wings_total = (SELECT SUM(@bwt + ?)) WHERE agent_id=?;"))
                      {  
                        $staWings->bind_param("dddds",$ww,$ww,$bw,$bw,$row['A']);
                        $staWings->execute();
                        $staWings->reset();
                      }

                    }

                    $totalRows++;
                  }

                  $sta->close();
                }
               
                $db->commit();
                $db->autocommit(TRUE);

                $totalRows=$totalRows-1;

                if($sta = $db->prepare("UPDATE ".$tableReport." SET master_report_total_row=?,master_report_success_row=?,master_report_bad_row=? WHERE master_report_id=?"))
                {     
                  $sta->bind_param("iiii",$totalRows,$rowSucces,$rowError,$tableReportID);
                  $sta->execute();
                  $sta->close();
                }

                if($rowError>0)
                {
                  echo $rowSucces."-".$rowError."-".$totalRows;
                  //echo "2";
                  //echo "ROWS: $totalRows SUCCES: $rowSucces ERROR: $rowError";
                }
                else
                {
                  echo $rowSucces."-".$rowError."-".$totalRows;
                  //echo "1";
                  //echo "ROWS: $totalRows SUCCES: $rowSucces ERROR: $rowError";
                }

                break;
              } // Fin agent-load
              
              case 'agency-load':
              {
                $tableReport="master_report";
                $dateInsert=date('Y-m-d H:i:s');

                if($sta = $db->prepare("INSERT INTO ".$tableReport." (master_report_name_file,master_report_date_added) VALUES (?,?)"))
                {    
                  $sta->bind_param("ss",$newName,$dateInsert);
                  $sta->execute();
                  $sta->close();
                }

                $tableReportID=$db->insert_id;

                $table="report_agency";

                $tableFields="report_agency_base,report_agency_agency,report_agency_pin,report_agency_white_wings,report_agency_white_plus,report_master_id";

                $sheet=openXls($filePath);

                $db->autocommit(FALSE);

                if($sta = $db->prepare("INSERT INTO ".$table." (".$tableFields.") VALUES (?,?,?,?,?,?);"))
                {  
                  foreach ($sheet as $row)
                  {

                    $sta->bind_param("sisddi",$row['A'],$row['B'],$row['C'],$row['D'],$row['E'],$tableReportID);
                  
                    if(!$sta->execute())
                    {
                      $rowError++;
                    }
                    else
                    {
                      $rowSucces++;
                      $sqlVar="SELECT @wwa:=agency_white_wings_available, @wwt:=agency_white_wings_total FROM agency WHERE agency_id='".$row['B']."';";

                      $db->query($sqlVar);

                      $ww=(float)$row['D']+(float)$row['E'];

                      if($staWings = $db->prepare("UPDATE agency SET agency_white_wings_available = (SELECT SUM(@wwa + ?)), agency_white_wings_total = (SELECT SUM(@wwt + ?)) WHERE agency_id=?;"))
                      {  
                        $staWings->bind_param("dds",$ww,$ww,$row['B']);
                        $staWings->execute();
                        $staWings->reset();
                      }
                    }

                    $totalRows++;
                  }
                  $sta->close();
                }

                $db->commit();
                $db->autocommit(TRUE);

                $totalRows=$totalRows-1;

                if($sta = $db->prepare("UPDATE ".$tableReport." SET master_report_total_row=?,master_report_success_row=?,master_report_bad_row=? WHERE master_report_id=?"))
                {     
                  $sta->bind_param("iiii",$totalRows,$rowSucces,$rowError,$tableReportID);
                  $sta->execute();
                  $sta->close();
                }

                if($rowError>0)
                {
                  echo $rowSucces."-".$rowError."-".$totalRows;
                  //echo "2";
                  //echo "ROWS: $totalRows SUCCES: $rowSucces ERROR: $rowError";
                }
                else
                {
                  echo $rowSucces."-".$rowError."-".$totalRows;
                  //echo "1";
                  //echo "ROWS: $totalRows SUCCES: $rowSucces ERROR: $rowError";
                }

                break;
              } // Fin agency-load

              case 'agent-commail-b-load':
              {

                $tableReport="master_communications";
                $dateInsert=date('Y-m-d H:i:s');
                $cat=1;

                if($sta = $db->prepare("INSERT INTO ".$tableReport." (master_c_name_file,master_c_date_added,master_c_cat) VALUES (?,?,?)"))
                {    
                  $sta->bind_param("ssi",$newName,$dateInsert,$cat);
                  $sta->execute();
                  $sta->close();
                }

                $tableReportID=$db->insert_id;

                $table="basic_communications_email_agent";

                $tableFields="basic_email_agent_city,basic_email_agent_country,basic_email_agent_campaign,basic_email_agent_date_send,basic_email_agent_agent,basic_email_agent_recived,basic_email_agent_opened,basic_email_agent_master";

                $sheet=openXls($filePath);

                $db->autocommit(FALSE);

                if($sta = $db->prepare("INSERT INTO ".$table." (".$tableFields.") VALUES (?,?,?,?,?,?,?,?);"))
                {  
                  foreach ($sheet as $row)
                  {

                    $row['D']=date('Y-m-d H:i:s', strtotime(str_replace('-', '/', $row['D'])));

                    if($row['F']=='SI')
                    {
                      $row['F']=1;
                    }
                    else
                    {
                      $row['F']=0;
                    }

                    if($row['G']=='SI')
                    {
                      $row['G']=1;
                    }
                    else
                    {
                      $row['G']=0;
                    }

                    $sta->bind_param("iisssiii",$row['A'],$row['B'],$row['C'],$row['D'],$row['E'],$row['F'],$row['G'],$tableReportID);
                  
                    if(!$sta->execute())
                    {
                      $rowError++;
                    }
                    else
                    {
                      $rowSucces++;
                    }

                    $totalRows++;
                  }
                  $sta->close();
                }

                $db->commit();
                $db->autocommit(TRUE);

                $totalRows=$totalRows-1;

                if($sta = $db->prepare("UPDATE ".$tableReport." SET master_c_total_row=?,master_c_success_row=?,master_c_bad_row=? WHERE master_c_id=?"))
                {     
                  $sta->bind_param("iiii",$totalRows,$rowSucces,$rowError,$tableReportID);
                  $sta->execute();
                  $sta->close();
                }

                if($rowError>0)
                {
                  echo $rowSucces."-".$rowError."-".$totalRows;
                  //echo "2";
                  //echo "ROWS: $totalRows SUCCES: $rowSucces ERROR: $rowError";
                }
                else
                {
                  echo $rowSucces."-".$rowError."-".$totalRows;
                  //echo "1";
                  //echo "ROWS: $totalRows SUCCES: $rowSucces ERROR: $rowError";
                }

                break;
              } // Fin agent-commail-b-load

              case 'agency-commail-b-load':
              {

                $tableReport="master_communications";
                $dateInsert=date('Y-m-d H:i:s');
                $cat=1;

                if($sta = $db->prepare("INSERT INTO ".$tableReport." (master_c_name_file,master_c_date_added,master_c_cat) VALUES (?,?,?)"))
                {    
                  $sta->bind_param("ssi",$newName,$dateInsert,$cat);
                  $sta->execute();
                  $sta->close();
                }

                $tableReportID=$db->insert_id;

                $table="basic_communications_email_agency";

                $tableFields="basic_email_agency_city,basic_email_agency_country,basic_email_agency_campaign,basic_email_agency_date_send,basic_email_agency_agency,basic_email_agency_recived,basic_email_agency_opened,basic_email_agency_master";

                $sheet=openXls($filePath);
                $notFilled=0;

                $db->autocommit(FALSE);

                if($sta = $db->prepare("INSERT INTO ".$table." (".$tableFields.") VALUES (?,?,?,?,?,?,?,?);"))
                {  
                  foreach ($sheet as $row)
                  {

                    $row['D']=date('Y-m-d H:i:s', strtotime(str_replace('-', '/', $row['D'])));

                    if($row['F']=='SI')
                    {
                      $row['F']=1;
                    }
                    else
                    {
                      $row['F']=0;
                    }

                    if($row['G']=='SI')
                    {
                      $row['G']=1;
                    }
                    else
                    {
                      $row['G']=0;
                    }

                    $sta->bind_param("iissiiii",$row['A'],$row['B'],$row['C'],$row['D'],$row['E'],$row['F'],$row['G'],$tableReportID);
                  
                    if(!$sta->execute())
                    {
                      $rowError++;
                    }
                    else
                    {
                      $rowSucces++;
                    }

                    $totalRows++;
                  }
                  $sta->close();
                }

                $db->commit();
                $db->autocommit(TRUE);

                $totalRows=$totalRows-1;

                if($sta = $db->prepare("UPDATE ".$tableReport." SET master_c_total_row=?,master_c_success_row=?,master_c_bad_row=? WHERE master_c_id=?"))
                {     
                  $sta->bind_param("iiii",$totalRows,$rowSucces,$rowError,$tableReportID);
                  $sta->execute();
                  $sta->close();
                }
                
                if($rowError>0)
                {
                  echo $rowSucces."-".$rowError."-".$totalRows;
                  //echo "2";
                  //echo "ROWS: $totalRows SUCCES: $rowSucces ERROR: $rowError";
                }
                else
                {
                  echo $rowSucces."-".$rowError."-".$totalRows;
                  //echo "1";
                  //echo "ROWS: $totalRows SUCCES: $rowSucces ERROR: $rowError";
                }

                break;
              } // Fin agency-commail-b-load

              case 'agent-commail-t-load':
              {

                $tableReport="master_communications";
                $dateInsert=date('Y-m-d H:i:s');
                $cat=2;

                if($sta = $db->prepare("INSERT INTO ".$tableReport." (master_c_name_file,master_c_date_added,master_c_cat) VALUES (?,?,?)"))
                {    
                  $sta->bind_param("ssi",$newName,$dateInsert,$cat);
                  $sta->execute();
                  $sta->close();
                }

                $tableReportID=$db->insert_id;

                $table="tactic_communications_email_agent";

                $tableFields="tactic_email_agent_city,tactic_email_agent_country,tactic_email_agent_date_send,tactic_email_agent_agent,tactic_email_agent_recived,tactic_email_agent_opened,tactic_email_agent_basic,tactic_email_agent_tactic,tactic_email_agent_master";

                $sheet=openXls($filePath);

                $db->autocommit(FALSE);

                if($sta = $db->prepare("INSERT INTO ".$table." (".$tableFields.") VALUES (?,?,?,?,?,?,?,?,?);"))
                {  
                  foreach ($sheet as $row)
                  {

                    $row['C']=date('Y-m-d H:i:s', strtotime(str_replace('-', '/', $row['C'])));

                    if($row['F']=='SI')
                    {
                      $row['F']=1;
                    }
                    else
                    {
                      $row['F']=0;
                    }

                    if($row['G']=='SI')
                    {
                      $row['G']=1;
                    }
                    else
                    {
                      $row['G']=0;
                    }

                    if($row['E']=='SI')
                    {
                      $row['E']=1;
                    }
                    else
                    {
                      $row['E']=0;
                    }

                    if($row['H']=='SI')
                    {
                      $row['H']=1;
                    }
                    else
                    {
                      $row['H']=0;
                    }

                    $sta->bind_param("iissiiiii",$row['A'],$row['B'],$row['C'],$row['D'],$row['E'],$row['F'],$row['G'],$row['H'],$tableReportID);
                  
                    if(!$sta->execute())
                    {
                      $rowError++;
                    }
                    else
                    {
                      $rowSucces++;
                    }

                    $totalRows++;
                  }
                  $sta->close();
                }

                $db->commit();
                $db->autocommit(TRUE);

                $totalRows=$totalRows-1;

                if($sta = $db->prepare("UPDATE ".$tableReport." SET master_c_total_row=?,master_c_success_row=?,master_c_bad_row=? WHERE master_c_id=?"))
                {     
                  $sta->bind_param("iiii",$totalRows,$rowSucces,$rowError,$tableReportID);
                  $sta->execute();
                  $sta->close();
                }

                if($rowError>0)
                {
                  echo $rowSucces."-".$rowError."-".$totalRows;
                  //echo "2";
                  //echo "ROWS: $totalRows SUCCES: $rowSucces ERROR: $rowError";
                }
                else
                {
                  echo $rowSucces."-".$rowError."-".$totalRows;
                  //echo "1";
                  //echo "ROWS: $totalRows SUCCES: $rowSucces ERROR: $rowError";
                }

                break;
              } // Fin agent-commail-t-load

              case 'agency-commail-t-load':
              {

                $tableReport="master_communications";
                $dateInsert=date('Y-m-d H:i:s');
                $cat=2;

                if($sta = $db->prepare("INSERT INTO ".$tableReport." (master_c_name_file,master_c_date_added,master_c_cat) VALUES (?,?,?)"))
                {    
                  $sta->bind_param("ssi",$newName,$dateInsert,$cat);
                  $sta->execute();
                  $sta->close();
                }

                $tableReportID=$db->insert_id;

                $table="tactic_communications_email_agency";

                $tableFields="tactic_email_agency_city,tactic_email_agency_country,tactic_email_agency_date_send,tactic_email_agency_agency,tactic_email_agency_recived,tactic_email_agency_opened,tactic_email_agency_basic,tactic_email_agency_tactic,tactic_email_agency_master";

                $sheet=openXls($filePath);

                $db->autocommit(FALSE);

                if($sta = $db->prepare("INSERT INTO ".$table." (".$tableFields.") VALUES (?,?,?,?,?,?,?,?,?);"))
                {  
                  foreach ($sheet as $row)
                  {
                    $row['C']=date('Y-m-d H:i:s', strtotime(str_replace('-', '/', $row['C'])));

                    if($row['F']=='SI')
                    {
                      $row['F']=1;
                    }
                    else
                    {
                      $row['F']=0;
                    }

                    if($row['G']=='SI')
                    {
                      $row['G']=1;
                    }
                    else
                    {
                      $row['G']=0;
                    }

                    if($row['E']=='SI')
                    {
                      $row['E']=1;
                    }
                    else
                    {
                      $row['E']=0;
                    }

                    if($row['H']=='SI')
                    {
                      $row['H']=1;
                    }
                    else
                    {
                      $row['H']=0;
                    }

                    $sta->bind_param("iisiiiiii",$row['A'],$row['B'],$row['C'],$row['D'],$row['E'],$row['F'],$row['G'],$row['H'],$tableReportID);
                  
                    if(!$sta->execute())
                    {
                      $rowError++;
                    }
                    else
                    {
                      $rowSucces++;
                    }
                    $totalRows++;
                  }
                  $sta->close();
                }

                $db->commit();
                $db->autocommit(TRUE);

                $totalRows=$totalRows-1;

                if($sta = $db->prepare("UPDATE ".$tableReport." SET master_c_total_row=?,master_c_success_row=?,master_c_bad_row=? WHERE master_c_id=?"))
                {     
                  $sta->bind_param("iiii",$totalRows,$rowSucces,$rowError,$tableReportID);
                  $sta->execute();
                  $sta->close();
                }

                if($rowError>0)
                {
                  echo $rowSucces."-".$rowError."-".$totalRows;
                  //echo "2";
                  //echo "ROWS: $totalRows SUCCES: $rowSucces ERROR: $rowError";
                }
                else
                {
                  echo $rowSucces."-".$rowError."-".$totalRows;
                  //echo "1";
                  //echo "ROWS: $totalRows SUCCES: $rowSucces ERROR: $rowError";
                }

                break;
              } // Fin agency-commail-t-load

              case 'agent-comsms-t-load':
              {

                $tableReport="master_communications";
                $dateInsert=date('Y-m-d H:i:s');
                $cat=3;

                if($sta = $db->prepare("INSERT INTO ".$tableReport." (master_c_name_file,master_c_date_added,master_c_cat) VALUES (?,?,?)"))
                {    
                  $sta->bind_param("ssi",$newName,$dateInsert,$cat);
                  $sta->execute();
                }

                //$sta->close();

                $tableReportID=$db->insert_id;

                $table="tactic_communications_sms_agent";

                $tableFields="tactic_sms_agent_city,tactic_sms_agent_country,tactic_sms_agent_date_send,tactic_sms_agent_agent,tactic_sms_agent_recived,tactic_sms_agent_clic,tactic_sms_agent_basic,tactic_sms_agent_tactic,tactic_sms_agent_master";

                $sheet=openXls($filePath);
                $notFilled=0;

                $db->autocommit(FALSE);

                if($sta = $db->prepare("INSERT INTO ".$table." (".$tableFields.") VALUES (?,?,?,?,?,?,?,?,?);"))
                {  
                  foreach ($sheet as $row)
                  {

                    $row['C']=date('Y-m-d H:i:s', strtotime(str_replace('-', '/', $row['C'])));
                    if($row['F']=='SI')
                    {
                      $row['F']=1;
                    }
                    else
                    {
                      $row['F']=0;
                    }

                    if($row['G']=='SI')
                    {
                      $row['G']=1;
                    }
                    else
                    {
                      $row['G']=0;
                    }

                    if($row['E']=='SI')
                    {
                      $row['E']=1;
                    }
                    else
                    {
                      $row['E']=0;
                    }

                    if($row['H']=='SI')
                    {
                      $row['H']=1;
                    }
                    else
                    {
                      $row['H']=0;
                    }

                    $sta->bind_param("iissiiiii",$row['A'],$row['B'],$row['C'],$row['D'],$row['E'],$row['F'],$row['G'],$row['H'],$tableReportID);
                  
                    if(!$sta->execute())
                    {
                      $rowError++;
                    }
                    else
                    {
                      $rowSucces++;
                    }

                    $totalRows++;
                  }
                  $sta->close();
                }

                $db->commit();
                $db->autocommit(TRUE);

                $totalRows=$totalRows-1;

                if($sta = $db->prepare("UPDATE ".$tableReport." SET master_c_total_row=?,master_c_success_row=?,master_c_bad_row=? WHERE master_c_id=?"))
                {     
                  $sta->bind_param("iiii",$totalRows,$rowSucces,$rowError,$tableReportID);
                  $sta->execute();
                  $sta->close();
                }

                if($rowError>0)
                {
                  echo $rowSucces."-".$rowError."-".$totalRows;
                  //echo "2";
                  //echo "ROWS: $totalRows SUCCES: $rowSucces ERROR: $rowError";
                }
                else
                {
                  echo $rowSucces."-".$rowError."-".$totalRows;
                  //echo "1";
                  //echo "ROWS: $totalRows SUCCES: $rowSucces ERROR: $rowError";
                }

                break;
              } // Fin agent-comsms-t-load

              case 'agency-comsms-t-load':
              {

                $tableReport="master_communications";
                $dateInsert=date('Y-m-d H:i:s');
                $cat=3;

                if($sta = $db->prepare("INSERT INTO ".$tableReport." (master_c_name_file,master_c_date_added,master_c_cat) VALUES (?,?,?)"))
                {    
                  $sta->bind_param("ssi",$newName,$dateInsert,$cat);
                  $sta->execute();
                }

                //$sta->close();

                $tableReportID=$db->insert_id;

                $table="tactic_communications_sms_agency";

                $tableFields="tactic_sms_agency_city,tactic_sms_agency_country,tactic_sms_agency_date_send,tactic_sms_agency_agency,tactic_sms_agency_recived,tactic_sms_agency_clic,tactic_sms_agency_basic,tactic_sms_agency_tactic,tactic_sms_agency_master";

                $sheet=openXls($filePath);
                $notFilled=0;

                $db->autocommit(FALSE);

                if($sta = $db->prepare("INSERT INTO ".$table." (".$tableFields.") VALUES (?,?,?,?,?,?,?,?,?);"))
                {  
                  foreach ($sheet as $row)
                  {

                    $row['C']=date('Y-m-d H:i:s', strtotime(str_replace('-', '/', $row['C'])));

                    if($row['F']=='SI')
                    {
                      $row['F']=1;
                    }
                    else
                    {
                      $row['F']=0;
                    }

                    if($row['G']=='SI')
                    {
                      $row['G']=1;
                    }
                    else
                    {
                      $row['G']=0;
                    }

                    if($row['E']=='SI')
                    {
                      $row['E']=1;
                    }
                    else
                    {
                      $row['E']=0;
                    }

                    if($row['H']=='SI')
                    {
                      $row['H']=1;
                    }
                    else
                    {
                      $row['H']=0;
                    }

                    $sta->bind_param("iisiiiiii",$row['A'],$row['B'],$row['C'],$row['D'],$row['E'],$row['F'],$row['G'],$row['H'],$tableReportID);
                  
                    if(!$sta->execute())
                    {
                      $rowError++;
                    }
                    else
                    {
                      $rowSucces++;
                    }

                    $totalRows++;
                  }
                  $sta->close();
                }


                $db->commit();
                $db->autocommit(TRUE);

                $totalRows=$totalRows-1;

                if($sta = $db->prepare("UPDATE ".$tableReport." SET master_c_total_row=?,master_c_success_row=?,master_c_bad_row=? WHERE master_c_id=?"))
                {     
                  $sta->bind_param("iiii",$totalRows,$rowSucces,$rowError,$tableReportID);
                  $sta->execute();

                }

                if($rowError>0)
                {
                  echo $rowSucces."-".$rowError."-".$totalRows;
                  //echo "2";
                  //echo "ROWS: $totalRows SUCCES: $rowSucces ERROR: $rowError";
                }
                else
                {
                  echo $rowSucces."-".$rowError."-".$totalRows;
                  //echo "1";
                  //echo "ROWS: $totalRows SUCCES: $rowSucces ERROR: $rowError";
                }

                break;
              } // Fin agency-comsms-t-load

              default:
              {
                break;
              }
                
            } // Fin switch
            
          }
        } // Fin if load-data
      } // Fin if file error
    } 
    else 
    {
      echo "002";
    } // Fin check extension
  } // Fin check file.
  else
  {
    echo "0";
  }

?>