<?php
	include("db.php");

	require('classes/session.class.php');
	$session = new session();
	$session->start_session('_s', false, $db);

	if(isset($_POST['filtersComsTacTab1']) && $_POST['filtersComsTacTab1'])
	{

		$join0="";
		$join1="";
		$conditional0="";
		$conditional1="";
		$conditional2="";
		$whr=false;
		$and=false;
		$per=false;

		if(isset($_POST['byNeighborhood']))
		{
			if($_POST['byNeighborhood']!='0')
			{
				$v=$_POST['byNeighborhood'];

				$join0.="INNER JOIN dashboard_neighborhood_agent ON tactic_email_agent_agent=dn_agent ";
				$conditional0.="WHERE (dn_neighborhood='".$v."') ";
				$join1.="INNER JOIN dashboard_neighborhood_agency ON tactic_email_agency_agency=dna_agency ";
				$conditional1.="WHERE (dna_neighborhood='".$v."') ";
				$and=true;
			}
		}

		if(isset($_POST['byPeriod']))
		{
			$start=explode('-', $_POST['byPeriod']);

			if($start[0]>0 && $start[1]>1999)
			{

				if($and){ $conditional0.=" AND "; }else{ $conditional0=" WHERE "; }
				if($and){ $conditional1.=" AND "; }else{ $conditional1=" WHERE "; }
				$conditional0.=" (tactic_email_agent_date_send>='".$start[1]."-".$start[0]."-01' AND tactic_email_agent_date_send<='".$start[1]."-".$start[0]."-30') ";
				$conditional1.=" (tactic_email_agency_date_send>='".$start[1]."-".$start[0]."-01' AND tactic_email_agency_date_send<='".$start[1]."-".$start[0]."-30')";
				$and=true;
			}

		}

		if(isset($_POST['byLocation']))
		{
			if ($_POST['byLocation']!=0) 
			{

				$city=$_POST['byLocation'];

				if($and){ $conditional0.=" AND "; }else{ $conditional0=" WHERE "; }
				if($and){ $conditional1.=" AND "; }else{ $conditional1=" WHERE "; }

				$conditional0.=" (tactic_email_agent_city='".$city."')";
				$conditional1.=" (tactic_email_agency_city='".$city."')";
			}
		}

		if(isset($_POST['byRoutes']))
		{
			$route=explode("-", $_POST['byRoutes']);
			$conditional2.=" AND (report_agent_flight_origin='".$route[0]."' AND report_agent_flight_destiny='".$route[1]."')";
			//var_dump($_POST['byRoutes']);
		}



		$select="
        (
                         SELECT
                            tactic as type,
                            SUM(recieved) as total_recieved,
                            ((SUM(opened)*100)/SUM(recieved)) as percent_opened,
                            SUM(tickets) as total_tickets,
                            SUM(revenue) as total_revenue

                          FROM
                          (
                            (SELECT
                                tactic_email_agent_tactic as tactic,
                                tactic_email_agent_basic as basic,
                                tactic_email_agent_recived as recieved, 
                                tactic_email_agent_opened as opened, 
                                (
                                  SELECT COUNT( DISTINCT report_agent_flight_origin, report_agent_flight_destiny ) AS cont
                                  FROM report_agent
                                  WHERE report_agent_agent = tactic_email_agent_agent ".$conditional2."
                                ) as tickets,
                                (
                                  SELECT SUM(report_agent_copa_revenue) AS sm
                                  FROM report_agent
                                  WHERE report_agent_agent =  tactic_email_agent_agent ".$conditional2."
                                ) as revenue,
								tactic_email_agent_city
                              FROM
                                tactic_communications_email_agent
                              ".$join0.$conditional0."
                              )UNION(
                              SELECT
                            tactic_email_agency_tactic as tactic,
                            tactic_email_agency_basic as basic,
                            tactic_email_agency_recived as recieved, 
                            tactic_email_agency_opened as opened, 
                            (
                              SELECT COUNT( DISTINCT report_agent_flight_origin, report_agent_flight_destiny ) AS cont
                              FROM report_agent
                              INNER JOIN agent
                              ON report_agent_agent=agent_id
                              WHERE agent_agency = tactic_email_agency_agency ".$conditional2."
                            ) as tickets,
                            (
                              SELECT SUM(report_agent_copa_revenue) AS sm
                              FROM report_agent
                              INNER JOIN agent
                              ON report_agent_agent=agent_id
                              WHERE agent_agency = tactic_email_agency_agency ".$conditional2."
                            ) as revenue,
							tactic_email_agency_city
                          FROM
                            tactic_communications_email_agency
                          ".$join1.$conditional1."
                              )
                          ) drv
                          WHERE
                            tactic='1'
                        )UNION(
                         SELECT
                            tactic as type,
                            SUM(recieved) as total_recieved,
                            ((SUM(opened)*100)/SUM(recieved)) as percent_opened,
                            SUM(tickets) as total_tickets,
                            SUM(revenue) as total_revenue

                          FROM
                          (
                            (SELECT
                                tactic_email_agent_tactic as tactic,
                                tactic_email_agent_basic as basic,
                                tactic_email_agent_recived as recieved, 
                                tactic_email_agent_opened as opened, 
                                (
                                  SELECT COUNT( DISTINCT report_agent_flight_origin, report_agent_flight_destiny ) AS cont
                                  FROM report_agent
                                  WHERE report_agent_agent = tactic_email_agent_agent ".$conditional2."
                                ) as tickets,
                                (
                                  SELECT SUM(report_agent_copa_revenue) AS sm
                                  FROM report_agent
                                  WHERE report_agent_agent =  tactic_email_agent_agent ".$conditional2."
                                ) as revenue,
								tactic_email_agent_city
                              FROM
                                tactic_communications_email_agent
                              ".$join0.$conditional0."
                              )UNION(
                              SELECT
                            tactic_email_agency_tactic as tactic,
                            tactic_email_agency_basic as basic,
                            tactic_email_agency_recived as recieved, 
                            tactic_email_agency_opened as opened, 
                            (
                              SELECT COUNT( DISTINCT report_agent_flight_origin, report_agent_flight_destiny ) AS cont
                              FROM report_agent
                              INNER JOIN agent
                              ON report_agent_agent=agent_id
                              WHERE agent_agency = tactic_email_agency_agency ".$conditional2."
                            ) as tickets,
                            (
                              SELECT SUM(report_agent_copa_revenue) AS sm
                              FROM report_agent
                              INNER JOIN agent
                              ON report_agent_agent=agent_id
                              WHERE agent_agency = tactic_email_agency_agency ".$conditional2."
                            ) as revenue,
							tactic_email_agency_city
                          FROM
                            tactic_communications_email_agency
                          ".$join1.$conditional1."
                              )
                          ) drv

                          WHERE
                           tactic='0'
                        )
        ";

		$query=$select;
		$_SESSION['query_coms_tac_0']=$query;
		//echo $query;
		echo "1";
	}

	if(isset($_POST['filtersComsTacTab2']) && $_POST['filtersComsTacTab2'])
	{

		$select="";
		$conditional="";
		$conditional0="";
		$and=false;

		if(isset($_POST['byRoutes']))
		{

			$rou=explode("-", $_POST['byRoutes']);

			if(sizeof($rou)>1)
			{

				$conditional0=" AND (report_agent_flight_origin='".$rou[0]."' AND report_agent_flight_destiny='".$rou[1]."')";
				//print_r($rou);
			}
			
		}

		if(isset($_POST['byPercent']))
		{
			switch ($_POST['byPercent']) {
				case 0:
					# code...
					break;
				case 1:
					if($and){ $conditional.=" AND "; }else{ $conditional=" HAVING "; }
					$conditional.="(opening>=0 AND opening<=60)";
					$and=true;
					break;
				case 2:
					if($and){ $conditional.=" AND "; }else{ $conditional=" HAVING "; }
					$conditional.="(opening>=61 AND opening<=70)";
					$and=true;
					break;
				case 3:
					if($and){ $conditional.=" AND "; }else{ $conditional=" HAVING "; }
					$conditional.="(opening>=71 AND opening<=80)";
					$and=true;
					break;
				case 4:
					if($and){ $conditional.=" AND "; }else{ $conditional=" HAVING "; }
					$conditional.="(opening>=81 AND opening<=90)";
					$and=true;
					break;
				case 5:
					if($and){ $conditional.=" AND "; }else{ $conditional=" HAVING "; }
					$conditional.="(opening>=91 AND opening<=100)";
					$and=true;
					break;
				default:
					# code...
					break;
			}
		}

		
		if(isset($_POST['byLocation']))
		{
			if ($_POST['byLocation']!=0) 
			{

				$city=$_POST['byLocation'];

				switch ($_POST['byCondition']) {
					case 1:
						if($and){ $conditional.=" AND "; }else{ $conditional=" HAVING "; }
						$conditional.=" (cityCod='".$city."')";
						$and=true;
						break;
					
					case 2:
						if($and){ $conditional.=" AND "; }else{ $conditional=" HAVING "; }
						$conditional.=" (cityCod='".$city."')";
						$and=true;
						break;

					default:
						if($and){ $conditional.=" AND "; }else{ $conditional=" HAVING "; }
						$conditional.=" (cityCod='".$city."')";
						$and=true;
						break;
				}
			}
		}

		if(isset($_POST['byNeighborhood']))
		{
			if($_POST['byNeighborhood']!='0')
			{
				$v=$_POST['byNeighborhood'];
				if($and){ $conditional.=" AND "; }else{ $conditional=" HAVING "; }
				$conditional.="(loyalty='".$v."')";
				$and=true;
			}
		}

		if(isset($_POST['byCondition']))
		{
			switch ($_POST['byCondition']) {
				case 1:
					$select.="
					  SELECT
                        code,
                        opening,
                        loyalty,
                        country,
                        city,
                        cityCod,
                        tickets,
                        revenue
                      FROM
                            (
                              SELECT
                                tactic_email_agent_agent as code,
                                tactic_email_agent_id as id,
                                ((tactic_email_agent_opened*100)/tactic_email_agent_recived) as opening,
                                dn_neighborhood as loyalty,
                                country_name as country,
                                city_name as city,
                                tactic_email_agent_city as cityCod,
                                (
                                  SELECT COUNT( DISTINCT report_agent_flight_origin, report_agent_flight_destiny ) AS cont
                                  FROM report_agent
                                  WHERE report_agent_agent = tactic_email_agent_agent ".$conditional0."
                                ) as tickets,
                                (
                                  SELECT SUM(report_agent_copa_revenue) AS sm
                                  FROM report_agent
                                  WHERE report_agent_agent =  tactic_email_agent_agent ".$conditional0."
                                ) as revenue
                              FROM
                                tactic_communications_email_agent
                              JOIN
                                dashboard_neighborhood_agent
                              ON
                                dn_agent=tactic_email_agent_agent
                              JOIN
                                country
                              ON
                                country_id=tactic_email_agent_country
                              JOIN
                                city
                              ON
                                city_id=tactic_email_agent_city
                              GROUP BY
                                code, id
                            ) as drv2
					";
					break;
				
				case 2:
					$select.="
					  SELECT
                        code,
                        opening,
                        loyalty,
                        country,
                        city,
                        cityCod,
                        tickets,
                        revenue
                      FROM
                            (
                              SELECT
                                tactic_email_agency_agency as code,
                                tactic_email_agency_id as id,
                                ((tactic_email_agency_opened*100)/tactic_email_agency_recived) as opening,
                                dna_neighborhood as loyalty,
                                country_name as country,
                                city_name as city,
                                tactic_email_agency_city as cityCod,
                                (
	                              SELECT COUNT( DISTINCT report_agent_flight_origin, report_agent_flight_destiny ) AS cont
	                              FROM report_agent
	                              INNER JOIN agent
	                              ON report_agent_agent=agent_id
	                              WHERE agent_agency = tactic_email_agency_agency ".$conditional0."
	                            ) as tickets,
	                            (
	                              SELECT SUM(report_agent_copa_revenue) AS sm
	                              FROM report_agent
	                              INNER JOIN agent
	                              ON report_agent_agent=agent_id
	                              WHERE agent_agency = tactic_email_agency_agency ".$conditional0."
	                            ) as revenue
                              FROM
                                tactic_communications_email_agency
                              JOIN
                                dashboard_neighborhood_agency
                              ON
                                dna_agency=tactic_email_agency_agency
                              JOIN
                                country
                              ON
                                country_id=tactic_email_agency_country
                              JOIN
                                city
                              ON
                                city_id=tactic_email_agency_city
                              GROUP BY
                                code, id
                            ) as drv2
					";
					break;

				default:
					$select.="
						SELECT
                        code,
                        opening,
                        loyalty,
                        country,
                        city,
                        cityCod,
                        tickets,
                        revenue
                      FROM
                            (
                              SELECT
                                tactic_email_agent_agent as code,
                                tactic_email_agent_id as id,
                                ((tactic_email_agent_opened*100)/tactic_email_agent_recived) as opening,
                                dn_neighborhood as loyalty,
                                country_name as country,
                                city_name as city,
                                tactic_email_agent_city as cityCod,
                                (
                                  SELECT COUNT( DISTINCT report_agent_flight_origin, report_agent_flight_destiny ) AS cont
                                  FROM report_agent
                                  WHERE report_agent_agent = tactic_email_agent_agent ".$conditional0."
                                ) as tickets,
                                (
                                  SELECT SUM(report_agent_copa_revenue) AS sm
                                  FROM report_agent
                                  WHERE report_agent_agent =  tactic_email_agent_agent ".$conditional0."
                                ) as revenue
                              FROM
                                tactic_communications_email_agent
                              JOIN
                                dashboard_neighborhood_agent
                              ON
                                dn_agent=tactic_email_agent_agent
                              JOIN
                                country
                              ON
                                country_id=tactic_email_agent_country
                              JOIN
                                city
                              ON
                                city_id=tactic_email_agent_city
                              GROUP BY
                                code, id
                            ) as drv2
					";
					break;
			}
		}

		$query=$select.$conditional;
		$_SESSION['query_coms_tac_1']=$query;
		//echo $query;
		echo "1";
	}

	if(isset($_POST['cleanTacTab1']) && $_POST['cleanTacTab1'])
	{
		unset($_SESSION['query_coms_tac_0']);
		if(!isset($_SESSION['query_coms_tac_0']))
		{
			echo "1";
		}
	}

	if(isset($_POST['cleanTacTab2']) && $_POST['cleanTacTab2'])
	{
		unset($_SESSION['query_coms_tac_1']);
		if(!isset($_SESSION['query_coms_tac_1']))
		{
			echo "1";
		}
	}
?>