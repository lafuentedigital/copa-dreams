<?php 
	include("db.php");
	require('classes/session.class.php');

	include('../views/vfunctions.php');
    
	$session = new session();
	$session->start_session('_s', false, $db);
	
	set_include_path(get_include_path() . PATH_SEPARATOR . 'xls/Classes/');
	include 'PHPExcel.php';
	$kpi = $_SESSION['kpi_array'];
	$objPHPExcel = new PHPExcel();

	$objPHPExcel->getProperties()->setCreator("Copa");
	$objPHPExcel->getProperties()->setLastModifiedBy("LFD");
	$objPHPExcel->getProperties()->setTitle("Reporte");
	$objPHPExcel->getProperties()->setSubject("Reporte de Copa");
	$objPHPExcel->getProperties()->setDescription("Reporte de Copa KPI");

	$reportName = "REPORTE KPI"; 

	/////////////////////////////REPORTE1/////////////////////////////
	
	$colName1 = array('KPIs','Grupo (RM)','Grupo (CTRL)','Cumplimiento KPIs');
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:D1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $reportName); 
	$lista= array('A','B','C','D');

	for($u=0;$u<sizeof($lista);$u++){
    	$objPHPExcel->getActiveSheet()->getColumnDimension($lista[$u])->setAutoSize(true);
    	$objPHPExcel->getActiveSheet()->getStyle($lista[$u])->getAlignment()
    				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($lista[$u].'2',  $colName1[$u]);    	
    }

    /////////////////////////////INFDEBD/////////////////////////////
    	
	$kpi['compromiso']['grm'] = number_format((float)round($kpi['compromiso']['grm'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
	$kpi['compromiso']['gctrl'] = number_format((float)round($kpi['compromiso']['gctrl'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
	$kpi['conducta']['grm'] = number_format((float)round($kpi['conducta']['grm'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
	$kpi['conducta']['gctrl'] = number_format((float)round($kpi['conducta']['gctrl'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
	$kpi['involucramiento']['grm'] = number_format((float)round($kpi['involucramiento']['grm'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
	$kpi['involucramiento']['gctrl'] = number_format((float)round($kpi['involucramiento']['gctrl'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');	
	$kpi['compromiso']['cumplimiento'] = str_replace('#', '', $kpi['compromiso']['cumplimiento']);
	$kpi['conducta']['cumplimiento'] = str_replace('#', '', $kpi['conducta']['cumplimiento']);
	$kpi['involucramiento']['cumplimiento'] = str_replace('#', '', $kpi['involucramiento']['cumplimiento']);

	$objPHPExcel->setActiveSheetIndex(0)
    	->setCellValue('A3', 'Compromiso')
    	->setCellValue('B3', $kpi['compromiso']['grm'])
    	->setCellValue('C3', $kpi['compromiso']['gctrl'])
    	->setCellValue('A4', 'Conducta Deseada')
    	->setCellValue('B4', $kpi['conducta']['grm'])
    	->setCellValue('C4', $kpi['conducta']['gctrl'])
    	->setCellValue('A5', 'Involucramiento')
    	->setCellValue('B5', $kpi['involucramiento']['grm'])
    	->setCellValue('C5', $kpi['involucramiento']['gctrl']);  
    	
	cellColor('D3',$kpi['compromiso']['cumplimiento']);
	cellColor('D4',$kpi['conducta']['cumplimiento']);
	cellColor('D5',$kpi['involucramiento']['cumplimiento']);

    /////////////////////////////REPORTE2/////////////////////////////
    
    $colName2 = array('KPIs','Meta','Real','% Diferencia','Cumplimiento KPIs');
	$lista= array('A','B','C','D','E');

	for($u=0;$u<sizeof($lista);$u++){
    	$objPHPExcel->getActiveSheet()->getColumnDimension($lista[$u])->setAutoSize(true);
    	$objPHPExcel->getActiveSheet()->getStyle($lista[$u])->getAlignment()
    				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($lista[$u].'8',  $colName2[$u]);    	
    }

    /////////////////////////////INFDEBD/////////////////////////////

    $kpi['rate']['real'] = number_format((float)round($kpi['rate']['real'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
    $kpi['rate']['dif'] = number_format((float)round($kpi['rate']['dif'], 2, PHP_ROUND_HALF_ODD), 2, '.', ',');
    $kpi['rate']['cumplimiento'] = str_replace('#', '', $kpi['rate']['cumplimiento']);

    $objPHPExcel->setActiveSheetIndex(0)
    	->setCellValue('A9', 'Rata de Pago')
    	->setCellValue('B9', '6%')
    	->setCellValue('C9', $kpi['rate']['real'])
    	->setCellValue('D9', $kpi['rate']['dif']);

   	cellColor('E9',$kpi['rate']['cumplimiento']);

    /////////////////////////////ESTILOS/////////////////////////////
    
    function cellColor($cells,$color){
        global $objPHPExcel;
        $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()
        ->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array('rgb' => $color)
        ));
    }

    $esReportName = array(
	    'font' => array(
	        'name'      => 'Times New Roman',
	        'bold'      => true,
	        'italic'    => false,
	        'strike'    => false,
	        'size'      => 12,
	        'color'     => array('rgb' => 'FFFFFF')
	    ),
	    'fill' => array(
	        'type'  => PHPExcel_Style_Fill::FILL_SOLID,
	        'color' => array('rgb' => '006699')
	    ),
	    'borders' => array(
	        'allborders' => array(
	            'style' => PHPExcel_Style_Border::BORDER_NONE
	        )
	    ),
	    'alignment' => array(
	        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	        'rotation' => 0,
	        'wrap' => TRUE
	    )
	);

    $esColName = array(
	    'font' => array(
	        'name'  => 'Times new Roman',
	        'bold'  => false,
	        'size'  => 12,
	        'color' => array(
	            'rgb' => '333333'
	        )
	    ),
	    'fill' => array(
	        'type'  => PHPExcel_Style_Fill::FILL_SOLID,
	        'color' => array('rgb' => 'cdcdcd')
	    ),
	    'borders' => array(
	        'allborders' => array(
	            'style' => PHPExcel_Style_Border::BORDER_NONE
	        )
	    ),
	    
	);

	$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($esReportName);
	$objPHPExcel->getActiveSheet()->getStyle('A2:D2')->applyFromArray($esColName);
	$objPHPExcel->getActiveSheet()->getStyle('A8:E8')->applyFromArray($esColName);

	/////////////////////////////CREACIONARCHIVO/////////////////////////////
    $objPHPExcel->getActiveSheet()->setTitle('Reporte KPIs');	 
	$objPHPExcel->setActiveSheetIndex(0);	 

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="reporteKPI.xls"');
	header('Cache-Control: max-age=0');

	require_once 'PHPExcel/IOFactory.php';
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
	$objWriter->save('php://output',__FILE__);
	exit;

?>