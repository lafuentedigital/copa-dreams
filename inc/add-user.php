<?php
	include("db.php");

	require('classes/session.class.php');
	$session = new session();
	$session->start_session('_s', false, $db);

	//print_r($_POST);
	if (isset($_POST['edit'])) {

		if (isset($_POST['email'])) {

			if($stc = $db->prepare("SELECT COUNT(*) FROM dashboard_users WHERE du_mail=?"))
			{
				$stc->bind_param('s',$_POST['email']);
				$stc->execute();
				$stc->bind_result($chk);
				if($chk>0)
				{
					exit("0200");
				}
				$stc->close();
			}
		}

		if (isset($_POST['pass1']) && isset($_POST['pass2'])) {

			if($_POST['pass1']!=$_POST['pass2'])
			{
				exit("0300");
			}
			else
			{
				$pass=true;
			}
		}

		if (isset($_POST['capabilities']) && !empty($_POST['capabilities'])) {

			if($_POST['capabilities']=="write")
			{
				$capabilities="read,".$_POST['capabilities'];
			}
			else
			{
				$capabilities="read";
			}
		}
		else
		{
			$capabilities="read";
		}



		if($pass)
		{
			$hpass=hash('sha512', $_POST['pass1']);
			$hkey=hash('sha512', $_POST['user'].$_POST['pass1'].$_POST['email'].$_POST['id']);

			if($stb = $db->prepare("UPDATE dashboard_users SET du_mail=?,du_pass=?,du_key=?,du_capabilities=? WHERE du_id=?"))
			{
				$stb->bind_param('ssssi',$_POST['email'],$hpass,$hkey,$capabilities,$_POST['id']);
				$stb->execute();
				$stb->close();
				echo "5";
			}
		}
		else
		{
			if($stb = $db->prepare("UPDATE dashboard_users SET du_mail=?,du_capabilities=? WHERE du_id=?"))
			{
				$stb->bind_param('ssi',$_POST['email'],$capabilities,$_POST['id']);
				$stb->execute();
				$stb->close();
				echo "1";
			}
		}

	}
	else
	{
		if (isset($_POST['user'])) {


			if($stb = $db->prepare("SELECT COUNT(*) FROM dashboard_users WHERE du_user=?"))
			{
				$stb->bind_param('s',$_POST['user']);
				$stb->execute();
				$stb->bind_result($chk1);
				if($chk1>0)
				{
					exit("0100");
				}
				$stb->close();
			}
		}


		if (isset($_POST['email'])) {

			if($stc = $db->prepare("SELECT COUNT(*) FROM dashboard_users WHERE du_mail=?"))
			{
				$stc->bind_param('s',$_POST['email']);
				$stc->execute();
				$stc->bind_result($chk2);
				if($chk2>0)
				{
					exit("0200");
				}
				$stc->close();
			}
		}

		if (isset($_POST['pass1']) && isset($_POST['pass2'])) {

			if($_POST['pass1']!=$_POST['pass2'])
			{
				exit("0300");
			}
			else
			{
				$chk3=0;
			}
		}

		if (isset($_POST['capabilities']) && !empty($_POST['capabilities'])) {

			if($_POST['capabilities']=="write")
			{
				$capabilities="read,".$_POST['capabilities'];
			}
			else
			{
				$capabilities="read";
			}
		}
		else
		{
			$capabilities="read";
		}

		$hpass=hash('sha512', $_POST['pass1']);
			
		if($sta = $db->prepare("INSERT INTO dashboard_users (du_user,du_mail,du_pass,du_capabilities) VALUES (?,?,?,?)"))
		{
			$sta->bind_param('ssss',$_POST['user'],$_POST['email'],$hpass,$capabilities);
			$sta->execute();
			$id = $sta->insert_id;

			$hkey=hash('sha512', $_POST['user'].$_POST['pass1'].$_POST['email'].$id);

			if($id > 0)
			{
				if($stb = $db->prepare("UPDATE dashboard_users SET du_key=? WHERE du_id=?"))
				{
					$stb->bind_param('si',$hkey,$id);
					$stb->execute();
					$stb->close();
					echo "1";
				}
			}
			else
			{
				echo "2";
			}

			$sta->close();
		}
		else
		{
			echo "2";
		}
	}

?>