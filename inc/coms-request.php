<?php
	include("db.php");

	require('classes/session.class.php');
	$session = new session();
	$session->start_session('_s', false, $db);

	if(isset($_POST['filtersComsTab1']) && $_POST['filtersComsTab1'])
	{

		$join0="";
		$join1="";
		$conditional0="";
		$conditional1="";
		$whr=false;
		$and=false;
		$per=false;

		if(isset($_POST['byNeighborhood']))
		{
			if($_POST['byNeighborhood']!='0')
			{
				$v=$_POST['byNeighborhood'];

				$join0.="INNER JOIN dashboard_neighborhood_agent ON agent_id=dn_agent ";
				$conditional0.="WHERE (dn_neighborhood='".$v."') ";
				$join1.="INNER JOIN dashboard_neighborhood_agency ON agent_agency=dna_agency ";
				$conditional1.="WHERE (dna_neighborhood='".$v."') ";
				$and=true;
			}
		}

		if(isset($_POST['byPeriod']))
		{
			$start=explode('-', $_POST['byPeriod']);

			if($start[0]>0 && $start[1]>1999)
			{

				if($and){ $conditional0.=" AND "; }else{ $conditional0=" WHERE "; }
				if($and){ $conditional1.=" AND "; }else{ $conditional1=" WHERE "; }
				$conditional0.=" (basic_email_agent_date_send>='".$start[1]."-".$start[0]."-01' AND basic_email_agent_date_send<='".$start[1]."-".$start[0]."-30') ";
				$conditional1.=" (basic_email_agency_date_send>='".$start[1]."-".$start[0]."-01' AND basic_email_agency_date_send<='".$start[1]."-".$start[0]."-30')";
				$and=true;
			}

		}

		if(isset($_POST['byLocation']))
		{
			if ($_POST['byLocation']!=0) 
			{

				$city=$_POST['byLocation'];

				if($and){ $conditional0.=" AND "; }else{ $conditional0=" WHERE "; }
				if($and){ $conditional1.=" AND "; }else{ $conditional1=" WHERE "; }

				$conditional0.=" (basic_email_agent_city='".$city."')";
				$conditional1.=" (basic_email_agency_city='".$city."')";
			}
		}



		$select="
        SELECT
		        campaign, SUM(recived) as total_recived, ((SUM(opened)*100)/SUM(recived)) as percent_opened
		FROM
		        (
		                (SELECT 
		                        agent_id,agent_agency,basic_email_agent_campaign as campaign, basic_email_agent_recived as recived, basic_email_agent_opened as opened
		                FROM
		                        agent
		                INNER JOIN
		                        basic_communications_email_agent
		                ON
		                        agent_id=basic_email_agent_agent
		                ".$join0.$conditional0."
		                )
		                UNION
		                (SELECT 
		                        agent_id,agent_agency,basic_email_agency_campaign as campaign, basic_email_agency_recived as recived, basic_email_agency_opened as opened
		                FROM
		                        agent
		                INNER JOIN
		                        basic_communications_email_agency
		                ON
		                        agent_agency=basic_email_agency_agency
		                ".$join1.$conditional1."
		                GROUP BY
		                        agent_agency
		                )
		        ) as drv1
		GROUP BY
		        campaign
        ";

		$query=$select;
		$_SESSION['query_coms']=$query;
		//echo $query;
		echo "1";
	}

	if(isset($_POST['filtersComsTab2']) && $_POST['filtersComsTab2'])
	{

		$select="
		SELECT
			code,
			opening,
			loyalty,
			country,
			city
		";

		$conditional="";
		$and=false;

		if(isset($_POST['byCondition']))
		{
			switch ($_POST['byCondition']) {
				case 1:
					$select.="
						FROM
                            (
                              SELECT
                                      basic_email_agent_agent as code,
                                      basic_email_agent_campaign as campaign,
                                      ((basic_email_agent_opened*100)/basic_email_agent_recived) as opening,
                                      dn_neighborhood as loyalty,
                                      country_name as country,
                                      city_name as city,
                                      basic_email_agent_city
                              FROM
                                      basic_communications_email_agent
                              JOIN
                                      dashboard_neighborhood_agent
                              ON
                                      dn_agent=basic_email_agent_agent
                              JOIN
                                      country
                              ON
                                      country_id=basic_email_agent_country
                              JOIN
                                      city
                              ON
                                      city_id=basic_email_agent_city
                              GROUP BY
                                code, campaign
                            ) as drv2
					";
					break;
				
				case 2:
					$select.="
						FROM
							(
                              SELECT
                                      basic_email_agency_agency as code,
                                      basic_email_agency_campaign as campaign,
                                      ((basic_email_agency_opened*100)/basic_email_agency_recived) as opening,
                                      dna_neighborhood as loyalty,
                                      country_name as country,
                                      city_name as city,
                                      basic_email_agency_city
                              FROM
                                      basic_communications_email_agency
                              JOIN
                                      dashboard_neighborhood_agency
                              ON
                                      dna_agency=basic_email_agency_agency
                              JOIN
                                      country
                              ON
                                      country_id=basic_email_agency_country
                              JOIN
                                      city
                              ON
                                      city_id=basic_email_agency_city
                              GROUP BY
                                code, campaign
                            ) as drv2
					";
					break;

				default:
					$select.="
						(
                              SELECT
                                      basic_email_agent_agent as code,
                                      basic_email_agent_campaign as campaign,
                                      ((basic_email_agent_opened*100)/basic_email_agent_recived) as opening,
                                      dn_neighborhood as loyalty,
                                      country_name as country,
                                      city_name as city,
                                      basic_email_agent_city
                              FROM
                                      basic_communications_email_agent
                              JOIN
                                      dashboard_neighborhood_agent
                              ON
                                      dn_agent=basic_email_agent_agent
                              JOIN
                                      country
                              ON
                                      country_id=basic_email_agent_country
                              JOIN
                                      city
                              ON
                                      city_id=basic_email_agent_city
                              GROUP BY
                                code, campaign
                            ) as drv2
					";
					break;
			}
		}
		
		if(isset($_POST['byPercent']))
		{
			switch ($_POST['byPercent']) {
				case 0:
					# code...
					break;
				case 1:
					if($and){ $conditional.=" AND "; }else{ $conditional=" WHERE "; }
					$conditional.="(opening>=0 AND opening<=60)";
					$and=true;
					break;
				case 2:
					if($and){ $conditional.=" AND "; }else{ $conditional=" WHERE "; }
					$conditional.="(opening>=61 AND opening<=70)";
					$and=true;
					break;
				case 3:
					if($and){ $conditional.=" AND "; }else{ $conditional=" WHERE "; }
					$conditional.="(opening>=71 AND opening<=80)";
					$and=true;
					break;
				case 4:
					if($and){ $conditional.=" AND "; }else{ $conditional=" WHERE "; }
					$conditional.="(opening>=81 AND opening<=90)";
					$and=true;
					break;
				case 5:
					if($and){ $conditional.=" AND "; }else{ $conditional=" WHERE "; }
					$conditional.="(opening>=91 AND opening<=100)";
					$and=true;
					break;
				default:
					# code...
					break;
			}
		}

		
		if(isset($_POST['byLocation']))
		{
			if ($_POST['byLocation']!=0) 
			{

				$city=$_POST['byLocation'];

				switch ($_POST['byCondition']) {
					case 1:
						if($and){ $conditional.=" AND "; }else{ $conditional=" WHERE "; }
						$conditional.=" (basic_email_agent_city='".$city."')";
						$and=true;
						break;
					
					case 2:
						if($and){ $conditional.=" AND "; }else{ $conditional=" WHERE "; }
						$conditional.=" (basic_email_agency_city='".$city."')";
						$and=true;
						break;

					default:
						if($and){ $conditional.=" AND "; }else{ $conditional=" WHERE "; }
						$conditional.=" (basic_email_agent_city='".$city."')";
						$and=true;
						break;
				}
			}
		}

		if(isset($_POST['byNeighborhood']))
		{
			if($_POST['byNeighborhood']!='0')
			{
				$v=$_POST['byNeighborhood'];
				if($and){ $conditional.=" AND "; }else{ $conditional=" WHERE "; }
				$conditional.="(loyalty='".$v."')";
				$and=true;
			}
		}

		$query=$select.$conditional;
		$_SESSION['query_coms_0']=$query;
		//echo $query;
		echo "1";
	}

	if(isset($_POST['cleanTab1']) && $_POST['cleanTab1'])
	{
		unset($_SESSION['query_coms']);
		if(!isset($_SESSION['query_coms']))
		{
			echo "1";
		}
	}

	if(isset($_POST['cleanTab2']) && $_POST['cleanTab2'])
	{
		unset($_SESSION['query_coms_0']);
		if(!isset($_SESSION['query_coms_0']))
		{
			echo "1";
		}
	}
?>