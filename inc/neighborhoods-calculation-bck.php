<?php
	include("db.php");

	require('classes/session.class.php');
	$session = new session();
	$session->start_session('_s', false, $db);

	ini_set('max_execution_time', '-1');

	if(isset($_POST['neighborhood']))
	{	
		$today=date('Y-m-d');
		$todayM=(int)date('m');
		$todayY=(int)date('Y');
		$eachRoute=0;
		$eachRevenue=0;
		$limitRowDate=0;
		$limitRowRoute=0;
		$limitRowRevenue=0;
		$groupByRPRoutes=array();
		$groupByRPRevenue=array();
		$groupByRPDate=array();
		$groupByRoutes=array();
		$groupByRevenue=array();
		$groupByDate=array();
		$agents=array();
		$agencies=array();
		$whereDate="";

		$sql="
		SELECT 
			report_agent_date_flown
		FROM 
			report_agent
		ORDER BY 
			report_agent_date_flown DESC 
		LIMIT 0 , 1
		";

		//print_r("EXP1");

		if($stac3=$db->prepare($sql))
        {
          	$stac3->execute();
          	$stac3->bind_result($lChkDate);

      		if($stac3->fetch())
      		{
      			$ltmp=explode("-", $lChkDate);
      			$lChkM=$ltmp[1];
      			$lChkY=$ltmp[0];
      		}

          $stac3->close();
        }

		$sql="
            SELECT
              dn_month,dn_year
            FROM
              dashboard_neighborhood_agent
            ORDER BY
              dn_year DESC,
              dn_month DESC
            LIMIT 1,1
        ";

       //print_r("EXP2");
        if($stac1=$db->prepare($sql))
        {
          	$stac1->execute();
          	$stac1->bind_result($chkM,$chkY);

      		if($stac1->fetch())
      		{
      			if(!empty($chkM) && !empty($chkY))
      			{
	      			if($chkM==$todayM)
	      			{
	      				$chkDate=true;
	      				
	      			}
	      			else if($chkM==$lChkM)
	      			{
	      				$chkDate=true;
	      				
	      			}
	      			else
	      			{
	      				$chkDate=false;
	      				if($chkM==12)
	      				{
	      					$addY=$chkY+1;
	      					$addDate=$addY."-01-01";
	      					$whereDate="
		      				WHERE
		      					(main.report_agent_date_flown>='".$addDate."') 
		      				";
	      				}
	      				else
	      				{
	      					$addM=$chkM+1;
	      					$addDate=$chkY."-".$addM."-01";
	      					$whereDate="
		      				WHERE
		      					(main.report_agent_date_flown>='".$addDate."') 
		      				";
	      				}
	      				
	      				//print_r("DATE: FALSE 1");
	      			}
	      		}
		  		else
		  		{
		  			$chkDate=false;
		  			//print_r("DATE: FALSE 2 ");
		  		}
      		}
      		else
	  		{
	  			$chkDate=false;
	  			//print_r("DATE: FALSE 3");
	  		}
          $stac1->close();
        }
        //print_r($whereDate);


        if($chkDate)
        {
        	echo "021";
        	//print_r("DATE: TRUE");
        }
        else
        {

			$sql="
			SELECT
			        COUNT(*)
			FROM
			    (
					SELECT
					        (
					                SELECT 
					                        SUM(revenue.report_agent_copa_revenue) 
					                FROM 
					                        report_agent as revenue
					                WHERE 
					                        (revenue.report_agent_agent = main.report_agent_agent) 
					        ) as report_agent_revenue
					FROM
					        report_agent as main
					".$whereDate."
					GROUP BY
					        report_agent_agent
					ORDER BY
					        report_agent_revenue DESC
			    ) AS drv
			";

			if($stap1 = $db->prepare($sql))
			{
				$stap1->execute();
				$stap1->bind_result($totalRows);
				$stap1->fetch();

				$limitRowRevenue=$totalRows;
				$eachRowRevenue=ceil($totalRows/5);
				$eachRowRevenue=$eachRowRevenue-2;

				$stap1->close();
			}
			$totalRows=0;

			$sql="
			SELECT
			        COUNT(*)
			FROM
			    (
					SELECT
					        (
					                SELECT 
					                        COUNT(DISTINCT route_count.report_agent_flight_origin,route_count.report_agent_flight_destiny)
					                FROM 
					                        report_agent as route_count
					                WHERE 
					                        (route_count.report_agent_agent = main.report_agent_agent) 
					        ) as report_agent_routes_sales
					FROM
					        report_agent as main
					".$whereDate."
					GROUP BY
					       report_agent_agent
					ORDER BY
					       report_agent_routes_sales DESC
			    ) AS drv
			";

			if($stap2 = $db->prepare($sql))
			{
				$stap2->execute();
				$stap2->bind_result($totalRows);
				$stap2->fetch();

				$limitRowRoutes=$totalRows;
				$eachRowRoutes=ceil($totalRows/5);
				$eachRowRoutes=$eachRowRoutes-2;

				$stap2->close();
			}
			$totalRows=0;


			$sql="
			SELECT
			    COUNT(*)
			FROM
		        (
		        SELECT 
		                main.report_agent_date_flown
		        FROM 
		                report_agent as main
		        ".$whereDate."
		        GROUP BY
		                main.report_agent_agent
		        ORDER BY 
		                main.report_agent_date_flown DESC
		        ) as drv
			";

			//$ctYMRows=0;

			if($stai1 = $db->prepare($sql))
			{
				$stai1->execute();
				$stai1->bind_result($totalRows);
				
				while($stai1->fetch())
				{
					$limitRowDate=$totalRows;
					$eachRowDate=ceil($limitRowDate/5);
					$eachRowDate=$eachRowDate-2;
				}

				$stai1->close();
			}


			if($limitRowDate>0)
			{
				$currentRow=0;
				do{
					$currentRow+=$eachRowDate;

					$groupByRPDate[]=$currentRow;

				}while ($currentRow<$limitRowDate);
			}
			else
			{
				exit("005");
			}

			if($limitRowRevenue>0)
			{
				$currentRow=0;
				do{
					$currentRow+=$eachRowRevenue;

					$groupByRPRevenue[]=$currentRow;

				}while ($currentRow<$limitRowRevenue);
			}
			else
			{
				exit("006");
			}


			if($limitRowRoutes>0)
			{
				$currentRow=0;
				do{
					$currentRow+=$eachRowRoutes;

					$groupByRPRoutes[]=$currentRow;

				}while ($currentRow<$limitRowRoutes);
			}
			else
			{
				exit("007");
			}
/*
			print_r($limitRowDate);
			print_r("///");
			print_r($limitRowRoutes);
			print_r("///");
			print_r($limitRowRevenue);
			print_r("///");
			print_r($eachRowDate);
			print_r("***");
			print_r($eachRowRoutes);
			print_r("***");
			print_r($eachRowRevenue);
			print_r("***");
*/
			$sql="
			SELECT
	        (
	                SELECT 
	                main.report_agent_date_flown
	                FROM 
	                report_agent as main
	                ".$whereDate."
	                GROUP BY
	                main.report_agent_agent
	                ORDER BY 
	                main.report_agent_date_flown DESC
	                LIMIT
	                ".$groupByRPDate[0].",1
	        ) as gp0,
	        (
	                SELECT 
	                main.report_agent_date_flown
	                FROM 
	                report_agent as main
	                ".$whereDate."
	                GROUP BY
	                main.report_agent_agent
	                ORDER BY 
	                main.report_agent_date_flown DESC
	                LIMIT
	                ".$groupByRPDate[1].",1
	        ) as gp1,
	        (
	                SELECT 
	                main.report_agent_date_flown
	                FROM 
	                report_agent as main
	                ".$whereDate."
	                GROUP BY
	                main.report_agent_agent
	                ORDER BY 
	                main.report_agent_date_flown DESC
	                LIMIT
	                ".$groupByRPDate[2].",1
	        ) as gp2,
	        (
	                SELECT 
	                main.report_agent_date_flown
	                FROM 
	                report_agent as main
	                ".$whereDate."
	                GROUP BY
	                main.report_agent_agent
	                ORDER BY 
	                main.report_agent_date_flown DESC
	                LIMIT
	                ".$groupByRPDate[3].",1
	        ) as gp3,
	        (
	                SELECT 
	                main.report_agent_date_flown
	                FROM 
	                report_agent as main
	                ".$whereDate."
	                GROUP BY
	                main.report_agent_agent
	                ORDER BY 
	                main.report_agent_date_flown DESC
	                LIMIT
	                ".$groupByRPDate[4].",1
	        ) as gp4
			";


			if($stai = $db->prepare($sql))
			{
				$stai->execute();
				$stai->bind_result($gp0,$gp1,$gp2,$gp3,$gp4);
				
				while($stai->fetch())
				{
					$groupByDate[0]=$gp0;
					$groupByDate[1]=$gp1;
					$groupByDate[2]=$gp2;
					$groupByDate[3]=$gp3;
					$groupByDate[4]=$gp4;
				}

				$stai->close();
			}

			$sql="
			SELECT
	        (
	                SELECT
	                        (
	                                SELECT 
	                                        SUM(revenue.report_agent_copa_revenue) 
	                                FROM 
	                                        report_agent as revenue
	                                WHERE 
	                                        (revenue.report_agent_agent = main.report_agent_agent) 
	                        ) as report_agent_revenue
	                FROM
	                        report_agent as main
	                ".$whereDate."
	                GROUP BY
	                        report_agent_agent
	                ORDER BY
	                        report_agent_revenue DESC
	                LIMIT
	                        ".$groupByRPRevenue[0].",1
	        ) AS gp0,
	        (
	                SELECT
	                        (
	                                SELECT 
	                                        SUM(revenue.report_agent_copa_revenue) 
	                                FROM 
	                                        report_agent as revenue
	                                WHERE 
	                                        (revenue.report_agent_agent = main.report_agent_agent) 
	                        ) as report_agent_revenue
	                FROM
	                        report_agent as main
	                ".$whereDate."
	                GROUP BY
	                        report_agent_agent
	                ORDER BY
	                        report_agent_revenue DESC
	                LIMIT
	                        ".$groupByRPRevenue[1].",1
	        ) AS gp1,
	        (
	                SELECT
	                        (
	                                SELECT 
	                                        SUM(revenue.report_agent_copa_revenue) 
	                                FROM 
	                                        report_agent as revenue
	                                WHERE 
	                                        (revenue.report_agent_agent = main.report_agent_agent) 
	                        ) as report_agent_revenue
	                FROM
	                        report_agent as main
					".$whereDate."
	                GROUP BY
	                        report_agent_agent
	                ORDER BY
	                        report_agent_revenue DESC
	                LIMIT
	                        ".$groupByRPRevenue[2].",1
	        ) AS gp2,
	        (
	                SELECT
	                        (
	                                SELECT 
	                                        SUM(revenue.report_agent_copa_revenue) 
	                                FROM 
	                                        report_agent as revenue
	                                WHERE 
	                                        (revenue.report_agent_agent = main.report_agent_agent) 
	                        ) as report_agent_revenue
	                FROM
	                        report_agent as main
					".$whereDate."
	                GROUP BY
	                        report_agent_agent
	                ORDER BY
	                        report_agent_revenue DESC
	                LIMIT
	                        ".$groupByRPRevenue[3].",1
	        ) AS gp3,
	        (
	                SELECT
	                        (
	                                SELECT 
	                                        SUM(revenue.report_agent_copa_revenue) 
	                                FROM 
	                                        report_agent as revenue
	                                WHERE 
	                                        (revenue.report_agent_agent = main.report_agent_agent) 
	                        ) as report_agent_revenue
	                FROM
	                        report_agent as main
					".$whereDate."
	                GROUP BY
	                        report_agent_agent
	                ORDER BY
	                        report_agent_revenue DESC
	                LIMIT
	                        ".$groupByRPRevenue[4].",1
	        ) AS gp4
			";


			if($stai2 = $db->prepare($sql))
			{
				$stai2->execute();
				$stai2->bind_result($gp0,$gp1,$gp2,$gp3,$gp4);
				
				while($stai2->fetch())
				{
					$groupByRevenue[0]=$gp0;
					$groupByRevenue[1]=$gp1;
					$groupByRevenue[2]=$gp2;
					$groupByRevenue[3]=$gp3;
					$groupByRevenue[4]=$gp4;
				}

				$stai2->close();
			}

			$sql="
			SELECT
	        (
	                SELECT
	                        (
	                                SELECT 
	                                        COUNT(DISTINCT route_count.report_agent_flight_origin,route_count.report_agent_flight_destiny)
	                                FROM 
	                                        report_agent as route_count
	                                WHERE 
	                                        (route_count.report_agent_agent = main.report_agent_agent) 
	                        ) as report_agent_routes_sales
	                FROM
	                        report_agent as main
					".$whereDate."
	                GROUP BY
	                       report_agent_agent
	                ORDER BY
	                       report_agent_routes_sales DESC
	                LIMIT
	                        ".$groupByRPRoutes[0].",1
	        ) AS gp0,
	        (
	                SELECT
	                        (
	                                SELECT 
	                                        COUNT(DISTINCT route_count.report_agent_flight_origin,route_count.report_agent_flight_destiny)
	                                FROM 
	                                        report_agent as route_count
	                                WHERE 
	                                        (route_count.report_agent_agent = main.report_agent_agent) 
	                        ) as report_agent_routes_sales
	                FROM
	                        report_agent as main
	                ".$whereDate."        
	                GROUP BY
	                       report_agent_agent
	                ORDER BY
	                       report_agent_routes_sales DESC
	                LIMIT
	                        ".$groupByRPRoutes[1].",1
	        ) AS gp1,
	        (
	                SELECT
	                        (
	                                SELECT 
	                                        COUNT(DISTINCT route_count.report_agent_flight_origin,route_count.report_agent_flight_destiny)
	                                FROM 
	                                        report_agent as route_count
	                                WHERE 
	                                        (route_count.report_agent_agent = main.report_agent_agent) 
	                        ) as report_agent_routes_sales
	                FROM
	                        report_agent as main
	                ".$whereDate."
	                GROUP BY
	                      report_agent_agent
	                ORDER BY
	                       report_agent_routes_sales DESC
	                LIMIT
	                        ".$groupByRPRoutes[2].",1
	        ) AS gp2,
	        (
	                SELECT
	                        (
	                                SELECT 
	                                        COUNT(DISTINCT route_count.report_agent_flight_origin,route_count.report_agent_flight_destiny)
	                                FROM 
	                                        report_agent as route_count
	                                WHERE 
	                                        (route_count.report_agent_agent = main.report_agent_agent) 
	                        ) as report_agent_routes_sales
	                FROM
	                        report_agent as main
	                ".$whereDate."
	                GROUP BY
	                       report_agent_agent
	                ORDER BY
	                       report_agent_routes_sales DESC
	                LIMIT
	                        ".$groupByRPRoutes[3].",1
	        ) AS gp3,
	        (
	                SELECT
	                        (
	                                SELECT 
	                                        COUNT(DISTINCT route_count.report_agent_flight_origin,route_count.report_agent_flight_destiny)
	                                FROM 
	                                        report_agent as route_count
	                                WHERE 
	                                        (route_count.report_agent_agent = main.report_agent_agent) 
	                        ) as report_agent_routes_sales
	                FROM
	                        report_agent as main
	                ".$whereDate."
	                GROUP BY
	                       report_agent_agent
	                ORDER BY
	                       report_agent_routes_sales DESC
	                LIMIT
	                        ".$groupByRPRoutes[4].",1
	        ) AS gp4
			";


			if($stai3 = $db->prepare($sql))
			{
				$stai3->execute();
				$stai3->bind_result($gp0,$gp1,$gp2,$gp3,$gp4);
				
				while($stai3->fetch())
				{
					$groupByRoutes[0]=$gp0;
					$groupByRoutes[1]=$gp1;
					$groupByRoutes[2]=$gp2;
					$groupByRoutes[3]=$gp3;
					$groupByRoutes[4]=$gp4;
				}

				$stai3->close();
			}

		
			print_r($groupByRoutes);
			
			print_r($groupByRevenue);
			
			print_r($groupByDate);
		
			
			$sql="
	            SELECT
	              dn_month,dn_year
	            FROM
	              dashboard_neighborhood_agent
	            ORDER BY
	              dn_year DESC,
	              dn_month DESC
	            LIMIT 1,1
	        ";

	       
	        if($stac=$db->prepare($sql))
	        {
	          	$stac->execute();
	          	$stac->bind_result($lM,$lY);

	      		if($stac->fetch())
	      		{
	      			//$lVDate=$lM."-".$lY;	
	      			if(!empty($lM) && !empty($lY))
	      			{
		      			if((int)$lM!=12)
		      			{	
		      				$lM=$lM+1;
		      				$lVDate=$lY."-".$lM."-01";

		      			}
		      			else
		      			{
		      				$lM=1;
		      				$lY=$lY+1;
		      				$lVDate=$lY."-".$lM."-01";
		      			}
		      		}
			  		else
			  		{
			  			$lVDate=NULL;
			  		}
	      		}
	      		else
		  		{
		  			$lVDate=NULL;
		  		}
	          $stac->close();
	        }

			$sql='
				SELECT
					main.report_agent_id,
					main.report_agent_agent,
					(
						SELECT x.agent_agency FROM agent as x WHERE x.agent_id=main.report_agent_agent
					) as report_agent_agency,
					(
						SELECT 
							COUNT(DISTINCT route_count.report_agent_flight_origin,route_count.report_agent_flight_destiny)
						FROM 
							report_agent as route_count
						WHERE 
							(route_count.report_agent_agent = main.report_agent_agent) 
					) as report_agent_routes_sales,
					(	SELECT
							date_flown.report_agent_date_flown
						FROM
							report_agent as date_flown
						WHERE
							(date_flown.report_agent_agent=main.report_agent_agent)
						ORDER BY
							date_flown.report_agent_date_flown DESC
						LIMIT 0,1
					) as report_agent_date,
					(
						SELECT 
							SUM(revenue.report_agent_copa_revenue) 
						FROM 
							report_agent as revenue
						WHERE 
							(revenue.report_agent_agent = main.report_agent_agent) 
					) as report_agent_revenue
				FROM
					report_agent as main
				GROUP BY
					main.report_agent_agent
			';

			$allRows=0;
			$error=0;
			$succes=0;

			if($sta = $db->prepare($sql))
			{
				$sta->execute();
				$sta->bind_result($id,$agent,$agency,$routes,$vDate,$revenue);
				$cnt=0;
				while($sta->fetch())
				{
					$revenue=(float)$revenue;
					$routes=(int)$routes;

					#New format date mm-yyyy

					$d=explode("-", $vDate);
					$vPeriod=$d[1]."-".$d[0];
		
	                //$pastM=$todayM-1;
	                //$todayMY=$pastM."-".$todayY;
	                $todayMY=$todayY."-".$todayM."-01";
	                //echo $vDate."|".$lVDate."|".$todayMY."|";
	                
	                if(is_null($lVDate))
	                {
	                	
	                	$agents[$agent][$vPeriod]['agent']=$agent;
						$agents[$agent][$vPeriod]['agency']=$agency;
						//$agents[$agent][$vPeriod]['period']=$vDate;
						if(empty($agents[$agent][$vPeriod]['period']))
	                	{
	                		$agents[$agent][$vPeriod]['period']=$vDate;
	                	}
	                	else
	                	{
	                		if($agents[$agent][$vPeriod]['period']>$vDate)
	                		{
	                			$agents[$agent][$vPeriod]['period']=$vDate;
	                		}
	                	}

	                	if(empty($agents[$agent][$vPeriod]['revenue']))
	                	{
	                		$agents[$agent][$vPeriod]['revenue']=$revenue;
	                	}
	                	else
	                	{
	                		$agents[$agent][$vPeriod]['revenue']+=$revenue;
	                	}

	                	if(empty($agents[$agent][$vPeriod]['routes']))
	                	{
	                		$agents[$agent][$vPeriod]['routes']=$routes;
	                	}
	                	else
	                	{
	                		$agents[$agent][$vPeriod]['routes']+=$routes;
	                	}

						//print_r("AG: ".$agent." PR: ".$vPeriod."<br>");
						//print_r($agents);
						//$cnt++;


	                }
	                else
	                {
		                if($vDate>=$lVDate && $vDate<$todayMY)
		                {
		                	
		                	$agents[$agent][$vPeriod]['agent']=$agent;
							$agents[$agent][$vPeriod]['agency']=$agency;
							//$agents[$agent][$vPeriod]['period']=$vDate;
							if(empty($agents[$agent][$vPeriod]['period']))
		                	{
		                		$agents[$agent][$vPeriod]['period']=$vDate;
		                	}
		                	else
		                	{
		                		if($agents[$agent][$vPeriod]['period']>$vDate)
		                		{
		                			$agents[$agent][$vPeriod]['period']=$vDate;
		                		}
		                	}
							
							if(empty($agents[$agent][$vPeriod]['revenue']))
			            	{
			            		$agents[$agent][$vPeriod]['revenue']=$revenue;
			            	}
			            	else
			            	{
			            		$agents[$agent][$vPeriod]['revenue']+=$revenue;
			            	}

			            	if(empty($agents[$agent][$vPeriod]['routes']))
			            	{
			            		$agents[$agent][$vPeriod]['routes']=$routes;
			            	}
			            	else
			            	{
			            		$agents[$agent][$vPeriod]['routes']+=$routes;
			            	}
							
							//echo "AG2: ".$agent." PR2: ".$vPeriod."<br>";
		                }
		            }
				}

				//echo sizeof($agents)."<br>";
				//echo $cnt;
				$sta->reset();
			}


			//print_r($agents);

			if(!empty($agents))
			{
				foreach ($agents as $r) 
				{
					foreach ($r as $row) 
					{
					
						if($row['period']>=$groupByDate[0])
						{
							$gDy=5;
						}
						else if($row['period']<$groupByDate[0] && $row['period']>=$groupByDate[1])
						{
							$gDy=4;
						}
						else if ($row['period']<$groupByDate[1] && $row['period']>=$groupByDate[2]) {
							$gDy=3;
						}
						else if ($row['period']<$groupByDate[2] && $row['period']>=$groupByDate[3]) {
							$gDy=2;
						}
						else if ($row['period']<$groupByDate[3]) {
							$gDy=1;
						}

						//print_r("P:".$row['period']);
						//print_r($groupByDate);
						//print_r(" GP:".$gDy);
						//print_r("----");

						if($row['revenue']>$groupByRevenue[0])
						{
							$gREx=5;
						}
						else if($row['revenue']<=$groupByRevenue[0] && $row['revenue']>$groupByRevenue[1])
						{
							$gREx=4;
						}
						else if ($row['revenue']<=$groupByRevenue[1] && $row['revenue']>$groupByRevenue[2]) {
							$gREx=3;
						}
						else if ($row['revenue']<=$groupByRevenue[2] && $row['revenue']>$groupByRevenue[3]) {
							$gREx=2;
						}
						else if ($row['revenue']<$groupByRevenue[3]) {
							$gREx=1;
						}

						//print_r("Re:".$row['revenue']);
						//print_r($groupByRevenue);
						//print_r(" GRe:".$gREx);
						//print_r("----");

						if($row['routes']>$groupByRoutes[0])
						{
							$gROz=5;
						}
						else if($row['routes']<=$groupByRoutes[0] && $row['routes']>$groupByRoutes[1])
						{
							$gROz=4;
						}
						else if ($row['routes']<=$groupByRoutes[1] && $row['routes']>$groupByRoutes[2]) {
							$gROz=3;
						}
						else if ($row['routes']<=$groupByRoutes[2] && $row['routes']>$groupByRoutes[3]) {
							$gROz=2;
						}
						else if ($row['routes']<$groupByRoutes[3]) {
							$gROz=1;
						}

						//print_r("Ro:".$row['routes']);
						//print_r($groupByRoutes);
						//print_r(" GRo:".$gROz);
						//print_r("----");

						if($gDy<=2 && $gREx<=2 && $gROz<=2)
						{
							$nbh='V1';
						}
						else if ($gDy<=2 && $gREx<=2 && $gROz>=3) {
							$nbh='V2';
						}
						else if ($gDy<=2 && $gREx>=3 && $gROz<=2) {
							$nbh='V3';
						}
						else if ($gDy<=2 && $gREx>=3 && $gROz>=3) {
							$nbh='V4';
						}
						else if ($gDy>=3 && $gREx<=2 && $gROz<=2) {
							$nbh='V5';
						}
						else if ($gDy>=3 && $gREx<=2 && $gROz>=3) {
							$nbh='V6';
						}
						else if ($gDy>=3 && $gREx>=3 && $gROz<=2) {
							$nbh='V7';
						}
						else if ($gDy>=3 && $gREx>=3 && $gROz>=3) {
							$nbh='V8';
						}

						//print_r("Y:".$gDy." X:".$gREx." Z:".$gROz." V:".$nbh);
						//print_r("----");

						#New format date mm-yyyy
						$d=explode("-", $row['period']);
						$m=$d[1];
						$y=$d[0];

						if($sta2 = $db->prepare("INSERT INTO dashboard_neighborhood_agent (dn_neighborhood,dn_agent,dn_agency,dn_date_record,dn_month,dn_year,dn_revenue,dn_routes_sold) VALUES (?,?,?,?,?,?,?,?)"))
						{
							$sta2->bind_param('ssisiidi',$nbh,$row['agent'],$row['agency'],$today,$m,$y,$row['revenue'],$row['routes']);
						
							if(!$sta2->execute())
			                {
			                    $error++;
			                }
			                else
			                {
			                    $succes++;
			                }
							$sta2->close();
						}

						$allRows++;
					}
				}
			}
			//echo $allRows.".".$succes.".".$error;

			if($allRows==$succes)
			{
				$allRows=0;
				$error=0;
				$succes=0;

				#Evaluar vencindarios por agencias.
				$sql="
	                SELECT
	                  dna_month,dna_year
	                FROM
	                  dashboard_neighborhood_agency
	                ORDER BY
	                  dna_year DESC,
	                  dna_month DESC
	                LIMIT 0,1
	            ";

	            if($staa=$db->prepare($sql))
	            {
					$staa->execute();
					$staa->bind_result($laM,$laY);

					if($staa->fetch())
					{
						//$laVDate=$laM."-".$laY;
						if(!empty($laM) && !empty($laY))
		      			{
			      			if((int)$laM!=12)
			      			{	
			      				$laM=$laM+1;
			      				$laVDate=$laY."-".$laM."-01";

			      			}
			      			else
			      			{
			      				$laM=1;
			      				$laY=$laY+1;
			      				$laVDate=$laY."-".$laM."-01";
			      			}
			      		}
				  		else
				  		{
				  			$laVDate=NULL;
				  		}
					}
					else
					{
					$laVDate=NULL;
					}
					$staa->reset();
	            }

				$sql1='SELECT dn_agency, dn_revenue, dn_month, dn_year, dn_routes_sold, (SELECT report_agent_date_flown FROM report_agent WHERE report_agent_agent=dn_agent ORDER BY report_agent_date_flown DESC LIMIT 0,1) as dn_last_sale FROM dashboard_neighborhood_agent';

				
				//echo $sql1;

				if($stad = $db->prepare($sql1))
				{
					$stad->execute();
					$stad->bind_result($aag,$are,$am,$ay,$aro,$als);
					
					while($stad->fetch())
					{
						$are=(float)$are;
						$aro=(int)$aro;

						//$vDate=$am."-".$ay;

		                //$pastM=$todayM-1;
		                //$todayMY=$pastM."-".$todayY;

		                $todayMY=$todayY."-".$todayM."-01";
		                $vDate=$am."-".$ay;

		                //echo $vDate."|".$laVDate."|".$todayMY."|";

		                if($laVDate==NULL)
		                {
		                	$agencies[$aag][$vDate]['agency']=$aag;

		                	if(empty($agencies[$aag][$vDate]['period']))
		                	{
		                		$agencies[$aag][$vDate]['period']=$als;
		                	}
		                	else
		                	{
		                		if($agencies[$aag][$vDate]['period']>$als)
		                		{
		                			$agencies[$aag][$vDate]['period']=$als;
		                		}
		                	}

							//$agencies[$aag][$vDate]['period']=$vDate;

							if(empty($agencies[$aag][$vDate]['revenue']))
			            	{
			            		$agencies[$aag][$vDate]['revenue']=$are;
			            	}
			            	else
			            	{
			            		$agencies[$aag][$vDate]['revenue']+=$are;
			            	}

			            	if(empty($agencies[$aag][$vDate]['routes']))
			            	{
			            		$agencies[$aag][$vDate]['routes']=$aro;
			            	}
			            	else
			            	{
			            		$agencies[$aag][$vDate]['routes']+=$aro;
			            	}
		                }
		                else
		                {
			                if($als>=$laVDate && $als<$todayMY)
			                {
			                	if(empty($agencies[$aag][$vDate]['period']))
			                	{
			                		$agencies[$aag][$vDate]['period']=$als;
			                	}
			                	else
			                	{
			                		if($agencies[$aag][$vDate]['period']>$als)
			                		{
			                			$agencies[$aag][$vDate]['period']=$als;
			                		}
			                	}
								$agencies[$aag][$vDate]['agency']=$aag;

								//$agencies[$aag][$vDate]['period']=$vDate;

								if(empty($agencies[$aag][$vDate]['revenue']))
				            	{
				            		$agencies[$aag][$vDate]['revenue']=$are;
				            	}
				            	else
				            	{
				            		$agencies[$aag][$vDate]['revenue']+=$are;
				            	}

				            	if(empty($agencies[$aag][$vDate]['routes']))
				            	{
				            		$agencies[$aag][$vDate]['routes']=$aro;
				            	}
				            	else
				            	{
				            		$agencies[$aag][$vDate]['routes']+=$aro;
				            	}
							}
						}
					}
					$stad->reset();
				}

				//print_r($agencies);

				//var_dump($agencies);
				$row=null;

				if(!empty($agencies))
				{
					foreach ($agencies as $re) 
					{
						foreach ($re as $row) 
						{
						
							if($row['period']>$groupByDate[0])
							{
								$gDy=5;
							}
							else if($row['period']<=$groupByDate[0] && $row['period']>$groupByDate[1])
							{
								$gDy=4;
							}
							else if ($row['period']<=$groupByDate[1] && $row['period']>$groupByDate[2]) {
								$gDy=3;
							}
							else if ($row['period']<=$groupByDate[2] && $row['period']>$groupByDate[3]) {
								$gDy=2;
							}
							else if ($row['period']<$groupByDate[3]) {
								$gDy=1;
							}

							if($row['revenue']>$groupByRevenue[0])
							{
								$gREx=5;
							}
							else if($row['revenue']<=$groupByRevenue[0] && $row['revenue']>$groupByRevenue[1])
							{
								$gREx=4;
							}
							else if ($row['revenue']<=$groupByRevenue[1] && $row['revenue']>$groupByRevenue[2]) {
								$gREx=3;
							}
							else if ($row['revenue']<=$groupByRevenue[2] && $row['revenue']>$groupByRevenue[3]) {
								$gREx=2;
							}
							else if ($row['revenue']<$groupByRevenue[3]) {
								$gREx=1;
							}

							if($row['routes']>$groupByRoutes[0])
							{
								$gROz=5;
							}
							else if($row['routes']<=$groupByRoutes[0] && $row['routes']>$groupByRoutes[1])
							{
								$gROz=4;
							}
							else if ($row['routes']<=$groupByRoutes[1] && $row['routes']>$groupByRoutes[2]) {
								$gROz=3;
							}
							else if ($row['routes']<=$groupByRoutes[2] && $row['routes']>$groupByRoutes[3]) {
								$gROz=2;
							}
							else if ($row['routes']<$groupByRoutes[3]) {
								$gROz=1;
							}

							if($gDy<=2 && $gREx<=2 && $gROz<=2)
							{
								$nbh='V1';
							}
							else if ($gDy<=2 && $gREx<=2 && $gROz>=3) {
								$nbh='V2';
							}
							else if ($gDy<=2 && $gREx>=3 && $gROz<=2) {
								$nbh='V3';
							}
							else if ($gDy<=2 && $gREx>=3 && $gROz>=3) {
								$nbh='V4';
							}
							else if ($gDy>=3 && $gREx<=2 && $gROz<=2) {
								$nbh='V5';
							}
							else if ($gDy>=3 && $gREx<=3 && $gROz>=3) {
								$nbh='V6';
							}
							else if ($gDy>=3 && $gREx>=3 && $gROz<=3) {
								$nbh='V7';
							}
							else if ($gDy>=3 && $gREx>=3 && $gROz>=3) {
								$nbh='V8';
							}

							#New format date mm-yyyy
							$d=explode("-", $row['period']);
							$m=$d[1];
							$y=$d[0];

							if($sta3 = $db->prepare("INSERT INTO dashboard_neighborhood_agency (dna_neighborhood,dna_agency,dna_date_record,dna_month,dna_year,dna_revenue,dna_routes_sold) VALUES (?,?,?,?,?,?,?)"))
							{
								$sta3->bind_param('sisiidi',$nbh,$row['agency'],$today,$m,$y,$row['revenue'],$row['routes']);
							
								if(!$sta3->execute())
				                {
				                    $error++;
				                }
				                else
				                {
				                    $succes++;
				                }
								$sta3->close();
							}

							$allRows++;
						}
					}
				}
				else
				{
					echo "0";
				}


				//echo $allRows.".".$succes.".".$error;

				if($allRows==$succes)
				{
					echo "1";
				}
				else if ($allRows==$error) 
				{
					echo "004";
				}
				else
				{
					echo "003";
				}
			}
			else if ($allRows==$error) 
			{
				echo "002";
			}
			else
			{
				echo "001";
			}
		}
	}

?>