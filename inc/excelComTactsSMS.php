<?php 
	include("db.php");
	require('classes/session.class.php');

	include('../views/vfunctions.php');
    
    error_reporting(E_ALL);
	ini_set('display.errors', 1);

	$session = new session();
	$session->start_session('_s', false, $db);

	set_include_path(get_include_path() . PATH_SEPARATOR . 'xls/Classes/');
	include 'PHPExcel.php';
	$query1 = $_SESSION['query_coms_tacs_0'];
	$query2 = $_SESSION['query_coms_tacs_1'];

	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("Copa");
	$objPHPExcel->getProperties()->setLastModifiedBy("LFD");
	$objPHPExcel->getProperties()->setTitle("Reporte");
	$objPHPExcel->getProperties()->setSubject("Reporte de Copa");
	$objPHPExcel->getProperties()->setDescription("Reporte de Copa Comunicaciones Tacticas-Email");

	$reportName = "REPORTE COMUNICACIONES TACTICAS-SMS";

	$colName1 = array('---','Recibidos','%Clics','Tickets','Ingresos');

	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $reportName); 

	$lista = array('A','B','C','D','E');
 
	for($u=0;$u<sizeof($lista);$u++){
    	$objPHPExcel->getActiveSheet()->getColumnDimension($lista[$u])->setAutoSize(true);
    	$objPHPExcel->getActiveSheet()->getStyle($lista[$u])->getAlignment()
    				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($lista[$u].'2',  $colName1[$u]);
    	  	
    }
        
    $i=3;

    if($sta=$db->prepare($query1)){

        $sta->execute();                 
        $sta->bind_result($type,$recieved,$opened,$tickets,$revenue);
        
        while ($sta->fetch()){

        	if($type == 1){
        		$type = "Tactica"; 
        	}else{ 
        		$type = "Basica"; 
        	}
        	
        	if($recieved == NULL){
        	    $recieved = '0';
        	}else{
        		$recieved = number_format((float)round($recieved, 2, PHP_ROUND_HALF_ODD), 0, '.', ','); 
        	}
        	
        	if($opened == NULL){
        		$opened = '0';  
        	}else{ 
        	    $opened = number_format((float)round($opened, 2, PHP_ROUND_HALF_ODD), 0, '.', ','); 
        	}

        	if($tickets == NULL){
        		$tickets = '0'; 
        	}else{ 
        		$tickets = number_format((float)round($tickets, 2, PHP_ROUND_HALF_ODD), 0, '.', ',');
        	}

        	if($revenue == NULL){
        		$revenue = '0.00'; 
        	}else{ 
        		$revenue = "$ ".number_format((float)round($revenue, 2, PHP_ROUND_HALF_ODD), 2, '.', ','); 
        	}

        	$objPHPExcel->setActiveSheetIndex(0)
        	->setCellValue('A'.$i, $type)
        	->setCellValue('B'.$i, $recieved)
        	->setCellValue('C'.$i, $opened)
        	->setCellValue('D'.$i, $tickets)
        	->setCellValue('E'.$i, $revenue);
        	$i++;
        }
        $sta->close();
    }

    $objPHPExcel->createSheet();
    $reportName = "REPORTE COMUNICACIONES TACTICAS-SMS";
    $colName2 = array('Agente/Agencia','% Abiertos','Vecindario','Pais','Ciudad','Tickets','Ingresos');
    $lista = array('A','B','C','D','E','F','G');

    $objPHPExcel->setActiveSheetIndex(1)->mergeCells('A1:G1');
	$objPHPExcel->setActiveSheetIndex(1)->setCellValue('A1', $reportName); 

    for($u=0;$u<sizeof($lista);$u++){
    	$objPHPExcel->getActiveSheet()->getColumnDimension($lista[$u])->setAutoSize(true);
    	$objPHPExcel->getActiveSheet()->getStyle($lista[$u])->getAlignment()
    				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    	$objPHPExcel->setActiveSheetIndex(1)->setCellValue($lista[$u].'2',  $colName2[$u]);   	  	
    }

    $i=3;

    if($sta=$db->prepare($query2)){

        $sta->execute();                 
        $sta->bind_result($code,$opening,$loyalty,$country,$city,$cityCod,$tickets,$revenue);
        
        while ($sta->fetch()){
        	
        	if($opening == NULL){
        		$opening = '0';
        	}else{ 
        		$opening = number_format((float)round($opening, 2, PHP_ROUND_HALF_ODD), 0, '.', ','); 
        	}

        	if($loyalty == NULL){
        		$loyalty = 'No Aplica'; 
        	}else{ 
        		$loyalty = neighborhoodsName($loyalty); 
        	}

        	if($tickets == NULL){
        		$tickets = '0'; 
        	}else{
        		$tickets = number_format((float)round($tickets, 2, PHP_ROUND_HALF_ODD), 0, '.', ','); 
        	}

        	if($revenue == NULL){
        		$revenue = '0.00'; 
        	}else{ 
        		$revenue = "$ ".number_format((float)round($revenue, 2, PHP_ROUND_HALF_ODD), 2, '.', ','); 
        	}
        	
        	$objPHPExcel->setActiveSheetIndex(1)
        	->setCellValue('A'.$i, $code)
        	->setCellValue('B'.$i, $opening)
        	->setCellValue('C'.$i, $loyalty)
        	->setCellValue('D'.$i, $country)
        	->setCellValue('E'.$i, $city)
        	->setCellValue('F'.$i, $tickets)
        	->setCellValue('G'.$i, $revenue);
        	$i++;
        }
        $sta->close();
    }
	
	$esReportName = array(
	    'font' => array(
	        'name'      => 'Times New Roman',
	        'bold'      => true,
	        'italic'    => false,
	        'strike'    => false,
	        'size'      => 12,
	        'color'     => array('rgb' => 'FFFFFF')
	    ),
	    'fill' => array(
	        'type'  => PHPExcel_Style_Fill::FILL_SOLID,
	        'color' => array('rgb' => '006699')
	    ),
	    'borders' => array(
	        'allborders' => array(
	            'style' => PHPExcel_Style_Border::BORDER_NONE
	        )
	    ),
	    'alignment' => array(
	        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	        'rotation' => 0,
	        'wrap' => TRUE
	    )
		);
	
	$objPHPExcel->setActiveSheetIndex(0)->getStyle('A1:F1')->applyFromArray($esReportName);
	$objPHPExcel->setActiveSheetIndex(1)->getStyle('A1:G1')->applyFromArray($esReportName);

    $esColName = array(
	    'font' => array(
	        'name'  => 'Times new Roman',
	        'bold'  => false,
	        'size'  => 12,
	        'color' => array(
	            'rgb' => '333333'
	        )
	    ),
	    'fill' => array(
	        'type'  => PHPExcel_Style_Fill::FILL_SOLID,
	        'color' => array('rgb' => 'cdcdcd')
	    ),
	    'borders' => array(
	        'allborders' => array(
	            'style' => PHPExcel_Style_Border::BORDER_NONE
	        )
	    ),
	    
	);
	$objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:F2')->applyFromArray($esColName);
	$objPHPExcel->setActiveSheetIndex(1)->getStyle('A2:G2')->applyFromArray($esColName);

    $objPHPExcel->setActiveSheetIndex(0)->setTitle('Reporte ComTact-SMS T1');	 
    $objPHPExcel->setActiveSheetIndex(1)->setTitle('Reporte ComTact-SMS T2');
	$objPHPExcel->setActiveSheetIndex(0);	 

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="reporteCTSMS.xls"');
	header('Cache-Control: max-age=0');

	require_once 'PHPExcel/IOFactory.php';
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
	$objWriter->save('php://output',__FILE__);
	exit;
?>