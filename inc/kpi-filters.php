<?php
	include("db.php");

	require('classes/session.class.php');
	$session = new session();
	$session->start_session('_s', false, $db);

    //var_dump($_POST);

	if(isset($_POST['filtersKpi']) && $_POST['filtersKpi'])
    {
        $today=date('Y-m-d');
        $todayM=(int)date('m');
        $todayY=(int)date('Y');
        $todayPY=$todayY-1;
        $todayD=(int)date('d');

        if(isset($_POST['byPeriod']))
        {
          $start=explode('-', $_POST['byPeriod']);
          //$finish=explode('-', $_POST['byPeriodFinish']);

          if($start[0]>0 && $start[1]>1999)
          {

            $bfrDate=$start[1]."-".$start[0]."-01";
            $nM=$start[0]+1;
            if($start[0]=='12')
            {
              $start[1]=(int)$start[1]+1;
              $nM=1;
            }
            if($nM<10)
            {
              $afrDate=$start[1]."-0".$nM."-01";
            }
            else
            {
              $afrDate=$start[1]."-".$nM."-01";
            }
            $filterDate="AND (report_agent_date_flown>='".$bfrDate."' AND report_agent_date_flown<'".$afrDate."')";
            $filterWDate="WHERE (report_agent_date_flown>='".$bfrDate."' AND report_agent_date_flown<'".$afrDate."')";
          }
        }

        if(isset($_POST['byKIU']))
        {
          $currenYear=(int)date('Y');
          $nextYear=$currenYear+1;

            switch ($_POST['byKIU']) {
              
              case 'Q1':{
                $filterDate="AND (report_agent_date_flown>='".$currenYear."-01-01' AND report_agent_date_flown<'".$currenYear."-04-01')";
                $filterWDate="WHERE (report_agent_date_flown>='".$currenYear."-01-01' AND report_agent_date_flown<'".$currenYear."-04-01')";
                //var_dump($_POST['byKIU']);
                break;
              }
              
              case 'Q2':{
                $filterDate="AND (report_agent_date_flown>='".$currenYear."-04-01' AND report_agent_date_flown<'".$currenYear."-07-01')";
                $filterWDate="WHERE (report_agent_date_flown>='".$currenYear."-04-01' AND report_agent_date_flown<'".$currenYear."-07-01')";
                //var_dump($_POST['byKIU']);
                break;
              }

              case 'Q3':{
                $filterDate="AND (rpa0.report_agent_date_flown>='".$currenYear."-07-01' AND report_agent_date_flown<'".$currenYear."-10-01')";
                $filterWDate="WHERE (rpa0.report_agent_date_flown>='".$currenYear."-07-01' AND report_agent_date_flown<'".$currenYear."-10-01')";
                //var_dump($_POST['byKIU']);
                break;
              }

              case 'Q4':{
                $filterDate="AND (report_agent_date_flown>='".$currenYear."-10-01' AND report_agent_date_flown<'".$nextYear."-01-01')";
                $filterWDate="WHERE (report_agent_date_flown>='".$currenYear."-10-01' AND report_agent_date_flown<'".$nextYear."-01-01')";
                //var_dump($_POST['byKIU']);
                break;
              }
            }
        }

        $kpi=array();

        //CONSULTAS

        $sqlAgentGRM="
        SELECT 
          COUNT(DISTINCT report_agent_agent) 
        FROM 
          report_agent
        WHERE
          (report_agent_agent IN (SELECT basic_email_agent_agent FROM basic_communications_email_agent) OR
          report_agent_agent IN (SELECT tactic_email_agent_agent FROM tactic_communications_email_agent) OR
          report_agent_agent IN (SELECT tactic_sms_agent_agent FROM tactic_communications_sms_agent)) ".$filterDate."
        ";


        $sqlAgentGCTRL="
        SELECT 
          COUNT(DISTINCT report_agent_agent) 
        FROM 
          report_agent
        WHERE
          (report_agent_agent NOT IN (SELECT basic_email_agent_agent FROM basic_communications_email_agent) AND
          report_agent_agent NOT IN (SELECT tactic_email_agent_agent FROM tactic_communications_email_agent) AND
          report_agent_agent NOT IN (SELECT tactic_sms_agent_agent FROM tactic_communications_sms_agent)) ".$filterDate."
        ";

        $sqlAgentTotal="
        SELECT 
          COUNT(DISTINCT report_agent_agent) 
        FROM 
          report_agent
          ".$filterWDate."
        ";

        $sqlRevenueGRM="
        SELECT 
          SUM(report_agent_copa_revenue) 
        FROM 
          report_agent
        WHERE
          (report_agent_agent IN (SELECT basic_email_agent_agent FROM basic_communications_email_agent) OR
          report_agent_agent IN (SELECT tactic_email_agent_agent FROM tactic_communications_email_agent) OR
          report_agent_agent IN (SELECT tactic_sms_agent_agent FROM tactic_communications_sms_agent)) ".$filterDate."
        ";

        $sqlRevenueGCTRL="
        SELECT 
          SUM(report_agent_copa_revenue) 
        FROM 
          report_agent
        WHERE
          (report_agent_agent NOT IN (SELECT basic_email_agent_agent FROM basic_communications_email_agent) AND
          report_agent_agent NOT IN (SELECT tactic_email_agent_agent FROM tactic_communications_email_agent) AND
          report_agent_agent NOT IN (SELECT tactic_sms_agent_agent FROM tactic_communications_sms_agent)) ".$filterDate."
        ";

        $sqlRevenueTotal="
        SELECT 
          SUM(report_agent_copa_revenue) 
        FROM 
          report_agent
          ".$filterWDate."
        ";

        $sqlTicketGRM="
        SELECT 
          COUNT(DISTINCT report_agent_flight_origin,report_agent_flight_destiny)
        FROM 
          report_agent
        WHERE
          (report_agent_agent IN (SELECT basic_email_agent_agent FROM basic_communications_email_agent) OR
          report_agent_agent IN (SELECT tactic_email_agent_agent FROM tactic_communications_email_agent) OR
          report_agent_agent IN (SELECT tactic_sms_agent_agent FROM tactic_communications_sms_agent)) ".$filterDate."
        ";

        $sqlTicketGCTRL="
        SELECT 
          COUNT(DISTINCT report_agent_flight_origin,report_agent_flight_destiny)
        FROM 
          report_agent
        WHERE
          (report_agent_agent NOT IN (SELECT basic_email_agent_agent FROM basic_communications_email_agent) AND
          report_agent_agent NOT IN (SELECT tactic_email_agent_agent FROM tactic_communications_email_agent) AND
          report_agent_agent NOT IN (SELECT tactic_sms_agent_agent FROM tactic_communications_sms_agent)) ".$filterDate."
        ";

        $sqlTicketTotal="
        SELECT 
          COUNT(DISTINCT report_agent_flight_origin,report_agent_flight_destiny)
        FROM 
          report_agent
          ".$filterWDate."
        ";

        $sqlWings="
        SELECT
          SUM((report_agent_white_wings+report_agent_white_plus+report_agent_blue_wings)*4000)
        FROM 
          report_agent
          ".$filterWDate."
        ";

        if($sta=$db->prepare($sqlAgentTotal))
        {
          $sta->execute();
          $sta->bind_result($agentTotal);
          $sta->fetch();
          $sta->close();
        }

        if($sta=$db->prepare($sqlAgentGRM))
        {
          $sta->execute();
          $sta->bind_result($agentGRM);
          $sta->fetch();

          $kpi['compromiso']['grm']=(($agentGRM/$agentTotal)*100);

          $sta->close();
        }

        if($sta=$db->prepare($sqlAgentGCTRL))
        {
          $sta->execute();
          $sta->bind_result($agentGCTRL);
          $sta->fetch();

          $kpi['compromiso']['gctrl']=(($agentGCTRL/$agentTotal)*100);
          
          $sta->close();
        }

        if($kpi['compromiso']['grm']>$kpi['compromiso']['gctrl']){
          $kpi['compromiso']['cumplimiento']='#00FF00';
        }
        else if($kpi['compromiso']['grm']<$kpi['compromiso']['gctrl']){
          $kpi['compromiso']['cumplimiento']='#FF0000';
        }
        else if($kpi['compromiso']['grm']==$kpi['compromiso']['gctrl']){
          $kpi['compromiso']['cumplimiento']='#FFFF00';
        }

        if($sta=$db->prepare($sqlRevenueTotal))
        {
          $sta->execute();
          $sta->bind_result($revenueTotal);
          $sta->fetch();
          $sta->close();
        }

        if($sta=$db->prepare($sqlRevenueGRM))
        {
          $sta->execute();
          $sta->bind_result($revenueGRM);
          $sta->fetch();

          $kpi['conducta']['grm']=(($revenueGRM/$revenueTotal)*100);

          $sta->close();
        }

        if($sta=$db->prepare($sqlRevenueGCTRL))
        {
          $sta->execute();
          $sta->bind_result($revenueGCTRL);
          $sta->fetch();

          $kpi['conducta']['gctrl']=(($revenueGCTRL/$revenueTotal)*100);
          
          $sta->close();
        }

        if($kpi['conducta']['grm']>$kpi['conducta']['gctrl']){
          $kpi['conducta']['cumplimiento']='#00FF00';
        }
        else if($kpi['conducta']['grm']<$kpi['conducta']['gctrl']){
          $kpi['conducta']['cumplimiento']='#FF0000';
        }
        else if($kpi['conducta']['grm']==$kpi['conducta']['gctrl']){
          $kpi['conducta']['cumplimiento']='#FFFF00';
        }

        if($sta=$db->prepare($sqlTicketTotal))
        {
          $sta->execute();
          $sta->bind_result($ticketTotal);
          $sta->fetch();
          $sta->close();
        }

        if($sta=$db->prepare($sqlTicketGRM))
        {
          $sta->execute();
          $sta->bind_result($ticketGRM);
          $sta->fetch();

          $kpi['involucramiento']['grm']=(($ticketGRM/$ticketTotal)*100);

          $sta->close();
        }

        if($sta=$db->prepare($sqlTicketGCTRL))
        {
          $sta->execute();
          $sta->bind_result($ticketGCTRL);
          $sta->fetch();

          $kpi['involucramiento']['gctrl']=(($ticketGCTRL/$ticketTotal)*100);
          
          $sta->close();
        }

        if($kpi['involucramiento']['grm']>$kpi['involucramiento']['gctrl']){
          $kpi['involucramiento']['cumplimiento']='#00FF00';
        }
        else if($kpi['involucramiento']['grm']<$kpi['involucramiento']['gctrl']){
          $kpi['involucramiento']['cumplimiento']='#FF0000';
        }
        else if($kpi['involucramiento']['grm']==$kpi['involucramiento']['gctrl']){
          $kpi['involucramiento']['cumplimiento']='#FFFF00';
        }

        if($sta=$db->prepare($sqlWings))
        {
          $sta->execute();
          $sta->bind_result($wingsTotal);
          $sta->fetch();

          $kpi['rate']['real']=(($wingsTotal/$revenueTotal)*100);

          $kpi['rate']['dif']=$kpi['rate']['real']-6;

          $sta->close();
        }

        if($kpi['rate']['dif']>0){
          $kpi['rate']['cumplimiento']='#00FF00';
        }
        else if($kpi['rate']['dif']<0){
          $kpi['rate']['cumplimiento']='#FF0000';
        }
        else if($kpi['rate']['dif']==0){
          $kpi['rate']['cumplimiento']='#FFFF00';
        }

        
        $_SESSION['kpi_array']=$kpi;
        echo "1";
    }

    if(isset($_POST['cleanKpi']) && $_POST['cleanKpi'])
    {
        unset($_SESSION['kpi_array']);
        if(!isset($_SESSION['kpi_array']))
        {
            echo "1";
        }
    }
?>