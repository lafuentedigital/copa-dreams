function dataXlsBSubmit(formData, jqForm, options)
{
    var queryString = $.param(formData); 
    return true; 
}

function loadPage(link,target,pagination,urla,attr)
{
  target = target || "#page-loader";
  pagination = pagination || 1;
  urla = urla || "inc/page-request.php";
  attr = attr || "";

  dataSend="page="+link+"&navpag="+pagination+attr;
  $("#load-anim").fadeIn(100);
  $.ajax({
    type: "POST",
    url: urla,
    data: dataSend,
    async: true
  }).done(function(data){
    $("#load-anim").fadeOut(100);
    $(target).html(data);
    onLoadPages();
  });
}

function dataXlsSucces(responseText, statusText, xhr, $form)
{
	$("#load-bar").toggle();
	//$('#responsesubmit').html(responseText);
  var text = responseText;
  var rows=text.split('-');
  //console.log(text);
  alert("Filas: "+rows[2]+" Agregadas: "+rows[0]+" Errores: "+rows[1]);
  loadPage('#loyalty');
}

function onLoadPages()
{
	//$(".input-group.date").datepicker({ autoclose: true, todayHighlight: true});
  //$("#pdate").datepicker({ format: "mm-yyyy", startView: 2, minViewMode: 1, language: "es", autoclose: true });
  $('.input-group.date.p').datepicker({
    format: "mm-yyyy",
    startView: 2,
    minViewMode: 1,
    todayBtn: true,
    language: "es",
    orientation: "top auto",
    calendarWeeks: true,
    autoclose: true,
    todayHighlight: true
  });
}

$( document ).ready(function() {
  /*
    LOAD THE DATEPICKER AND OTHER FUNCTIONS
  */
  onLoadPages();


  /*
    CHECK IF THE PAGE IS NOT LOAD
  */
  if( $('#page-loader').height()<50) {
    urla = "inc/page-request.php";
    
    $('#page-loader').html('<div id="load-anim" style="display: block; margin: 0 auto; text-align: center; margin-top: 20%; }"><img src="img/load.gif" height="45px" width="45px"></div>');
    $.ajax({
      type: "POST",
      url: urla,
      async: true
    }).done(function(data){
      $("#load-anim").fadeOut(100);
      $('#page-loader').html(data);
      onLoadPages();
    });
  }


  /*
    CHECK TIMEOUT LOGIN
  */

  setInterval(function(){
    $.ajax({
      type: "POST",
      url: "reload.php",
      data: { neighborhood: true },
      async: true
    }).done(function(response){
      console.log(response);
      if(response==1)
      {
        location.reload();
      }

    });
  },
    24*60*60*1000
  );
  /*
    MENU DROPDOWN
  */
  $(document).on('click','.menu-title', function(e){
    e.preventDefault();
    var menu = $(this).attr('id');
    var hide = $('#'+menu+"-list").css('display');
    if(hide=='none'){
      $('#'+menu+"-list").slideDown();
    }
    else
    {
      $('#'+menu+"-list").slideUp();
    }
  });

  /*
    LOAD PAGE BY LINK 
  */
	$(document).on('click','.dash-link', function(e){
		e.preventDefault();
		var link = $(this).attr('href');
		loadPage(link);
	});

  $(document).on('click','.dash-pag', function(e){
    e.preventDefault();
    var link = $(this).attr('href');
    var tab = $(this).attr('id');
    tmp1=tab.split("_");
    tab=tmp1[0];
    tab="#"+tab;
    tmp2=link.split("_");
    link=tmp2[0];
    pag=tmp2[1];
    loadPage(link+"-table",tab,pag);
    console.log(link+"\n");
    console.log(tab+"\n");
    console.log(pag+"\n");
  });

  var options = { 
      target:     '#responsesubmit', 
      beforeSubmit:  dataXlsBSubmit,  
      success: dataXlsSucces,
      type:      'POST',
      resetForm: true 
  }; 
  
  /*
    SEND FILES
  */
  $(document).on('click', '#submitbtn', function(e)
  {
  		$('#data-xls-submit').submit(function() { 
  			$("#load-bar").toggle();
        $(this).ajaxSubmit(options); 
        return false; 
    }); 
  }); 
});

$(window).load(function() {
  /*
    DEFINE NEIGHBORHOODS CALL
  */
  $(document).on('click','#define-neighborhoods', function(e)
  {
    e.preventDefault();
    if (confirm("¿Esta seguro que desea proseguir con esta operacion?"))
    {
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/neighborhoods-calculation.php",
        data: { neighborhood: true, date: $('#date-neighborhood').val() },
        async: true
      }).done(function(data){
        
        //console.log(data);
        $("#load-anim").fadeOut(100);
        if(data=="1"){
          loadPage('#loyalty');
          $('#clean-filter').trigger('click');
        }
        else if(data=="01")
        {
          alert("Se han definido los vecindarios correctamente.");
          loadPage('#loyalty');
        }
        else if(data=="021")
        {
          alert("Ya se han definido los vecindarios para este periodo.");
          loadPage('#loyalty');
        }
        else if(data=="007")
        {
          alert("No has seleccionado una Fecha.");
          loadPage('#define-neighborhood');
        }
        else
        {
          //alert('log');
          console.log(data);
        }
      });
    }
  });

    $(document).on('change','#filter-location-country', function()
    {
      var c = $(this).val();
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/loyalty-request.php",
        data: { countrys: c },
        async: true
      }).done(function(data){
        $('#filter-location-city').html(data);
        $("#load-anim").fadeOut(100);
      });
    });

    $(document).on('change','#filter-location-country-2', function()
    {
      var c = $(this).val();
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/loyalty-request.php",
        data: { countrys: c },
        async: true
      }).done(function(data){
        $('#filter-location-city-2').html(data);
        $("#load-anim").fadeOut(100);
      });
    });

    //LOYALTY

    $(document).on('click','#filter-loyalty', function(e)
    {
      e.preventDefault();
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/loyalty-request.php",
        data: { filters: true , byCondition: $("#filter-condition").val(), byTreatment: $("#filter-treatment").val(), byLocation:$("#filter-location-city").val(), byRoute:$("#filter-rute").val(), byRouteType: $("#filter-rute-type").val(), byPeriodStart: $("#filter-period-start").val(), byQ: $("#filter-q").val()},
        async: true
      }).done(function(data){
        //alert(data);
        $("#load-anim").fadeOut(100);
        if(data==1){
          loadPage('#loyalty-table','#loyalty-table');
        }
      });
    });
    
/*    $(document).on('click','#filter-loyalty', function(e)
    {
      e.preventDefault();
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/loyalty-request.php",
        data: { filters: true , byQ: $("#filter-q").val()},
        async: true
      }).done(function(data){
        //alert(data);
        $("#load-anim").fadeOut(100);
        if(data==1){
          loadPage('#loyalty-table','#loyalty-table');
        }
      });
    });*/

    $(document).on('click','#clean-filter', function(e)
    {
      e.preventDefault();
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/loyalty-request.php",
        data: { clean: true},
        async: true
      }).done(function(data){
        //alert(data);
        $("#load-anim").fadeOut(100);
        if(data==1){
          //alert("¡Listo!");
          loadPage('#loyalty');
        }
      });
    });

    //TOP AGENTS

    $(document).on('click','#filter-top-agents', function(e)
    {
      e.preventDefault();
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/tops-request.php",
        data: { filtersAgents: true , byNeighborhood: $("#filter-neighborhood").val(), byTreatment: $("#filter-treatment").val(), byPeriodStart: $("#filter-period-start").val(),/* byPeriodFinish: $("#filter-period-finish").val(), */byQ: $("#filter-q").val()},
        async: true
      }).done(function(data){
        //alert(data);
        $("#load-anim").fadeOut(100);
        if(data==1){
          loadPage('#top-agent-table','#top-table');
        }
      });
    });

    $(document).on('click','#clean-top-agents', function(e)
    {
      e.preventDefault();
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/tops-request.php",
        data: { cleanAg: true},
        async: true
      }).done(function(data){
        //alert(data);
        $("#load-anim").fadeOut(100);
        if(data==1){
          //alert("¡Listo!");
          loadPage('#top-agent');
        }
      });
    });

    //TOP AGENCIES

    $(document).on('click','#filter-top-agencies', function(e)
    {
      e.preventDefault();
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/tops-request.php",
        data: { filtersAgencies: true , byNeighborhood: $("#filter-neighborhood").val(), byTreatment: $("#filter-treatment").val(), byPeriodStart: $("#filter-period-start").val(),/* byPeriodFinish: $("#filter-period-finish").val(), */byQ: $("#filter-q").val()},
        async: true
      }).done(function(data){
        //alert(data);
        $("#load-anim").fadeOut(100);
        if(data==1){
          loadPage('#top-agencies-table','#top-table');
        }
      });
    });

    $(document).on('click','#clean-top-agencies', function(e)
    {
      e.preventDefault();
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/tops-request.php",
        data: { cleanAgCy: true},
        async: true
      }).done(function(data){
        //alert(data);
        $("#load-anim").fadeOut(100);
        if(data==1){
          //alert("¡Listo!");
          loadPage('#top-agencies');
        }
      });
    });

    //BASIC MAIL

    $(document).on('click','#filter-coms-tab1', function(e)
    {
      e.preventDefault();
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/coms-request.php",
        data: { filtersComsTab1: true , byNeighborhood: $("#filter-neighborhood").val(), byPeriod: $("#filter-period").val(), byLocation:$("#filter-location-city").val()},
        async: true
      }).done(function(data){
        //alert(data);
        $("#load-anim").fadeOut(100);
        if(data==1){
          loadPage('#coms-table','#top-table1');
        }
      });
    });

    $(document).on('click','#clean-coms-tab1', function(e)
    {
      e.preventDefault();
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/coms-request.php",
        data: { cleanTab1: true},
        async: true
      }).done(function(data){
        //alert(data);
        $("#load-anim").fadeOut(100);
        if(data==1){
          //alert("¡Listo!");
          loadPage('#coms');
        }
      });
    });

    $(document).on('click','#filter-coms-tab2', function(e)
    {
      e.preventDefault();
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/coms-request.php",
        data: { filtersComsTab2: true , byCondition: $("#filter-condition").val(), byPercent: $("#filter-open-percent").val(), byLocation:$("#filter-location-city-2").val(), byNeighborhood: $("#filter-neighborhood-2").val()},
        async: true
      }).done(function(data){
        //alert(data);
        $("#load-anim").fadeOut(100);
        if(data==1){
          loadPage('#coms-table-0','#top-table2');
        }
      });
    });

    $(document).on('click','#clean-coms-tab2', function(e)
    {
      e.preventDefault();
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/coms-request.php",
        data: { cleanTab2: true},
        async: true
      }).done(function(data){
        //alert(data);
        $("#load-anim").fadeOut(100);
        if(data==1){
          //alert("¡Listo!");
          loadPage('#coms');
        }
      });
    });

    //TACTICAL MAIL

    $(document).on('click','#filter-coms-tac-tab1', function(e)
    {
      e.preventDefault();
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/coms-tactic-request.php",
        data: { filtersComsTacTab1: true , byNeighborhood: $("#filter-neighborhood").val(), byPeriod: $("#filter-period").val(), byLocation:$("#filter-location-city").val(), byRoutes:$("#filter-route-1").val()},
        async: true
      }).done(function(data){
        //alert(data);
        $("#load-anim").fadeOut(100);
        if(data==1){
          loadPage('#coms-tmail-table','#top-table1');
        }
      });
    });

    $(document).on('click','#clean-coms-tac-tab1', function(e)
    {
      e.preventDefault();
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/coms-tactic-request.php",
        data: { cleanTacTab1: true},
        async: true
      }).done(function(data){
        //alert(data);
        $("#load-anim").fadeOut(100);
        if(data==1){
          //alert("¡Listo!");
          loadPage('#coms-tmail');
        }
      });
    });

    $(document).on('click','#filter-coms-tac-tab2', function(e)
    {
      e.preventDefault();
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/coms-tactic-request.php",
        data: { filtersComsTacTab2: true , byCondition: $("#filter-condition").val(), byPercent: $("#filter-open-percent").val(), byLocation:$("#filter-location-city-2").val(), byNeighborhood: $("#filter-neighborhood-2").val(), byRoutes:$("#filter-route-2").val()},
        async: true
      }).done(function(data){
        console.log(data);
        $("#load-anim").fadeOut(100);
        if(data==1){
          loadPage('#coms-tmail-table-0','#top-table2');
        }
      });
    });

    $(document).on('click','#clean-coms-tac-tab2', function(e)
    {
      e.preventDefault();
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/coms-tactic-request.php",
        data: { cleanTacTab2: true},
        async: true
      }).done(function(data){
        console.log(data);
        $("#load-anim").fadeOut(100);
        if(data==1){
          //alert("¡Listo!");
          loadPage('#coms-tmail');
        }
      });
    });

    //TACTICAL SMS

    $(document).on('click','#filter-coms-tacs-tab1', function(e)
    {
      e.preventDefault();
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/coms-tactics-request.php",
        data: { filtersComsTacTab1: true , byNeighborhood: $("#filter-neighborhood").val(), byPeriod: $("#filter-period").val(), byLocation:$("#filter-location-city").val(), byRoutes:$("#filter-route-1").val()},
        async: true
      }).done(function(data){
        console.log(data);
        $("#load-anim").fadeOut(100);
        if(data==1){
          loadPage('#coms-tsms-table','#top-table1');
        }
      });
    });

    $(document).on('click','#clean-coms-tacs-tab1', function(e)
    {
      e.preventDefault();
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/coms-tactics-request.php",
        data: { cleanTacTab1: true},
        async: true
      }).done(function(data){
        console.log(data);
        $("#load-anim").fadeOut(100);
        if(data==1){
          //alert("¡Listo!");
          loadPage('#coms-tsms');
        }
      });
    });

    $(document).on('click','#filter-coms-tacs-tab2', function(e)
    {
      e.preventDefault();
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/coms-tactics-request.php",
        data: { filtersComsTacTab2: true , byCondition: $("#filter-condition").val(), byPercent: $("#filter-open-percent").val(), byLocation:$("#filter-location-city-2").val(), byNeighborhood: $("#filter-neighborhood-2").val(), byRoutes:$("#filter-route-2").val()},
        async: true
      }).done(function(data){
        console.log(data);
        $("#load-anim").fadeOut(100);
        if(data==1){
          loadPage('#coms-tsms-table-0','#top-table2');
        }
      });
    });

    $(document).on('click','#clean-coms-tacs-tab2', function(e)
    {
      e.preventDefault();
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/coms-tactics-request.php",
        data: { cleanTacTab2: true},
        async: true
      }).done(function(data){
        console.log(data);
        $("#load-anim").fadeOut(100);
        if(data==1){
          //alert("¡Listo!");
          loadPage('#coms-tsms');
        }
      });
    });

    //KPI

    $(document).on('click','#filter-kpi', function(e)
    {
      e.preventDefault();
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/kpi-filters.php",
        data: { filtersKpi: true , byKIU: $("#filter-q").val(), byPeriod: $("#filter-period-start").val()},
        async: true
      }).done(function(data){
        console.log(data);
        $("#load-anim").fadeOut(100);
        if(data==1){
          console.log("Filter");
          loadPage('#kpi');
        }
      });
    });

    $(document).on('click','#clean-kpi', function(e)
    {
      e.preventDefault();
      $("#load-anim").fadeIn(100);
      $.ajax({
        type: "POST",
        url: "inc/kpi-filters.php",
        data: { cleanKpi: true},
        async: true
      }).done(function(data){
        console.log(data);
        $("#load-anim").fadeOut(100);
        if(data==1){
          //alert("¡Listo!");
          loadPage('#kpi');
        }
      });
    });
});