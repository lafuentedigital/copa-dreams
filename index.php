<?php
	include("inc/db.php");

	require('inc/classes/session.class.php');
	$session = new session();
	$session->start_session('_s', false, $db);
	$_SESSION['expire'] = 24*60*60; // Horas*Minutos*Segundos

	if(isset($_SESSION['logged']))
	{
		if($_SESSION['activity'] < time()-$_SESSION['expire'] ) 
		{ 
			//have we expired?
		    //redirect to logout.php
		    header('Location: /logout.php'); //change yoursite.com to the name of you site!!
		} 
	}

	include("views/header.php");
	
	//unset($_SESSION['logged']);

	if(isset($_SESSION['logged']))
	{
		
		include("views/navbar.php");
?>
		<div id="page-loader" class="container-fluid">
<?php 
		if(!isset($_SESSION['active'])){
			$_SESSION['active']='loyalty';
			include("views/dash-loyalty.php");
		}
		else
		{
			//include("inc/page-request.php");
		}
		
		
?>
		</div>
<?php	
	}
	else
	{
		include("views/navbar.php");
		include("views/login.php");
	}
	include("views/footer.php");
?>